SHELL=bash

NAME := necro
# WARNING : GIT TAG is prefixed with "v"
VERSION := 0.16.2
TGZ := $(NAME)-v$(VERSION).tar.gz
MD5TXT := $(TGZ).md5sum.txt
OPAM_NAME := necrolib

# GIT VARIABLES
GITLAB := gitlab.inria.fr
GITPROJECT := skeletons
GIT_REPO := git@$(GITLAB):$(GITPROJECT)/$(NAME).git
GIT_RELEASE := https://$(GITLAB)/$(GITPROJECT)/$(NAME)/-/archive/v$(VERSION)/$(TGZ)

# OPAM VARIABLES
OPAM_REPO_GIT := ../opam-repository

##########################################################################################

nothing:

# Change version number everywhere where it's specified, and update Makefile
new_version:
	@read -p "Please enter new version (current is $(VERSION)): " g;\
	perl -i -pe "s#archive/v$(VERSION)#archive/v$$g#" ./necrolib.opam.template;\
	perl -i -pe "s#v$(VERSION).tar.gz#v$$g.tar.gz#" ./necrolib.opam.template;\
	perl -i -pe "s/^VERSION := $(VERSION)/VERSION := $$g/" ./Makefile;\
	perl -i -pe "s/^\(version $(VERSION)\)$$/\(version $$g\)/" ./dune-project;\
	git commit ./dune-project ./Makefile ./necrolib.opam.template -m "Release version $$g";\
	git tag -a v$$g -fm 'OPAM necrolib package for deployment'
	@git push --follow-tags

opam_update:
	@rm -f $(TGZ) $(MD5TXT)
	@wget -q $(GIT_RELEASE)
	@md5sum $(TGZ) > $(MD5TXT)
	@perl -i -pe 's|^\s*checksum: "md5\s*=.*"| checksum: "md5='$$(cat $(MD5TXT) | cut -d" " -f 1)'"|' necrolib.opam.template
	@dune build
	@cd $(OPAM_REPO_GIT); git checkout necro; git pull --rebase
	@mkdir -p $(OPAM_REPO_GIT)/packages/$(OPAM_NAME)/$(OPAM_NAME).$(VERSION)/
	@cp necrolib.opam $(OPAM_REPO_GIT)/packages/$(OPAM_NAME)/$(OPAM_NAME).$(VERSION)/opam

release:
	cd ../opam-repository; \
		git pull 2>/dev/null || true
	rm -f $(TGZ) $(MD5TXT)
	@make new_version
	@make opam_update
	@cd $(OPAM_REPO_GIT); git checkout necro; \
		git add packages/necrolib && git commit -am "Push new Necro Lib version" && git push

rerelease:
	cd ../opam-repository; \
		git pull 2>/dev/null || true
	@git tag -f v$(VERSION)
	@git push --tags --force
	@make opam_update
	cd ../opam-repository; git checkout necro; \
		git add packages/necrolib && git commit -am "Upstream change for Necro Lib" && git push

.PHONY: opam_update new_version nothing release rerelease
