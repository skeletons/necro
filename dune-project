(lang dune 3.4)

(name necrolib)
(version 0.16.2)

(using dune_site 0.1)

(generate_opam_files true)

(authors "Nathanaëlle Courant" "Enzo Crance" "Victoire Noizet" "Alan Schmitt")
(maintainers "Victoire Noizet")

(bug_reports https://gitlab.inria.fr/skeletons/necro/-/issues)
(homepage https://skeletons.gitlabpages.inria.fr/necro-website)
(documentation https://skeletons.gitlabpages.inria.fr/necro-website/manual.html)

(using menhir 2.0)

(package
   (name necrolib)
   (synopsis "Handling Skeletal Semantics")
   (description "Basic library to handle Skeletal Semantics and generate tools \
                handling them. Necrolib offers executables to parse, \
                pretty-print and transform skeletal semantics, and an OCaml \
                object file defining Skel's AST, and giving functions to do \
                basic manipulations such as parsing and typing")
   (depends
      ocamlfind
      menhir
      (ocaml (>= 5.1))
      dune-build-info
      ocamlgraph
   )
   (sites (share tools))
)
