(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

open ParsedAST
open TypedAST
open Util
open Errors

(**************************************************)
(*                     Helpers                    *)
(**************************************************)
type variable_env = typ SMap.t

let type_error x =
  Errors.type_error x

let type_error2 x =
  Errors.type_error2 x

let fail_type loc given expected =
  type_error loc (WrongType {given;expected})

let rec type_variables ty =
  begin match ty.tydesc with
  | UserType (_, args) ->
      sset_list_union (List.map type_variables args)
  | Product tl ->
      sset_list_union (List.map type_variables tl)
  | Arrow (tyi, tyo) ->
      SSet.union (type_variables tyi) (type_variables tyo)
  | Variable v ->
      SSet.singleton v
  end

(* Check that the number of given type arguments fits *)
let check_type_args loc typ name got expected =
  let ng = List.length got in
  let ne = List.length expected in
  if ng <> ne then
    type_error loc (WrongNumberArgs {typ;name;expected=ne;given=ng})



(**************************************************)
(*                  Typing stuff                  *)
(**************************************************)
(* Return a variable environment equal to [ve] extended with the associations of
   [new_ve] *)
let extend ve new_ve =
  let f x t1 t2 =
    begin match x, t2 with
    | _, None -> t1
    | _, Some (_, t2) -> Some t2
    end
  in
  SMap.merge f ve new_ve

(* Check that all variables of new_ve are used, and raises a warning if they are not *)
let check_used warn ve (s:skeleton) =
  let fv = Skeleton.free_variables_skel s in
  SMap.iter (fun x (loc, _) ->
    if not (SMap.mem x fv) then
      type_warning warn loc (Unused {typ="variable"; name=x})) ve


let rec unalias
    (te_deps:TypingEnv.t SMap.t)
    (te: TypingEnv.t)
    (ty: typ):
      typ =
  begin match ty.tydesc with
  | UserType (n, ta_got) ->
      let (_, kind, _) =
        try
          TypingEnv.te_get te_deps te TypingEnv.te_types n
        with Not_found ->
          let name = match n with
          | (n, None) -> n
          | (n, Some x) -> x ^ "::" ^ n
          in Errors.type_error ty.tyloc (Unbound {typ="type";name})
      in
      if kind <> Alias then ty else
      let (_, ta, ty) = TypingEnv.te_get te_deps te TypingEnv.te_aliases n in
      check_type_args ghost_loc "type alias" (fst n) ta_got ta ;
      unalias te_deps te (Types.subst_mult ta ta_got ty)
  | _ -> ty
  end

let rec unalias_rec
    (te_deps:TypingEnv.t SMap.t)
    (te: TypingEnv.t)
    (ty: typ):
      typ =
  {ty with tydesc=
    begin match ty.tydesc with
    | Variable x -> Variable x
    | UserType (n, ta) ->
        let (_, kind, _) = TypingEnv.te_get te_deps te TypingEnv.te_types n in
        if kind <> Alias
        then UserType (n, List.map (unalias_rec te_deps te) ta)
        else (unalias_rec te_deps te (unalias te_deps te ty)).tydesc
    | Arrow (i, o) ->
        Arrow (unalias_rec te_deps te i, unalias_rec te_deps te o)
    | Product tyl ->
        Product (List.map (unalias_rec te_deps te) tyl)
    end}

let rec type_equals
    (te_deps: TypingEnv.t SMap.t)
    (te: TypingEnv.t)
    (ty1: typ)
    (ty2: typ):
      bool =
  begin match
    (unalias te_deps te ty1).tydesc,
    (unalias te_deps te ty2).tydesc with
  | UserType (n1, ta1), UserType (n2, ta2) ->
      Pair.equal String.equal (Option.equal String.equal) n1 n2 &&
      List.compare_lengths ta1 ta2 = 0 &&
      List.for_all2 (type_equals te_deps te) ta1 ta2
  | UserType _, _ | _, UserType _ -> false
  | Arrow (i1, o1), Arrow (i2, o2) ->
      type_equals te_deps te i1 i2 &&
      type_equals te_deps te o1 o2
  | Arrow _, _ | _, Arrow _ -> false
  | Variable x, Variable y -> String.equal x y
  | Variable _, _ | _, Variable _ -> false
  | Product tyl1, Product tyl2 ->
      List.compare_lengths tyl1 tyl2 = 0 &&
      List.for_all2 (type_equals te_deps te) tyl1 tyl2
  end

(** [unify _ _ _ ty1 ty2] returns [Some m], if [m] is the minimal mapping of
    type variables to types, such that every variable from [ty1] has a mapping
    in [m], and [ty1] with the substitutions from [m] is equal to [ty2].
    [unify ss ty1 ty2] returns [None] if there is no such mapping.
    For instance,
    - [unify (a -> a) (int -> int) = Some [a → int]]
    - [unify (a -> a) (int -> bool) = None] *)
let unify te_deps te ty1 ty2 =
  let (let$) x f =
    begin match x with
    | None -> None
    | Some y -> f y
    end
  in
  let rec aux ty1 ty2 accu =
    begin match
      (unalias te_deps te ty1).tydesc,
      (unalias te_deps te ty2).tydesc with
    | Variable a, _ ->
        begin match SMap.find_opt a accu with
        | None -> Some (SMap.add a ty2 accu)
        | Some ty' -> if type_equals te_deps te ty' ty2 then Some accu else None
        end
    | Arrow (ty1i, ty1o), Arrow (ty2i, ty2o) ->
        let$ accu = aux ty1i ty2i accu in
        aux ty1o ty2o accu
    | Arrow _, _ -> None
    | UserType (s1, a1), UserType (s2, a2) ->
      if Pair.equal String.equal (Option.equal String.equal) s1 s2 (* same type *)
      then aux {ty1 with tydesc=Product a1} {ty2 with tydesc=Product a2} accu (* same type arguments *)
      else None
    | UserType _, _ -> None
    | Product [], Product [] -> Some accu
    | Product (a1 :: q1), Product (a2 :: q2) ->
        let$ accu = aux a1 a2 accu in
        aux {ty1 with tydesc = Product q1} {ty2 with tydesc = Product q2} accu
    | Product _, _ -> None
    end
  in
  aux ty1 ty2 SMap.empty

(** Using the algorithm described by Luc Maranget in "Les avertissements du
    filtrage", proceedings of the JFLA 2003 *)
let rec useful te_deps te pll pl =
  (* if [pll] is empty, then [pl] matches something, which is therefore
     something new (no empty types) *)
  if pll = [] then true else
  (* No column: the only value is (), so [pl] is useful iff [pll] is empty *)
  if pl = [] then false else
  (* case (c): p is an or-pattern *)
  let rec untype p =
    begin match p.pdesc with
    | PType (p, _) -> untype p
    | _ -> p
    end
  in
  let untype_head pl =
    begin match pl with
    | p :: pq -> untype p :: pq
    | [] -> []
    end
  in
  let is_or_pattern, result =
    begin match untype_head pl with
    | {pdesc=POr (p1, p2);_} :: pq ->
        (true, fun () ->
        useful te_deps te pll (p1::pq) ||
        useful te_deps te pll (p2::pq))
    | _ -> false, fun () -> assert false
    end
  in
  if is_or_pattern then result () else
  (* remove or-patterns from the first column of pll *)
  let rec remove_or p =
    begin match untype p with
    | {pdesc=POr (p1, p2);_} -> remove_or p1 @ remove_or p2
    | _ -> [p]
    end
  in
  let pll = List.concat_map (fun pl ->
    begin match pl with
    | p :: pq -> List.map (fun x -> x :: pq) (remove_or p)
    | [] -> assert false (* pl≠[] so elements of pll should be ≠[] as well *)
    end) pll
  in
  let p1, pq =
    begin match pl with
    | a :: q -> a, q
    | [] -> assert false (* we tested it above *)
    end
  in
  let ty = p1.ptyp in
  begin match (unalias te_deps te ty).tydesc with
  | Product tyl -> (* decompose the patterns *)
      let untuple p =
        begin match (untype p).pdesc with
        | PTuple pl -> pl
        | PWild | PVar _ ->
            List.map (fun ty -> {pdesc=PWild;ploc=ghost_loc; ptyp=ty}) tyl
        | _ -> assert false
        end
      in
      let pll =
        List.map (function | [] -> assert false | a :: q -> untuple a @ q) pll
      in
      let pl =
        begin match pl with | [] -> assert false | a :: q -> untuple a @ q end
      in useful te_deps te pll pl
  | Arrow _ | Variable _ ->
      (* Only _ matches these types *)
      useful te_deps te (List.map List.tl pll) pq
  | UserType (ty, ta) ->
      let (_, k, _) = TypingEnv.te_get te_deps te TypingEnv.te_types ty in
      begin match k with
      | Unspec ->
        useful te_deps te (List.map List.tl pll) pq (* Only _ matches this types *)
      | Alias -> assert false (* unaliased *)
      | Record -> (* decompose the patterns *)
        let fields = TypingEnv.te_get te_deps te TypingEnv.te_records ty in
        let fields = List.map (fun fsig ->
          let ftyp =
            Types.subst_mult fsig.fs_type_args ta fsig.fs_field_type in
          (fsig.fs_name, ftyp)) fields in
        let unrec p =
          begin match (untype p).pdesc with
          | PWild | PVar _ ->
              List.map (fun (_, ftyp) ->
                {pdesc=PWild;ptyp=ftyp;ploc=ghost_loc}) fields
          | PRecord fpl ->
              List.map (fun (f, ftyp) ->
                begin match List.find_opt (fun (((f', _), _),_) -> f' = f) fpl with
                | Some (_, p) -> p
                | None -> {pdesc=PWild; ptyp=ftyp; ploc=ghost_loc}
                end) fields
          | _ -> assert false
          end
        in
        let pll =
          List.map (function | [] -> assert false | a :: q -> unrec a @ q) pll
        in
        let pl =
          begin match pl with | [] -> assert false | a :: q -> unrec a @ q end
        in useful te_deps te pll pl
      | Variant ->
        let constructors = TypingEnv.te_get te_deps te TypingEnv.te_variants ty in
        begin match untype_head pl with
        | {pdesc=PConstr (c, p);_} :: pq -> (* case (a) *)
            let p_typ = p.ptyp in
            let select pl =
              begin match pl with
              | {pdesc=PConstr (c', p');_} :: pq when c = c' -> [p' :: pq]
              | {pdesc=PConstr (_, _);_} :: _ -> []
              | {pdesc=(PWild  | PVar _);_} :: pq ->
                  [{pdesc=PWild; ptyp=p_typ; ploc=ghost_loc} :: pq]
              | _ -> assert false
              end
            in
            let scp = List.concat_map select pll in
            useful te_deps te scp (p :: pq)
        | {pdesc=PWild;_} :: pq | {pdesc=PVar _;_} :: pq -> (* case (b) *)
            (* scp : to each constructor, bind the list of patterns *)
            let rec cut pl: string option * pattern list =
              begin match pl with
              | {pdesc=PConstr (((c, _), _), p);_} :: pq ->
                  (Some c, p :: pq)
              | {pdesc=(PWild | PVar _);_} :: pq ->
                  (None, pq)
              | {pdesc=(PType (p, _));_} :: pq -> cut (p :: pq)
              | _ -> assert false
              end
            in
            (* careful, here scp' is not [{S(c, p)| c in constructors}], since
               default cases are missing, so you have to add [D(p)] *)
            let scp', dp = partition cut pll in
            (* is there any unmatched constructor? *)
            let use_default =
              let constructors_matched = SMap.bindings scp' in
              List.compare_lengths constructors constructors_matched <> 0
            in
            SMap.exists (fun c scp' ->
              let cs = List.find (fun cs -> cs.cs_name = c) constructors in
              let ty = Types.subst_mult cs.cs_type_args ta cs.cs_input_type in
              let scp =
                List.map (fun pq -> {pdesc=PWild; ptyp=ty;ploc=ghost_loc} :: pq) dp @ scp'
              in
              let pl = {pdesc=PWild; ptyp=ty; ploc=ghost_loc} :: pq in
              useful te_deps te scp pl) scp' ||
            (use_default && useful te_deps te dp pq)
        | _ -> assert false
        end
      end
  end

(* Check the equality of types got and expected *)
let check_type loc deps_te te got expected =
  if not (type_equals deps_te te got expected) then
    fail_type loc got expected

(** only called with [branches <> []] or with [tyo <> None]*)
let merge_types ?(tyo=None) deps_te te ctxt loc tyl =
  begin match tyl with
  | [] ->
      begin match tyo with
      | None -> assert false
      | Some ty -> ty
      end
  | first :: others ->
    if List.for_all (fun other ->
      type_equals deps_te te first other) others
    then first
    else type_error loc (IncompatibleBranches (ctxt, tyl))
  end




let ppattern_to_pattern
    (deps_te: TypingEnv.t SMap.t)
    (te:TypingEnv.t)
    (tvars: SSet.t)
    (ty: typ option)
    (p:ppattern):
      (location * typ) SMap.t * pattern =
  let exception CannotInfer of location in
  let rec aux_pattern (ty: typ option) (p:ppattern):
        (location * typ) SMap.t * pattern =
    let make_ret map pdesc ptyp = map, {pdesc; ptyp; ploc=p.loc} in
    begin match p.contents, ty with
    | (Ppat_any|Ppat_variable _), None ->
        raise (CannotInfer p.loc)
    | Ppat_any, Some ty ->
        make_ret SMap.empty PWild ty
    | Ppat_variable x, Some ty ->
        make_ret (SMap.singleton x (p.loc, ty)) (PVar x) ty
    | Ppat_tuple pl, Some ty ->
        let tyl =
          begin match (unalias deps_te te ty).tydesc with
          | Product tyl when List.length tyl = List.length pl -> tyl
          | _ -> type_error p.loc (NoTypeMatch ty)
          end
        in
        let (vel, pl) =
          List.map2 aux_pattern (List.map Option.some tyl) pl |> List.split
        in
        let unify =
          let redef x _ _ = type_error p.loc (ReboundPattern x) in
          List.fold_left (SMap.union redef) SMap.empty
        in
        make_ret (unify vel) (PTuple pl) {tyloc=ty.tyloc;tydesc=Product tyl}
    | Ppat_tuple pl, None ->
        let (vel, pl) =
          List.map (aux_pattern None) pl |> List.split in
        let tyl = List.map (fun p -> p.ptyp) pl in
        let unify =
          let redef x _ _ = type_error p.loc (ReboundPattern x) in
          List.fold_left (SMap.union redef) SMap.empty
        in
        make_ret (unify vel) (PTuple pl) {tyloc=ghost_loc;tydesc=Product tyl}
    | Ppat_record _, None -> raise (CannotInfer p.loc)
    | Ppat_record [], Some _ -> assert false
    | Ppat_record fpl, Some ty ->
        let ((ty_name, path) as rec_ty), ta_got =
          begin match (unalias deps_te te ty).tydesc with
          | UserType (ty, ta) -> ty, ta
          | _ -> type_error p.loc (NoTypeMatch ty)
          end
        in
        let fsl =
          begin try
          TypingEnv.te_get deps_te te TypingEnv.te_records rec_ty
          with Not_found -> (* type ty is not a record type *)
            type_error p.loc (NoTypeMatch ty) end
        in
        let do_one (f, p) =
          let fdef =
            begin match List.find_opt (fun fs -> fs.fs_name = f.contents) fsl with
            | Some fdef -> fdef
            | None ->
                type_error f.loc (NoSuchField (f.contents, ty))
            end
          in
          let ty_p = Types.subst_mult fdef.fs_type_args ta_got fdef.fs_field_type in
          let (ve, input) = aux_pattern (Some ty_p) p in
          (ve, (((f.contents, path),(ty_name, ta_got)), input))
        in
        let vel, fpl_typed =
          List.map do_one fpl |> List.split
        in
        let unify =
          let redef x _ _ = type_error p.loc (ReboundPattern x) in
          List.fold_left (SMap.union redef) SMap.empty
        in
        make_ret (unify vel) (PRecord fpl_typed) ty
    | Ppat_constructor _, None -> raise (CannotInfer p.loc)
    | Ppat_constructor (c, p'), Some ty ->
        let ((ty_name, path) as var_ty), ta_got =
          begin match (unalias deps_te te ty).tydesc with
          | UserType (var_ty, ta) -> var_ty, ta
          | _ -> type_error p.loc (NoTypeMatch ty)
          end
        in
        let csl =
          begin try
          TypingEnv.te_get deps_te te TypingEnv.te_variants var_ty
          with Not_found -> (* type ty_name is not variant *)
            type_error p.loc (NoTypeMatch ty) end
        in
        let cdef =
          begin match List.find_opt (fun cs -> cs.cs_name = c) csl with
          | Some cdef -> cdef
          | None ->
              type_error p.loc (NoSuchConstructor (c, ty))
          end
        in
        let ty_p = Types.subst_mult cdef.cs_type_args ta_got cdef.cs_input_type in
        let (ve, input) = aux_pattern (Some ty_p) p' in
        make_ret ve (PConstr (((c, path), (ty_name, ta_got)), input)) ty
    | Ppat_or (p1, p2), Some ty ->
        let (ve1, p1t) = aux_pattern (Some ty) p1 in
        let (ve2, p2t) = aux_pattern (Some ty) p2 in
        let rec check l1 l2 =
          begin match l1, l2 with
          | [], [] -> ()
          | (a1, (loc, _)) :: _, (a2, _) :: _ when String.compare a1 a2 < 0 ->
              type_error loc (DifferentVariableOrPattern a1)
          | (a1, (loc, _)) :: _, [] ->
              type_error loc (DifferentVariableOrPattern a1)
          | (a1, _) :: _, (a2, (loc, _)) :: _ when String.compare a1 a2 > 0 ->
              type_error loc (DifferentVariableOrPattern a2)
          | [], (a2, (loc, _)) :: _ ->
              type_error loc (DifferentVariableOrPattern a2)
          | (a, (_, ty1)) :: _, (_, (_, ty2)) :: _ 
                when not (type_equals deps_te te ty1 ty2)->
              type_error p.loc (DifferentVariableTypeOrPattern
                {name=a;left=ty1;right=ty2})
          | _ :: q1, _ :: q2 -> check q1 q2
          end
        in
        check (SMap.bindings ve1) (SMap.bindings ve2) ;
        make_ret ve1 (POr (p1t, p2t)) ty
    | Ppat_or (p1, p2), None ->
        let ty =
          begin try
            let (_, p1) = aux_pattern None p1 in
            p1.ptyp
          with
          | CannotInfer _ ->
              let (_, p2) = aux_pattern None p2 in
            p2.ptyp
          end
        in
        aux_pattern (Some ty) p
    | Ppat_typed (p', ty'), tyo ->
        let ty' = TypingEnv.ptyp_to_typ tvars ty' in
        Option.iter (check_type p.loc deps_te te ty') tyo;
        let (ve, p) = aux_pattern (Some ty') p' in
        make_ret ve (PType (p, ty')) ty'
    end
  in
  begin try aux_pattern ty p
  with CannotInfer loc ->
    type_error loc CannotInferPatternType
  end


let pterm_to_term
    (warn: int list)
    (deps_te: TypingEnv.t SMap.t)
    (te: TypingEnv.t)
    (tvars: SSet.t) =
  let rec aux_term (ve:variable_env) (tyo: typ option) (t:pterm):
      term =
    (fun (t', ty) ->
      Option.iter (check_type t.loc deps_te te ty) tyo ;
      {tdesc=t';tloc=t.loc; ttyp=ty})
    begin match t.contents with
    | Pterm_variable (({contents=(x, optional_path) as v;_}), ta_got) ->
        let local_variable_type =
          if Option.is_none optional_path then SMap.find_opt x ve else None
        in
        begin match local_variable_type with
        | Some ty when optional_path = None ->
          check_type_args t.loc "variable" x ta_got [] ;
          (TVar (LetBound (x, ty)), ty)
        | _ ->
          let (_, k, ta_exp, ty, _) =
            try TypingEnv.te_get deps_te te TypingEnv.te_terms v with
            | Not_found -> type_error t.loc (Unbound {typ="term"; name=x})
          in
          check_type_args t.loc "variable" (fst v) ta_got ta_exp ;
          let ta_got = List.map (TypingEnv.ptyp_to_typ tvars) ta_got in
          let ty = Types.subst_mult ta_exp ta_got ty in
          (TVar (TopLevel (k, v, ta_got, ty)), ty)
        end
    | Pterm_tuple l when tyo = None ->
        let tl = List.map (aux_term ve None) l in
        let tyl = List.map (fun t -> t.ttyp) tl in
        (TTuple tl, {tyloc=ghost_loc; tydesc=Product tyl})
    | Pterm_tuple l ->
        let tyl =
          begin match Option.map (unalias deps_te te) tyo with
          | Some {tydesc=Product tyl;_} when List.compare_lengths tyl l = 0 ->
              tyl
          | Some ({tydesc=Product _;_} as ty) ->
              type_error t.loc (TupleWrongLength
                {got=List.length l;expected=ty})
          | Some ty -> type_error t.loc (NotProductType ty)
          | None -> assert false (* Impossible *)
          end
        in
        let tl = List.map2 (fun t ty -> aux_term ve (Some ty) t) l tyl in
        let tyl = List.map (fun t -> t.ttyp) tl in
        (TTuple tl, {tydesc=Product tyl; tyloc=ghost_loc})
    | Pterm_function (p, sk) ->
        let typ, tys =
          begin match Option.map (unalias deps_te te) tyo with
          | Some {tydesc=Arrow (typ, tys);_} -> Some typ, Some tys
          | Some ty -> type_error t.loc (NotArrowType ty)
          | None -> None, None
          end
        in
        let (pve, p) = ppattern_to_pattern deps_te te tvars typ p in
        let ve = extend ve pve in
        let sk = aux_skel ve sk tys in
        check_used warn pve sk ;
        (TFunc (p, sk), {tydesc=Arrow (p.ptyp, sk.styp);tyloc=ghost_loc})
    | Pterm_constructor ({contents=c;_}, ta_got, t') ->
        let ta_got = List.map (TypingEnv.ptyp_to_typ tvars) ta_got in
        let (_, csig) =
          try
            TypingEnv.te_get deps_te te TypingEnv.te_constructors c
          with Not_found ->
            let name = match c with
            | (n, None) -> n
            | (n, Some x) -> x ^ "::" ^ n
            in Errors.type_error t.loc (Unbound {typ="constructor";name})
        in
        let ta_exp = csig.cs_type_args in
        check_type_args t.loc "constructor" (fst c) ta_got ta_exp ;
        let ty_t = Types.subst_mult ta_exp ta_got csig.cs_input_type in
        let t' = aux_term ve (Some ty_t) t' in
        let ty_name = (csig.cs_variant_type, snd c) in
        let ty_out =
          {tydesc=UserType (ty_name, ta_got);tyloc=ghost_loc} in
        (TConstr ((c, (csig.cs_variant_type, ta_got)), t'), ty_out)
    | Pterm_getfield (t, {contents=f;loc=floc}) ->
        let t = aux_term ve None t in
        let record_type, record_path, record_ta_got =
          begin match (unalias deps_te te t.ttyp).tydesc with
          | UserType ((ty_name, path), ta_got) -> ty_name, path, ta_got
          | _ -> type_error t.tloc NotARecord
          end
        in
        let f = (f, record_path) in
        let (_, fsig) =
          try TypingEnv.te_get deps_te te TypingEnv.te_fields f
          with Not_found -> Errors.type_error floc
            (Unbound {typ="field";name=fst f})
        in
        if not (String.equal record_type fsig.fs_record_type) then
          begin
            let expected = {tydesc=UserType ((fsig.fs_record_type, None),
              List.map (Fun.const ({tydesc=Variable "_";tyloc=ghost_loc}))
            fsig.fs_type_args);tyloc=ghost_loc} in
            fail_type t.tloc t.ttyp expected
          end ;
        let ty_out = Types.subst_mult fsig.fs_type_args record_ta_got fsig.fs_field_type in
        (TField (t, (f, (fsig.fs_record_type, record_ta_got))), ty_out)
    | Pterm_getnth (_, n) when n <= 0 ->
        type_error t.loc ComponentZero
    | Pterm_getnth (t, n) ->
        let t = aux_term ve None t in
        let tyl =
          begin match (unalias deps_te te t.ttyp).tydesc with
          | Product tyl -> tyl
          | _ -> type_error t.tloc NotAProduct
          end
        in
        let ty_out =
          begin match List.nth_opt tyl (n-1) with
          | None -> type_error t.tloc (NoSuchComponent n)
          | Some ty_out -> ty_out
          end
        in
        (TNth (t, tyl, n), ty_out)
    | Pterm_recmake (ftl, path) -> (* path::{f1 = t1; …; fn = tn} *)
        let f1 =
          begin match ftl with
          | [] -> assert false (* impossible, checked at parsing *)
          | (f, _) :: _ -> f.contents
          end
        in
        let (_, fsig) =
          TypingEnv.te_get deps_te te TypingEnv.te_fields (f1, path) in
        let record_type = fsig.fs_record_type in
        let exp_fields =
          TypingEnv.te_get deps_te te TypingEnv.te_records (record_type, path) in
        let given_fields = List.stable_sort (fun (f,_) (g,_) ->
          String.compare f.contents g.contents) ftl
        in
        (* check no duplicate *)
        let rec aux l =
          begin match l with
          | [] | _ :: [] -> ()
          | (a,_) :: (b,_) :: _ when String.equal (a.contents) (b.contents) ->
              type_error2 b.loc a.loc (Redefinition {typ="field";name=a.contents})
          | _ :: q -> aux q
          end
        in
        aux given_fields ;
        (* check no wrong field (and bind fields to signatures) *)
        let fields =
          let get_info (f, t) =
            let fsig = List.find_opt (fun fs ->
              fs.fs_name = f.contents) exp_fields in
            begin match fsig with
            | Some fsig -> (f, t, fsig)
            | None -> type_error f.loc (Unbound {typ="field";name=f.contents})
            end
          in
          List.map get_info given_fields
        in
        (* check all fields present *)
        let missing_fields =
          exp_fields
          |> List.filter (fun fsig ->
              List.for_all (fun (f, _) -> fsig.fs_name<>f.contents) given_fields)
        in
        begin match missing_fields with
        | [] -> ()
        | _ -> type_error t.loc
          (MissingFields (List.map (fun f -> f.fs_name) missing_fields))
        end ;
        let ftl, vel =
          List.map (fun (f, t, fsig) ->
            let t = aux_term ve None t in
            begin match unify deps_te te fsig.fs_field_type t.ttyp with
            | None -> type_error t.tloc
                (WrongType {expected=fsig.fs_field_type; given=t.ttyp})
            | Some ve ->
                (((f.contents, path), t), ve)
            end) fields
          |> List.split
        in
        let map = List.fold_left (fun map e ->
          let union n typ1 typ2 =
            if type_equals deps_te te typ1 typ2
            then Some typ1
            else type_error t.loc (VarUnification {name=n; typ1; typ2})
          in SMap.union union e map) SMap.empty vel in
        let ta = List.map (fun a ->
          begin match SMap.find_opt a map with
          | Some ty -> ty
          | None -> assert false (* all types can be inferred, it has been
                checked at type definition *)
          end) fsig.fs_type_args
        in
        let ty = {tydesc=UserType ((record_type, path), ta);tyloc=ghost_loc} in
        (TRecMake (List.map (fun (f, t) -> ((f, (record_type, ta)), t)) ftl), ty)
    | Pterm_recset (t, ftl) ->
        let t = aux_term ve tyo t in
        let ty = t.ttyp in
        let path, tyn, ta =
          begin match (unalias deps_te te ty).tydesc with
          | UserType ((name, path), ta) -> path, name, ta
          | _ -> type_error t.tloc NotARecord
          end
        in
        let fields = TypingEnv.te_get deps_te te TypingEnv.te_records (tyn, path) in
        (* check no duplicate *)
        let rec aux l =
          begin match l with
          | [] | _ :: [] -> ()
          | (a,_) :: (b,_) :: _ when String.equal (a.contents) (b.contents) ->
              type_error2 b.loc a.loc (Redefinition {typ="field"; name=a.contents})
          | _ :: q -> aux q
          end
        in
        aux (List.stable_sort (fun (f,_) (g,_) ->
          String.compare f.contents g.contents) ftl) ;
        let do_one (f, t) =
          begin match List.find_opt (fun fs -> fs.fs_name = f.contents) fields with
          | None -> type_error f.loc (Unbound {typ="field";name=f.contents})
          | Some fsig ->
              let ty_t = Types.subst_mult fsig.fs_type_args ta fsig.fs_field_type in
              let t = aux_term ve (Some ty_t) t in
              (((f.contents, path), (tyn, ta)), t)
          end
        in
        (TRecSet (t, List.map do_one ftl), ty)
    | Pterm_typed (t, ty) ->
        let ty = TypingEnv.ptyp_to_typ tvars ty in
        let t = aux_term ve (Some ty) t in
        Option.iter (check_type t.tloc deps_te te ty) tyo;
        (t.tdesc, t.ttyp)
    end
  and aux_skel (ve:variable_env) (s:pskeleton) (tyo:typ option):
      skeleton =
    (fun (sk', ty) ->
      Option.iter (check_type s.loc deps_te te ty) tyo ;
      ({sdesc=sk';sloc=s.loc;styp=ty}))
    begin match s.contents with
    | Pskel_return t ->
        let t = aux_term ve tyo t in
        (Return t, t.ttyp)
    | Pskel_branching [] when tyo = None ->
        begin match tyo with
        | None -> type_error s.loc EmptyBranching
        | Some ty -> (Branching (ty, []), ty)
        end
    | Pskel_branching sl ->
        if List.length sl = 1 then
          type_warning warn s.loc OneBranch;
        let sl = List.map (fun s -> aux_skel ve s tyo) sl in
        let tyl = List.map (fun s -> s.styp) sl in
        let output_type =
          merge_types ~tyo deps_te te "branching" s.loc tyl in
        (Branching (output_type, sl), output_type)
    | Pskel_match (_, []) when tyo = None -> type_error s.loc EmptyMatch
    | Pskel_match (sm, psl) ->
        let sm = aux_skel ve sm None in
        let tys = sm.styp in
        let do_one (p, s) =
          let loc = p.loc in
          let (pve, p) = ppattern_to_pattern deps_te te tvars (Some tys) p in
          let ve = extend ve pve in
          let s = aux_skel ve s tyo in
          (((p, loc), s), s.styp)
        in
        let psl, tyl = List.map do_one psl |> List.split in
        let output_type =
          merge_types ~tyo deps_te te "pattern matching" s.loc tyl in
        (* check exhaustivity and redundancy *)
        let () =
          let pl = List.map fst psl in
          let _ =
            (* pl is recombined in the wrong order but it doesn't matter to the
               algorithm. *)
            List.fold_left (fun pl (p, loc) ->
              if not (useful deps_te te pl [p]) then
                type_warning warn loc RedundantPattern ;
                [p] :: pl) [] pl
          in
          let wild = {pdesc=PWild;ploc=ghost_loc;ptyp=tys} in
          if useful deps_te te (List.map (fun (p,_) -> [p]) pl) [wild] then
            type_warning warn s.loc NotExhaustive
        in
        (Match (sm, List.map (Pair.map_fst fst) psl), output_type)
    | Pskel_letin (Some p, s1, s2) ->
        let s1 = aux_skel ve s1 None in
        let (pve, p) = ppattern_to_pattern deps_te te tvars (Some s1.styp) p in
        let ve = extend ve pve in
        let s2 = aux_skel ve s2 tyo in
        check_used warn pve s2 ;
        (LetIn (NoBind, p, s1, s2), s2.styp)
    | Pskel_letin (None, s1, s2) ->
        let s1 = aux_skel ve s1 (Some {tydesc=Product [];tyloc=ghost_loc}) in
        let s2 = aux_skel ve s2 tyo in
        let p = {pdesc=PTuple []; ploc=(snd s1.sloc, fst s2.sloc);ptyp=
          {tydesc=Product [];tyloc=ghost_loc}} in
        (LetIn (NoBind, p, s1, s2), s2.styp)
    | Pskel_letop ({contents=Symbol sym;loc=symloc}, p, s1, s2) ->
        let b =
          begin match SMap.find_opt sym te.te_binders with
          | None ->
              type_error symloc (UnboundBinder sym)
          | Some b ->
              mkloc symloc (Percent b)
          end
        in
        let psk =
          {loc=s.loc;
          contents=Pskel_letop (b, p, s1, s2)}
        in
        let sk = aux_skel ve psk tyo in
        let ty = sk.styp in
        begin match sk.sdesc with
        | LetIn (TermBind (k, b, ta, tyb), p, s1, s2) ->
            (LetIn (SymbolBind (sym, k, b, ta, tyb), p, s1, s2), ty)
        | _ -> assert false (* impossible *)
        end
    | Pskel_letop ({contents=Percent bind;loc=bloc}, p, s1, s2) ->
        (* Type of [bind]: [(a, b → c) → d]. *)
        let k, ta, a, b, c, d =
          let (_, k, ta, typ, _) =
            TypingEnv.te_get deps_te te TypingEnv.te_terms bind.contents
          in
          begin match (unalias_rec deps_te te typ).tydesc with
          | Arrow ({tydesc=Product [a; {tydesc=Arrow (b, c);_}];_}, d) ->
              k, ta, a, b, c, d
          | _ -> type_error bloc (NotBindType {name=fst bind.contents; typ})
          end
        in
        (* Type s1 → tys1*)
        let s1_typed = aux_skel ve s1 None in
        let tys1 = s1_typed.styp in
        (* Unify tys1 with a *)
        let map =
          begin match unify deps_te te a tys1 with
          | None -> type_error s1.loc (WrongType {given=tys1; expected=a})
          | Some map -> map
          end
        in
        (* if the letin is a ; then we expect b to be () so we unify *)
        let map =
          begin
            match p with
            | None ->
              let gloc tydesc = {tydesc;tyloc=ghost_loc} in
              let map_semi =
                begin match
                  unify deps_te te b {tydesc=Product [];tyloc=ghost_loc} with
                | None ->
                    let given =
                      gloc @@ Arrow (
                        gloc @@ Product [a; gloc @@ Arrow(b, c)], d) in
                    let expected =
                      gloc @@ Arrow (
                        gloc @@ Product [
                          gloc @@ Variable "_";
                          gloc @@ Arrow(
                            gloc @@ Product [],
                            gloc @@ Variable "_")],
                        gloc @@ Variable "_") in
                    type_error s2.loc (WrongType {given; expected})
                | Some map -> map
                end
              in
              let consistent name typ1 typ2 =
                if type_equals deps_te te typ1 typ2 then Some typ1 else
                  type_error bloc (VarUnification {name;typ1;typ2})
              in
              SMap.union consistent map map_semi
            | Some p ->
              (* Check that b's type variables are inferred *)
              SSet.iter (fun v ->
                begin match SMap.find_opt v map with
                | None -> type_error p.loc CannotInfer
                | Some _ -> ()
                end) (type_variables b) ;
              map
          end
        in
        (* specialize b using inferred type variables -> b_spec *)
        let b_spec =
          let vars, types = List.split (SMap.bindings map) in
          Types.subst_mult vars types b
        in
        (* assign type b_spec to p → pve*)
        let (pve, p) =
          begin
            match p with
            | Some p -> ppattern_to_pattern deps_te te tvars (Some b_spec) p
            | None -> SMap.empty, {pdesc=PTuple []; ptyp={tydesc=Product
            [];tyloc=ghost_loc}; ploc=ghost_loc}
            end
        in
        let ve = extend ve pve in
        (* type s2 in ve = (old ve)+pve → tys2 *)
        let s2_typed = aux_skel ve s2 None in
        let tys2 = s2_typed.styp in
        check_used warn pve s2_typed ;
        (* unify tys2 with c *)
        let map2 =
          begin match unify deps_te te c tys2 with
          | None -> type_error s2.loc (WrongType {given=tys2; expected=c})
          | Some map -> map
          end
        in
        let map =
          let consistent name typ1 typ2 =
            if type_equals deps_te te typ1 typ2 then Some typ1 else
              type_error bloc (VarUnification {name;typ1;typ2})
          in
          SMap.union consistent map map2
        in
        (* Check that d's type variables are inferred *)
        SSet.iter (fun v ->
          begin match SMap.find_opt v map with
          | None -> type_error s.loc CannotInfer
          | Some _ -> ()
          end) (type_variables d) ;
        (* specialize d using inferred type variables -> d_spec *)
        let d_spec =
          let vars, types = List.split (SMap.bindings map) in
          Types.subst_mult vars types d
        in
        (* list type variables *)
        let ta = List.map (fun x ->
          begin match SMap.find_opt x map with
          | None -> {tydesc=Product [];tyloc=ghost_loc}
          (* if a type variable is not inferred, then it
            does not belong to the signature, so it doesn't matter what we
            return *)
          | Some ty -> ty
          end) ta
        in
        (* result is of type d_spec *)
        let gloc tydesc = {tydesc;tyloc=ghost_loc} in
        let bind_ty =
          gloc @@ Arrow (
            gloc @@ Product [tys1; gloc @@ Arrow (b_spec, tys2)], d_spec)
        in
        let binder =TermBind (k, bind.contents, ta, bind_ty) in
        (LetIn (binder, p, s1_typed, s2_typed), d_spec)
    | Pskel_exists typ ->
        let typ = TypingEnv.ptyp_to_typ tvars typ in
        (Exists typ, typ)
    | Pskel_apply (f, x) ->
        let f = aux_term ve None f in
        let x = aux_term ve None x in
        begin match unalias deps_te te f.ttyp with
        | {tydesc=Arrow (tyx', output_type);_} ->
            check_type x.tloc deps_te te x.ttyp tyx' ;
            (Apply (f, x), output_type)
        | ty -> type_error f.tloc (NotAnArrow ty)
        end
    | Pskel_typed (s, ty) ->
        let ty = TypingEnv.ptyp_to_typ tvars ty in
        let s = aux_skel ve s (Some ty) in
        Option.iter (check_type s.sloc deps_te te s.styp) tyo;
        (s.sdesc, s.styp)
    end
  in
  fun (ty: typ): (pterm -> term) -> aux_term SMap.empty (Some ty)



(**************************************************)
(*            Well-formedness checkers            *)
(**************************************************)

let check_aliases te =
  let module StrGraph = Graph.Persistent.Digraph.Concrete(struct
    include String
    let hash = Hashtbl.hash
    end) in
  (* compute a graph of alias dependencies between types
     edge [a -> b] means type [a] is an alias referring to [b]
   *)
  let rec aliases_in accu ty =
    let aliases_in_mult = List.fold_left aliases_in in
    begin match ty.tydesc with
    | UserType ((x, None), tyl) ->
        SSet.add x (aliases_in_mult accu tyl)
    | UserType ((_, Some _), tyl) (* files are ordered so no cycle is possible
    with aliases from another file *) ->
        aliases_in_mult accu tyl
    | Product tyl ->
        aliases_in_mult accu tyl
    | Arrow (i, o) ->
        aliases_in (aliases_in accu i) o
    | Variable _ -> accu
    end
  in
  let graph =
    SMap.fold
      (fun ty_name (_loc, _ta, ty) graph ->
        SSet.fold
          (fun x g -> StrGraph.add_edge g ty_name x)
          (aliases_in SSet.empty ty) graph)
      te.TypingEnv.te_aliases StrGraph.empty
  in
  (* check whether graph has cycle *)
  let module SCC = Graph.Components.Make(StrGraph) in
  let scc = SCC.scc_list graph in
  List.iter (fun vlist ->
    begin match vlist with
    | [] -> assert false (* makes no sense *)
    | [ ty ] ->
        (* one type only, check whether it refers to itself *)
        if List.mem ty (StrGraph.succ graph ty) then
          let (loc, _, _) = SMap.find ty te.te_aliases in
          type_error loc (Cycle {typ="alias";cycle=[ty]})
        else () (* everything is fine *)
    | ty1 :: _ ->
        let ty2 = Option.get (Util.last vlist) in
        (* there is a cycle, but maybe not in [vlist]'s order *)
        let module Weight = struct
          type edge = StrGraph.edge
          include Int (* weight is an integer *)
          let weight _ = 1 (* weight of an edge is 1 *)
        end in
        let module Dijkstra = Graph.Path.Dijkstra(StrGraph)(Weight) in
        let (path1, _weight_of_said_path) =
          Dijkstra.shortest_path graph ty1 ty2 in
        let (path2, _weight_of_said_path) =
          Dijkstra.shortest_path graph ty2 ty1 in
        let cycle = List.map fst path2 @ List.map fst path1 in
        let (loc, _, _) = SMap.find ty2 te.te_aliases in
        type_error loc (Cycle {typ="alias";cycle})
    end) scc



(**************************************************)
(*            Make skeletal semantics             *)
(**************************************************)

let make_ss_types (te:TypingEnv.t) =
  SMap.mapi (fun typ (com, k, ta) ->
    begin match k with
    | Variant ->
        let csl = SMap.find typ te.te_variants in
        (com, TDVariant (ta, csl))
    | Record ->
        let fsl = SMap.find typ te.te_records in
        (com, TDRecord (ta, fsl))
    | Alias ->
        let (_, ta, ty) = SMap.find typ te.te_aliases in
        (com, TDAlias (ta, ty))
    | Unspec ->
        (com, TDUnspec (List.length ta))
    end) te.te_types

let make_ss_terms
    (warn: int list)
    (deps_te: TypingEnv.t SMap.t)
    (te: TypingEnv.t):
      (string option * term_definition) SMap.t =
  SMap.map (fun (com, k, ta, (ty:typ), t) ->
    begin match t with
    | None ->
        assert (k = Nonspecified) ;
        (com, (ta, ty, None))
    | Some t ->
        assert (k = Specified) ;
        let t = pterm_to_term warn deps_te te (SSet.of_list ta) ty t in
        check_type t.tloc deps_te te ty t.ttyp ;
        (com, (ta, ty, Some t))
    end) te.te_terms


let make_ss_binders
    (deps_te: TypingEnv.t SMap.t)
    (te: TypingEnv.t) =
  let open TypingEnv in
  SMap.mapi (fun sym x ->
    let (_, k, _, ty, _) =
      te_get deps_te te te_terms x.contents in
    begin match unalias_rec deps_te te ty with
    | {tydesc=Arrow ({tydesc=Product [_; {tydesc=Arrow (_, _);_}];_}, _);_} -> ()
    | _ -> type_error x.loc (NotBindType {name=sym; typ=ty})
    end ;
    (k, x.contents)) te.te_binders

let make_ss_includes deps te =
  List.fold_left (fun m x ->
    let xsem = match SMap.find_opt x.contents deps with
    | None -> type_error x.loc (UnboundFile x.contents)
    | Some xsem -> xsem in
    SMap.add x.contents xsem m) SMap.empty te.TypingEnv.te_includes

let make_skeletal_semantics
    ~(warn:int list)
    (deps: TypedAST.t SMap.t)
    (deps_te: TypingEnv.t SMap.t)
    (te: TypingEnv.t):
      TypedAST.t =
  let ss_order = te.te_order in
  let ss_types = make_ss_types te in
  let ss_terms = make_ss_terms warn deps_te te in
  let ss_binders = make_ss_binders deps_te te in
  let ss_includes = make_ss_includes deps te in
  { ss_order; ss_types; ss_terms; ss_binders ; ss_includes }

(** This function transforms an untyped AST, straight out of the parser, into a
    typed AST, ready for consumption. *)
let type_program ?(warn=Errors.all_warnings) deps prog =

  (* Compute typing environment for dependencies. *)
  let deps_te = SMap.map (fun ss ->
    TypingEnv.te_of_ss ss) deps in

  (* Create typing environement *)
  let te = TypingEnv.create deps_te prog in

  (* check that aliases aren't cyclical *)
  let () = check_aliases te in

  (* Compute skeletal semantics *)
  let sem = make_skeletal_semantics ~warn deps deps_te te in
  sem
