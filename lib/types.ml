(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

open TypedAST

let rec subst_type x s ty =
  {ty with tydesc =
  begin match ty.tydesc with
  | Variable y when x = y -> s.tydesc
  | Variable y -> Variable y
  | UserType (s', args) ->
      UserType (s', List.map (subst_type x s) args)
  | Product l -> Product (List.map (subst_type x s) l)
  | Arrow (s1, s2) -> Arrow (subst_type x s s1, subst_type x s s2)
  end}

let subst_mult l1 l2 ty =
  let () = if List.length l1 <> List.length l2 then
      raise (Invalid_argument "subst_mult") in
  let tae = List.combine l1 l2 in
  let rec aux ty =
    {ty with tydesc =
      begin match ty.tydesc with
      | Variable y ->
          begin match List.assoc_opt y tae with
          | Some ty -> ty.tydesc
          | _ -> Variable y
          end
      | UserType (s', args) ->
          UserType (s', List.map aux args)
      | Product l -> Product (List.map aux l)
      | Arrow (s1, s2) -> Arrow (aux s1, aux s2)
      end
    }
  in aux ty
