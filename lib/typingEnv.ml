(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

open Util
open ParsedAST
open TypedAST

  type te_type_decl = string option * type_kind * string list
  type te_variant_decl = constructor_signature list
  type te_record_decl = field_signature list
  type te_alias_decl = location * string list * typ
  type te_constructor_decl = location * constructor_signature
  type te_field_decl = location * field_signature
  type te_term_decl = string option * term_kind * string list * typ * pterm option
  type te_binder_decl = string TypedAST.with_path with_loc

type t = {
  te_order : decl_kind list ;
  te_types : te_type_decl SMap.t ;
  te_variants : te_variant_decl SMap.t ;
  te_records : te_record_decl SMap.t ;
  te_aliases : te_alias_decl SMap.t ;
  te_constructors : te_constructor_decl SMap.t ;
  te_fields : te_field_decl SMap.t ;
  te_terms : te_term_decl SMap.t ;
  te_binders : te_binder_decl SMap.t ;
  te_includes: string with_loc list
}

let te_types        te = te.te_types
let te_variants     te = te.te_variants
let te_records      te = te.te_records
let te_aliases      te = te.te_aliases
let te_constructors te = te.te_constructors
let te_fields       te = te.te_fields
let te_terms        te = te.te_terms
let te_binders      te = te.te_binders

let empty_te =
  { te_order        = []
  ; te_types        = SMap.empty
  ; te_variants     = SMap.empty
  ; te_records      = SMap.empty
  ; te_aliases      = SMap.empty
  ; te_constructors = SMap.empty
  ; te_fields       = SMap.empty
  ; te_terms        = SMap.empty
  ; te_binders      = SMap.empty
  ; te_includes     = [] }

let get_te_path
    (te_deps: t SMap.t)
    (te:t)
    (path: string option):
      t =
    begin match path with
    | None -> te
    | Some f -> SMap.find f te_deps
    end

let te_get
    (te_deps: t SMap.t)
    (te:t)
    (field: t -> 'a SMap.t)
    ((x, path): string with_path):
      'a =
  let new_te = get_te_path te_deps te path in
  SMap.find x (field new_te)




let rec ptyp_to_typ (tvars: SSet.t) (ty:ParsedAST.ptyp) =
  {tyloc = ty.loc ; tydesc =
  begin match ty.contents with
  | Ptyp_constructor ({contents=(x, path);_}, a) ->
      let is_var =
        Option.is_none path (* no path *)
        && SSet.mem x tvars (* in the currently defined type variables *)
      in
      begin if is_var then
        begin if List.is_empty a then
          Variable x
        else
          (* Type variable with type arguments*)
          Errors.type_error ty.loc (TypeVariablePoly x)
        end
      else
        UserType ((x, path), List.map (ptyp_to_typ tvars) a)
      end
  | Ptyp_product tyl ->
      Product (List.map (ptyp_to_typ tvars) tyl)
  | Ptyp_arrow (tyi, tyo) ->
      Arrow (ptyp_to_typ tvars tyi, ptyp_to_typ tvars tyo)
  end}

let create _deps ((reqs, past):(string with_loc list * ParsedAST.t)) =
  let kind decl =
    begin match decl with
    | Decl_nonspec (_, {contents=ty;_}, _)
    | Decl_variant (_, {contents=ty;_}, _, _)
    | Decl_record (_, {contents=ty;_}, _, _)
    | Decl_alias (_, {contents=ty;_}, _, _)
            -> TypeDecl ty
    | Decl_pterm (_, {contents=t;_}, _, _, _, _) -> TermDecl t
    | Decl_binder (sym, _) -> BinderDecl sym
    | Decl_comment com -> Comment com
    end
  in
  let te_order = List.map kind past in
  let tryadd typ name cont map =
    begin match SMap.find_opt name.contents map with
    | Some (oldloc, _) ->
      Errors.type_error2 name.loc oldloc (Redefinition {typ;name=name.contents})
    | None ->
      SMap.add name.contents (name.loc, cont) map
    end
  in
  let te_types,
      te_variants,
      te_records,
      te_aliases,
      te_terms,
      te_binders,
      te_constructors,
      te_fields =
    List.fold_left (fun (typs,vars,recs,als,ts,binds,cs,fs) decl ->
      begin match decl with
      | Decl_nonspec (comment, {contents=ty_name;_}, number_args) ->
          let typs = SMap.add ty_name
            (comment, Unspec, List.init number_args (Fun.const "_"))
            typs in
          (typs, vars, recs, als, ts, binds, cs, fs)
      | Decl_variant (comment, {contents=ty_name;_}, ty_args, constructors) ->
          let typs = SMap.add ty_name (comment, Variant, ty_args) typs in
          let csl = List.map (fun (ccomment, cname, cinput) ->
            let csig = {
              cs_name = cname.contents ;
              cs_type_args = ty_args ;
              cs_input_type = ptyp_to_typ (SSet.of_list ty_args) cinput ;
              cs_variant_type = ty_name ;
              cs_comment = Option.map contents ccomment
            } in
            (cname, csig)) constructors in
          let vars = SMap.add ty_name (List.map snd csl) vars in
          let cs = List.fold_left (fun cs (cname, csig) ->
            tryadd "constructor" cname csig cs) cs csl in
          (typs, vars, recs, als, ts, binds, cs, fs)
      | Decl_record (comment, {contents=ty_name;_}, ty_args, fields) ->
          let typs = SMap.add ty_name (comment, Record, ty_args) typs in
          let fsl = List.map (fun (fcomment, fname, ftype) ->
            let fsig = {
              fs_name = fname.contents ;
              fs_type_args = ty_args ;
              fs_field_type = ptyp_to_typ (SSet.of_list ty_args) ftype ;
              fs_record_type = ty_name ;
              fs_comment = Option.map contents fcomment
            } in
            (fname, fsig)) fields in
          let recs = SMap.add ty_name (List.map snd fsl) recs in
          let fs = List.fold_left (fun fs (fname, fsig) ->
            tryadd "field" fname fsig fs) fs fsl in
          (typs, vars, recs, als, ts, binds, cs, fs)
      | Decl_alias (comment, {contents=ty_name;loc}, ty_args, alias) ->
          let typs = SMap.add ty_name (comment, Alias, ty_args) typs in
          let als = SMap.add ty_name
            (loc, ty_args, ptyp_to_typ (SSet.of_list ty_args) alias) als in
          (typs, vars, recs, als, ts, binds, cs, fs)
      | Decl_pterm (comment, tname, ty_args, args, ty_ret, spec) ->
          let exception Untypable in
          let rec type_ppattern p =
            begin match p.contents with
            | Ppat_typed (_, ty) -> ptyp_to_typ (SSet.of_list ty_args) ty
            | Ppat_any | Ppat_variable _ -> raise Untypable
            | Ppat_tuple pl ->
                {tyloc=ghost_loc; tydesc=Product (List.map type_ppattern pl)}
            | Ppat_record _ | Ppat_constructor _ ->
                raise Untypable (* FIXME: low effort *)
            | Ppat_or (p1, p2) ->
                begin try type_ppattern p1 with
                  Untypable -> type_ppattern p2
                end
            end
          in
          let t_ty, spec =
            let rec aux args ty_ret spec: typ * pskeleton option =
              begin match args with
              | [] -> (* no args, so spec is a term (or parser would have
              failed *)
                  ptyp_to_typ (SSet.of_list ty_args) ty_ret, spec
              | a :: q ->
                  begin match aux q ty_ret spec with
                  | t_ty', None ->
                      let ty_a = try type_ppattern a with
                        Untypable ->
                          Errors.type_error a.loc CannotInferPatternType
                      in
                      {tydesc=Arrow (ty_a, t_ty');tyloc=ghost_loc}, None
                  | t_ty', Some sk ->
                      let ty_a = try type_ppattern a with
                        Untypable ->
                          Errors.type_error a.loc CannotInferPatternType
                      in
                      {tydesc=Arrow (ty_a, t_ty'); tyloc=ghost_loc},
                        Some {
                          contents = Pskel_return {
                            contents= Pterm_function (a, sk);
                            loc=(fst a.loc, snd sk.loc)} ;
                          loc =(fst a.loc, snd sk.loc)} ;
                  end
              end
            in
            begin match aux args ty_ret spec with
            | ty, None -> ty, None
            | ty, Some {contents=Pskel_return t;_} -> ty, Some t
            | _, Some t -> Errors.type_error t.loc SkeletonInsteadTerm
            end
          in
          let t =
          ( comment,
            (if Option.is_none spec then Nonspecified else Specified),
            ty_args,
            t_ty,
            spec
          )
          in
          let ts = SMap.add tname.contents t ts in
          (typs, vars, recs, als, ts, binds, cs, fs)
      | Decl_binder (sym, t) ->
          let binds = SMap.add sym t binds in
          (typs, vars, recs, als, ts, binds, cs, fs)
      | Decl_comment _ ->
          (typs, vars, recs, als, ts, binds, cs, fs)
      end)
      ( SMap.empty,
        SMap.empty,
        SMap.empty,
        SMap.empty,
        SMap.empty,
        SMap.empty,
        SMap.empty,
        SMap.empty ) past
  in
  {
    te_order ; te_types ; te_variants ; te_records ; te_aliases ;
    te_constructors ; te_fields ; te_terms ; te_binders ; te_includes = reqs}







let te_of_ss ss: t =
  let open ParsedAST in
  (* we have to add ghost location in some places, since te can be used for
     typing and needs location to raise typing errors. Since it is not use for
     this purpose here, ghost locations are ok *)
  let rec untype_type (t:typ): ptyp =
    mkloc t.tyloc
    begin match t.tydesc with
    | Variable x ->
        Ptyp_constructor (mkloc ghost_loc (x, None), [])
    | UserType (n, ta) ->
        Ptyp_constructor (mkloc ghost_loc n, List.map untype_type ta)
    | Arrow (t1, t2) ->
        Ptyp_arrow (untype_type t1, untype_type t2)
    | Product tl ->
        Ptyp_product (List.map untype_type tl)
    end
  in
  let rec untype_pattern (p:pattern): ppattern =
    mkloc ghost_loc
    begin match p.pdesc with
    | PWild -> Ppat_any
    | PVar x -> Ppat_variable x
    | PConstr ((c, _), p) -> Ppat_constructor (fst c, untype_pattern p)
    | PTuple pl -> Ppat_tuple (List.map untype_pattern pl)
    | PRecord fpl -> Ppat_record (List.map (Pair.map (fun (f, _) -> mkloc
        ghost_loc (fst f)) untype_pattern) fpl)
    | POr (p1, p2) -> Ppat_or (untype_pattern p1, untype_pattern p2)
    | PType (p, ty) -> Ppat_typed (untype_pattern p, untype_type ty)
    end
  in
  let rec untype_term (t:term): pterm =
    mkloc t.tloc
    begin match t.tdesc with
    | TVar (LetBound (v, _)) ->
        Pterm_variable (mkloc t.tloc (v, None), [])
    | TVar (TopLevel (_, x, ta, _)) ->
        Pterm_variable (mkloc t.tloc x, List.map untype_type ta)
    | TConstr ((c, (_, ta)), t) ->
        Pterm_constructor (mkloc ghost_loc c, List.map untype_type ta, untype_term t)
    | TTuple tl ->
        Pterm_tuple (List.map untype_term tl)
    | TFunc (p, s) -> Pterm_function (untype_pattern p, untype_skel s)
    | TField (t, (f, _)) -> Pterm_getfield (untype_term t, mkloc ghost_loc (fst f))
    | TNth (t, _, n) -> Pterm_getnth (untype_term t, n)
    | TRecMake [] -> assert false
    | TRecMake (((((_f1name, path), (_c, _ta)), _t) :: _) as ftl) ->
        let ftl = List.map (Pair.map
          (fun (f, _) -> mkloc ghost_loc (fst f)) untype_term
        ) ftl in
        Pterm_recmake (ftl, path)
    | TRecSet (t, ftl) ->
        let ftl = List.map (Pair.map
          (fun (f, _) -> mkloc ghost_loc (fst f)) untype_term
        ) ftl in
        Pterm_recset (untype_term t, ftl)
    end
  and untype_skel (s:skeleton): pskeleton =
    mkloc s.sloc
    begin match s.sdesc with
    | Branching (_, sl) ->
        Pskel_branching (List.map untype_skel sl)
    | Match (s, psl) ->
        Pskel_match (untype_skel s,
          List.map (Pair.map untype_pattern untype_skel) psl)
    | LetIn (b, p, s1, s2) ->
        let p = if p.pdesc = PTuple [] then None else Some (untype_pattern p) in
        let s1 = untype_skel s1 in
        let s2 = untype_skel s2 in
        begin match b with
        | NoBind ->
            Pskel_letin (p, s1, s2)
        | SymbolBind (sym, _, _, _, _) ->
            Pskel_letop (mkloc ghost_loc (Symbol sym),
              p, s1, s2)
        | TermBind (_, t, _, _)->
            Pskel_letop (mkloc ghost_loc (Percent (mkloc ghost_loc t)),
              p, s1, s2)
        end
    | Exists ty ->
        Pskel_exists (untype_type ty)
    | Return t -> Pskel_return (untype_term t)
    | Apply (f, x) -> Pskel_apply (untype_term f, untype_term x)
    end
  in
  let te_order = ss.ss_order in
  let te_types: te_type_decl SMap.t=
    SMap.map (fun (com, td) ->
      let k, ta =
        begin match td with
        | TDUnspec n         -> Unspec, List.init n (Format.sprintf "a%d")
        | TDVariant (ta, _)  -> Variant, ta
        | TDRecord (ta, _)   -> Record, ta
        | TDAlias (ta, _) -> Alias, ta
        end
      in
      com, k, ta) ss.ss_types in
  let te_variants, te_records, te_aliases =
    SMap.fold (fun name (_, td) (v, r, a) ->
      begin match td with
      | TDUnspec _ -> (v, r, a)
      | TDVariant (_, cl) -> (SMap.add name cl v, r, a)
      | TDRecord (_, fl) -> (v, SMap.add name fl r, a)
      | TDAlias (ta, ty) -> (v, r, SMap.add name (Util.ghost_loc, ta, ty) a)
      end) ss.ss_types (SMap.empty, SMap.empty, SMap.empty)
  in
  let te_constructors =
    SMap.fold (fun _ cl cl_all ->
      List.fold_left (fun m cs ->
        SMap.add cs.cs_name (ghost_loc, cs) m) cl_all cl) te_variants SMap.empty
  in
  let te_fields =
    SMap.fold (fun _ fl fl_all ->
      List.fold_left (fun m fs ->
        SMap.add fs.fs_name (ghost_loc, fs) m) fl_all fl) te_records SMap.empty in
  let te_terms = SMap.map (fun (com, (ta, ty, t)) ->
    let k = Option.fold ~none:Nonspecified ~some:(Fun.const Specified) t in
    (com, k, ta, ty, Option.map untype_term t)) ss.ss_terms in
  let te_binders = SMap.map (fun (_, x) -> mkloc ghost_loc x) ss.ss_binders in
  let te_includes = SMap.bindings ss.ss_includes |> List.map fst |> List.map
  (mkloc ghost_loc) in
  { te_order ; te_types ; te_variants ; te_records ; te_aliases ; te_terms ;
  te_constructors ; te_fields ; te_binders ; te_includes }


(* TODO: update/remove

(* Check that the number of given type arguments fits *)
let check_type_args loc typ name got expected =
  let ng = List.length got in
  let ne = List.length expected in
  if ng <> ne then
    Errors.type_error loc (WrongNumberArgs {typ;name;expected=ne;given=ng})


let rec unalias_te
    (te_deps:t SMap.t)
    (te: t)
    (ty: typ):
      typ =
  begin match (ty : typ) with
  | UserType (n, ta_got) ->
      let (_, kind, _) = te_get te_deps te te_types n in
      if kind <> Alias then ty else
      let (_, ta, ty) = te_get te_deps te te_aliases n in
      check_type_args ghost_loc "type alias" (name n) ta_got ta ;
      unalias_te te_deps te (Types.subst_mult ta ta_got ty)
  | _ -> ty
  end

let rec unalias_rec_te
    (te_deps:t SMap.t)
    (te: t)
    (ty: typ):
      typ =
  begin match (ty : typ) with
  | Variable x -> Variable x
  | UserType (n, ta) ->
      let (_, kind, _) = te_get te_deps te te_types n in
      if kind <> Alias
      then UserType (n, List.map (unalias_rec_te te_deps te) ta)
      else unalias_rec_te te_deps te (unalias_te te_deps te ty)
  | Arrow (i, o) ->
      Arrow (unalias_rec_te te_deps te i, unalias_rec_te te_deps te o)
  | Product tyl ->
      Product (List.map (unalias_rec_te te_deps te) tyl)
  end


let rec type_equals_te
    (te_deps: t SMap.t)
    (te: t)
    (ty1: typ)
    (ty2: typ):
      bool =
  begin match unalias_te te_deps te ty1, unalias_te te_deps te ty2 with
  | UserType (n1, ta1), UserType (n2, ta2) ->
      Pair.equal String.equal (Option.equal String.equal) n1 n2 &&
      List.compare_lengths ta1 ta2 = 0 &&
      List.for_all2 (type_equals_te te_deps te) ta1 ta2
  | UserType _, _ | _, UserType _ -> false
  | Arrow (i1, o1), Arrow (i2, o2) ->
      type_equals_te te_deps te i1 i2 &&
      type_equals_te te_deps te o1 o2
  | Arrow _, _ | _, Arrow _ -> false
  | Variable x, Variable y -> String.equal x y
  | Variable _, _ | _, Variable _ -> false
  | Product tyl1, Product tyl2 ->
      List.compare_lengths tyl1 tyl2 = 0 &&
      List.for_all2 (type_equals_te te_deps te) tyl1 tyl2
  end





(** [unify_te _ _ _ ty1 ty2] returns [Some m], if [m] is the minimal mapping of
    type variables to types, such that every variable from [ty1] has a mapping
    in [m], and [ty1] with the substitutions from [m] is equal to [ty2].
    [unify ss ty1 ty2] returns [None] if there is no such mapping.
    For instance,
    - [unify (a -> a) (int -> int) = Some [a → int]]
    - [unify (a -> a) (int -> bool) = None] *)
let unify_te te_deps te ty1 ty2 =
  let (let$) x f =
    begin match x with
    | None -> None
    | Some y -> f y
    end
  in
  let rec aux ty1 ty2 accu =
    begin match unalias_te te_deps te ty1, unalias_te te_deps te ty2 with
    | Variable a, _ ->
        begin match SMap.find_opt a accu with
        | None -> Some (SMap.add a ty2 accu)
        | Some ty' -> if type_equals_te te_deps te ty' ty2 then Some accu else None
        end
    | Arrow (ty1i, ty1o), Arrow (ty2i, ty2o) ->
        let$ accu = aux ty1i ty2i accu in
        aux ty1o ty2o accu
    | Arrow _, _ -> None
    | UserType (s1, a1), UserType (s2, a2) ->
      if Pair.equal String.equal (Option.equal String.equal) s1 s2 (* same type *)
      then aux (Product a1) (Product a2) accu (* same type arguments *)
      else None
    | UserType _, _ -> None
    | Product [], Product [] -> Some accu
    | Product (a1 :: q1), Product (a2 :: q2) ->
        let$ accu = aux a1 a2 accu in
        aux (Product q1) (Product q2) accu
    | Product _, _ -> None
    end
  in
  aux ty1 ty2 SMap.empty

let rec ptyp_to_typ (ty:ParsedAST.ptyp) =
  begin match ty.contents with
  | Ptyp_constructor ({contents={abs=Local;obj;_};_}, []) ->
      (* Type variable *)
      Variable obj
  | Ptyp_constructor ({contents={abs=Local;obj;_};_}, _ :: _) ->
      (* Type variable with type arguments*)
      Errors.type_error ty.loc (TypeVariablePoly obj)
  | Ptyp_constructor ({contents=x;_}, a) ->
      UserType (x, List.map ptyp_to_typ a)
  | Ptyp_product tyl ->
      Product (List.map ptyp_to_typ tyl)
  | Ptyp_arrow (tyi, tyo) ->
      Arrow (ptyp_to_typ tyi, ptyp_to_typ tyo)
  end



let rec shape_of_ss name ss: Shape.shape =
  let terms =
    List.map (fun (x, _) ->
      Tree.Leaf (Shape.KTerm, x)) (SMap.bindings ss.ss_terms)
  in
  let types =
    List.map (fun (x, _) ->
      Tree.Leaf (Shape.KType, x)) (SMap.bindings ss.ss_types)
  in
  let fields, constructors =
    SMap.fold (fun _ (_, t) (f, c) ->
      begin match t with
      | TDRecord (_, fl) ->
          (List.map (fun fs ->
            Tree.Leaf (Shape.KField, fs.fs_name)) fl @ f, c)
      | TDVariant (_, cl) ->
          (f, List.map (fun cs ->
            Tree.Leaf (Shape.KConstructor, cs.cs_name)) cl @ c)
      | _ -> (f, c)
      end) ss.ss_types ([], [])
  in
  let modules =
    List.map (fun (name, (_, ss)) ->
    shape_of_ss name ss) (SMap.bindings ss.ss_modules)
  in
  Node (name, terms @ types @ fields @ constructors @ modules)

let rec te_of_ss ss: t =
  (* we have to add ghost location in some places, since te can be used for
     typing and needs location to raise typing errors. Since it is not use for
     this purpose here, ghost locations are ok *)
  let rec untype_type (t:typ): ParsedAST.ptyp =
    mkloc ghost_loc
    begin match t with
    | Variable x ->
      let x = {obj=x;abs=Local;rel=Ident x} in
        ParsedAST.Ptyp_constructor (mkloc ghost_loc x, [])
    | UserType (n, ta) ->
        Ptyp_constructor (mkloc ghost_loc n, List.map untype_type ta)
    | Arrow (t1, t2) ->
        Ptyp_arrow (untype_type t1, untype_type t2)
    | Product tl ->
        Ptyp_product (List.map untype_type tl)
    end
  in
  let rec untype_pattern (p:pattern): ParsedAST.ppattern =
    mkloc ghost_loc
    begin match p.pdesc with
    | PWild -> ParsedAST.Ppat_any
    | PVar x -> Ppat_variable x
    | PConstr ((c, _), p) -> Ppat_constructor (c.obj, untype_pattern p)
    | PTuple pl -> Ppat_tuple (List.map untype_pattern pl)
    | PRecord fpl -> Ppat_record (List.map (Pair.map (fun (f, _) -> mkloc
        ghost_loc f.obj) untype_pattern) fpl)
    | POr (p1, p2) -> Ppat_or (untype_pattern p1, untype_pattern p2)
    | PType (p, ty) -> Ppat_typed (untype_pattern p, untype_type ty)
    end
  in
  let rec untype_term (t:term): ParsedAST.pterm =
    mkloc t.tloc
    begin match t.tdesc with
    | TVar (LetBound (v, _)) ->
        let v = {obj=v;abs=Local;rel=Ident v} in
        ParsedAST.Pterm_variable (mkloc t.tloc v, [])
    | TVar (TopLevel (_, x, ta, _)) ->
        ParsedAST.Pterm_variable (mkloc t.tloc x, List.map untype_type ta)
    | TConstr ((c, (_, ta)), t) ->
        Pterm_constructor (mkloc ghost_loc c, List.map untype_type ta, untype_term t)
    | TTuple tl ->
        Pterm_tuple (List.map untype_term tl)
    | TFunc (p, s) -> Pterm_function (untype_pattern p, untype_skel s)
    | TField (t, (f, _)) -> Pterm_getfield (untype_term t, mkloc ghost_loc f)
    | TNth (t, _, n) -> Pterm_getnth (untype_term t, n)
    | TRecMake [] -> assert false
    | TRecMake ((((f1, _), _) :: _) as ftl) ->
        let ftl = List.map (Pair.map
          (fun (f, _) -> mkloc ghost_loc f.obj) untype_term
        ) ftl in
        Pterm_recmake {obj=ftl;rel=Path.Rel.replace f1.rel ftl;abs=f1.abs}
    | TRecSet (t, ftl) ->
        let ftl = List.map (Pair.map
          (fun (f, _) -> mkloc ghost_loc f.obj) untype_term
        ) ftl in
        Pterm_recset (untype_term t, ftl)
    end
  and untype_skel (s:skeleton): ParsedAST.pskeleton =
    mkloc s.sloc
    begin match s.sdesc with
    | Branching (_, sl) ->
        ParsedAST.Pskel_branching (List.map untype_skel sl)
    | Match (s, psl) ->
        ParsedAST.Pskel_match (untype_skel s,
          List.map (Pair.map untype_pattern untype_skel) psl)
    | LetIn (b, p, s1, s2) ->
        let p = if p.pdesc = PTuple [] then None else Some (untype_pattern p) in
        let s1 = untype_skel s1 in
        let s2 = untype_skel s2 in
        begin match b with
        | NoBind ->
            Pskel_letin (p, s1, s2)
        | SymbolBind (sym, _, _, _, _) ->
            Pskel_letop (mkloc ghost_loc (ParsedAST.Symbol sym),
              p, s1, s2)
        | TermBind (_, t, _, _)->
            Pskel_letop (mkloc ghost_loc (ParsedAST.Percent (mkloc ghost_loc t)),
              p, s1, s2)
        end
    | Exists ty ->
        Pskel_exists (untype_type ty)
    | Return t -> Pskel_return (untype_term t)
    | Apply (f, x) -> Pskel_apply (untype_term f, untype_term x)
    end
  in
  let te_order = ss.ss_order in
  let te_types: te_type_decl SMap.t=
    SMap.map (fun (com, td) ->
      let k, ta =
        begin match td with
        | TDUnspec n         -> Unspec, List.init n (Format.sprintf "a%d")
        | TDVariant (ta, _)  -> Variant, ta
        | TDRecord (ta, _)   -> Record, ta
        | TDAlias (ta, _) -> Alias, ta
        end
      in
      com, k, ta) ss.ss_types in
  let te_variants, te_records, te_aliases =
    SMap.fold (fun name (_, td) (v, r, a) ->
      begin match td with
      | TDUnspec _ -> (v, r, a)
      | TDVariant (_, cl) -> (SMap.add name cl v, r, a)
      | TDRecord (_, fl) -> (v, SMap.add name fl r, a)
      | TDAlias (ta, ty) -> (v, r, SMap.add name (Util.ghost_loc, ta, ty) a)
      end) ss.ss_types (SMap.empty, SMap.empty, SMap.empty)
  in
  let te_constructors =
    SMap.fold (fun _ cl cl_all ->
      List.fold_left (fun m cs ->
        SMap.add cs.cs_name cs m) cl_all cl) te_variants SMap.empty
  in
  let te_fields =
    SMap.fold (fun _ fl fl_all ->
      List.fold_left (fun m fs ->
        SMap.add fs.fs_name fs m) fl_all fl) te_records SMap.empty in
  let te_terms = SMap.map (fun (com, (ta, ty, t)) ->
    let k = Option.fold ~none:Nonspecified ~some:(Fun.const Specified) t in
    (com, k, ta, ty, Option.map untype_term t)) ss.ss_terms in
  let te_binders = SMap.map (fun (_, x) -> mkloc ghost_loc x) ss.ss_binders in
  { te_order ; te_types ; te_variants ; te_records ; te_aliases ; te_terms ;
  te_constructors ; te_fields ; te_binders }

*)
