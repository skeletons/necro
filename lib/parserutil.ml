(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

(** [getlines ic] takes all the lines available in the input channel and puts
	 them in a string. **)
let getlines ic =
	let rec f acc =
		try f ((input_line ic) :: acc) with End_of_file -> String.concat "\n" (List.rev acc)
	in f []

let report_error_location lexbuf start_pos end_pos =
	let open Lexing in
	let start_col = start_pos.pos_cnum - start_pos.pos_bol + 1 in
	let end_col = end_pos.pos_cnum - end_pos.pos_bol + 1 in
	if start_pos.pos_lnum = end_pos.pos_lnum then
		begin
      begin try
        Format.eprintf "File %S, line %d, characters %d-%d:@."
        start_pos.pos_fname start_pos.pos_lnum start_col end_col;
        let substring = Lexing.sub_lexeme lexbuf start_pos.pos_bol (end_pos.pos_cnum) in
        Format.eprintf "%4d | %s\n" start_pos.pos_lnum substring;
        Format.eprintf "       %s%s\n"
          (String.init (start_pos.pos_cnum - start_pos.pos_bol) (Fun.const ' '))
          (String.init (end_pos.pos_cnum - start_pos.pos_cnum) (Fun.const '^'))
      with _ -> ()
      end
		end
	else
		Format.eprintf "File %S, lines %d-%d, characters %d-%d:@."
		start_pos.pos_fname start_pos.pos_lnum end_pos.pos_lnum start_col end_col

let parse_from_file parsefunc filename =
	let oc = if filename = "-" then stdin else open_in filename in
	let lexbuf = Lexing.from_channel oc in
	lexbuf.lex_curr_p <- {lexbuf.lex_curr_p with pos_fname = filename} ;
	let r = try
			parsefunc Lexer.token lexbuf
		with
			(Parser.Error | Lexer.Lexing_error _) as e ->
			let errs =
				begin match e with
				| Lexer.Lexing_error s ->
						s
				| Parser.Error ->
					"Syntax error, unexpected token: " ^ Lexing.lexeme  lexbuf
				| _ -> raise e
				end
			in
			let startloc, endloc =
				(Lexing.lexeme_start_p lexbuf, Lexing.lexeme_end_p lexbuf)
			in
			report_error_location lexbuf startloc endloc ;
			Format.eprintf "%s@." errs; exit 1
	in
	close_in oc;
	r

