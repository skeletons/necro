(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

module Errors = Errors

module Pair = Pair

module Util = Util

module ParsedAST = ParsedAST

module Types = Types

module TypedAST = TypedAST

module Interpretation = Interpretation

module Skeleton = Skeleton

module Printer = Printer

(* Auxiliary function *)
let get_basename filename =
  filename
  |> Filename.basename
  |> Filename.remove_extension


let wrap f x =
  begin try f x with
  | Errors.TypeError -> exit 1
  | e ->
    let stack = Printexc.get_backtrace () in
    let msg = Printexc.to_string e in
    Printf.eprintf "Fatal error: %s\n%s" msg stack ;
    exit 1
  end
let wrap2 f x y = wrap (wrap f x) y

let add_path_ss self (sem: TypedAST.t): TypedAST.t =
  let open TypedAST in
  let add_path_item (x, path) =
    let newpath =
      if Option.is_none path then Some self else path
    in (x, newpath)
  in
  let rec add_path_typ (ty: typ): typ =
    {ty with tydesc =
      begin match ty.tydesc with
      | Variable a -> Variable a
      | UserType (x, ta) -> UserType (add_path_item x, List.map add_path_typ ta)
      | Arrow (i, o) -> Arrow (add_path_typ i, add_path_typ o)
      | Product tyl -> Product (List.map add_path_typ tyl)
      end}
  in
  let add_path_cs (cs:constructor_signature): constructor_signature =
    let cs_name = cs.cs_name in
    let cs_comment = cs.cs_comment in
    let cs_type_args = cs.cs_type_args in
    let cs_input_type = add_path_typ cs.cs_input_type in
    let cs_variant_type = cs.cs_variant_type in
    {cs_name; cs_comment; cs_type_args; cs_input_type; cs_variant_type}
  in
  let add_path_fs (fs:field_signature): field_signature =
    let fs_name = fs.fs_name in
    let fs_comment = fs.fs_comment in
    let fs_type_args = fs.fs_type_args in
    let fs_field_type = add_path_typ fs.fs_field_type in
    let fs_record_type = fs.fs_record_type in
    {fs_name; fs_comment; fs_type_args; fs_field_type; fs_record_type}
  in
  let add_path_td (td:type_definition): type_definition =
    begin match td with
    | TDUnspec _ -> td
    | TDVariant (ta, csl) -> TDVariant (ta, List.map add_path_cs csl)
    | TDRecord (ta, fsl) -> TDRecord (ta, List.map add_path_fs fsl)
    | TDAlias (ta, ty) -> TDAlias (ta, add_path_typ ty)
    end
  in
  let add_path_constr (c:constructor): constructor =
    let (cname, (ty, ta)) = c in
    (add_path_item cname, (ty, List.map add_path_typ ta))
  in
  let add_path_field = add_path_constr in
  let add_path_bind b =
    begin match b with
    | NoBind -> NoBind
    | TermBind (k, x, ta, ty) ->
        TermBind (k, add_path_item x, List.map add_path_typ ta, add_path_typ ty)
    | SymbolBind (sym, k, x, ta, ty) ->
        SymbolBind (sym, k, add_path_item x, List.map add_path_typ ta, add_path_typ ty)
    end
  in
  let rec add_path_pattern (p:pattern): pattern =
    {ptyp = add_path_typ p.ptyp; ploc = p.ploc; pdesc =
    begin match p.pdesc with
    | PWild | PVar _ -> p.pdesc
    | PConstr (c, p) -> PConstr (add_path_constr c, add_path_pattern p)
    | PTuple pl -> PTuple (List.map add_path_pattern pl)
    | PRecord fpl -> PRecord (List.map (fun (f, p) ->
        (add_path_field f, add_path_pattern p)) fpl)
    | POr (p1, p2) -> POr (add_path_pattern p1, add_path_pattern p2)
    | PType (p, ty) -> PType (add_path_pattern p, add_path_typ ty)
    end}
  in
  let rec add_path_term (t:term): term =
    {ttyp = add_path_typ t.ttyp; tloc = t.tloc; tdesc =
    begin match t.tdesc with
    | TVar (LetBound _) -> t.tdesc
    | TVar (TopLevel (k, x, ta, ty)) ->
        TVar (TopLevel (k, add_path_item x,
          List.map add_path_typ ta, add_path_typ ty))
    | TConstr (c, t) -> TConstr (add_path_constr c, add_path_term t)
    | TTuple tl -> TTuple (List.map add_path_term tl)
    | TFunc (p, s) -> TFunc (add_path_pattern p, add_path_skel s)
    | TField (t, f) -> TField (add_path_term t, add_path_field f)
    | TNth (t, ta, n) -> TNth (add_path_term t, List.map add_path_typ ta, n)
    | TRecMake ftl -> TRecMake (List.map (fun (f, t) ->
        (add_path_field f, add_path_term t)) ftl)
    | TRecSet (t, ftl) -> TRecSet (add_path_term t, List.map (fun (f, t) ->
        (add_path_field f, add_path_term t)) ftl)
    end}
  and add_path_skel (s:skeleton): skeleton =
    {styp = add_path_typ s.styp; sloc = s.sloc; sdesc =
    begin match s.sdesc with
    | Branching (ty, sl) -> Branching (add_path_typ ty, List.map add_path_skel sl)
    | Match (s, psl) -> Match (add_path_skel s,
        List.map (Pair.map add_path_pattern add_path_skel) psl)
    | LetIn (b, p, s1, s2) ->
        LetIn (add_path_bind b, add_path_pattern p, add_path_skel s1, add_path_skel s2)
    | Exists ty -> Exists (add_path_typ ty)
    | Return t -> Return (add_path_term t)
    | Apply (f, arg) -> Apply (add_path_term f, add_path_term arg)
    end}
  in
  let ss_order = sem.ss_order in
  let ss_binders = Util.SMap.map (fun (k, s) ->
    (k, add_path_item s)) sem.ss_binders in
  let ss_types = Util.SMap.map (fun (com, td) ->
    com, add_path_td td) sem.ss_types in
  let ss_terms = Util.SMap.map (fun (com, (ta, ty, t)) ->
    (com, (ta, add_path_typ ty, Option.map add_path_term t))) sem.ss_terms in
  let ss_includes = sem.ss_includes in
  {ss_order; ss_binders; ss_types; ss_terms; ss_includes}


(* Auxiliary function *)

let parse_sk ~warn m f =
  let reqs, parsed_sem = Parserutil.parse_from_file Parser.main f in
  let fdeps = Util.SMap.filter (fun x _ ->
    List.exists (fun i -> i.Util.contents = x) reqs) m in
  Typer.type_program ~warn fdeps (reqs, parsed_sem)

let parse_and_type_list ?(warn=Errors.all_warnings) deps =
  let () = Printexc.record_backtrace true in
  let map = List.fold_left (fun m f ->
    let self = get_basename f in
    let ss = wrap2 (parse_sk ~warn) m f in
    let ss = add_path_ss self ss in
    Util.SMap.add self ss m) Util.SMap.empty deps in
  List.map (fun y -> Util.SMap.find (get_basename y) map) deps


let parse_and_type ?(warn=Errors.all_warnings) deps file =
  let () = Printexc.record_backtrace true in
  let deps_list = parse_and_type_list ~warn deps in
  let map = List.fold_left2 (fun m fname ss ->
    Util.SMap.add (get_basename fname) ss m) Util.SMap.empty deps deps_list in
  wrap2 (parse_sk ~warn) map file



let keywords =
  let open Util in
  Hashtbl.fold (fun x _ s -> SSet.add x s) Lexer.keywords SSet.empty

let find_deps =
  let open Util in
  let find_file maindir {contents=f;loc} =
    (* We search for the file in f's dir first, and then in the current working
       directory *)
    let basename = f ^ ".sk" in
    let f1 = maindir ^ "/" ^ basename in
    if Sys.file_exists f1 then f1 else
    let f2 = Sys.getcwd () ^ "/" ^ basename in
    if Sys.file_exists f2 then f2 else
      Errors.type_error loc (Unbound {typ="dependency";name=basename})
  in
  let extract f deps: string list =
    let rec dfs f ancestors seen =
      if SSet.mem f.contents seen then
        if SSet.mem f.contents ancestors then
          failwith "Found cycle in the dependencies"
        else
          ([], seen)
      else
        let seen = SSet.add f.contents seen in
        let ancestors = SSet.add f.contents ancestors in
        let (filename, children) = SMap.find f.contents deps in
        let (print, seen) =
          List.fold_left (fun (print, seen) i ->
            let (print', seen') = dfs i ancestors seen in
            (print @ print', seen')) ([], seen) children
        in
        (print @ [filename], seen)
    in
    let (_, f_children) = SMap.find f deps in
    let (print, _seen) =
      List.fold_left (fun (print, seen) i ->
        let (print', seen') = dfs i (SSet.singleton f) seen in
        (print @ print', seen')) ([], SSet.singleton f) f_children
    in
    print
  in
  let find_deps f =
    let () =
      if not (Sys.file_exists f) then
        failwith ("Could not find file " ^ f)
    in
    let maindir = Filename.dirname f in
    let rec aux sk deps =
      if SMap.mem sk.contents deps then deps
      else
        let filename = find_file maindir sk in
        let imports, _ = Parserutil.parse_from_file Parser.main filename in
        let deps = SMap.add sk.contents (filename, imports) deps in
        List.fold_left (fun m i -> aux i m) deps imports
    in
    let dep_tree =
      let imports, _ = Parserutil.parse_from_file Parser.main f in
      let deps = SMap.singleton (get_basename f) (f, imports) in
      List.fold_left (fun m i -> aux i m) deps imports
    in
    extract (get_basename f) dep_tree
  in wrap find_deps
