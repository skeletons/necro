(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

(* This file provides functions to print the types defined in the Skeltypes module. *)

open Util
open TypedAST
open Printf

(* Auxiliary functions *)

let here n = (n, None)
let ghost_typ = {tydesc=Product [];tyloc=ghost_loc}

let print_from print (item: 'a with_path) =
  let (x, path) = item in
  match path with
  | None -> print x
  | Some p -> p ^ "::" ^ print x

let add_parentheses str =
  if String.index_opt str ' ' = None then
    str
  else if str.[0] = '(' then (
    let rec close_paren i =
      if i >= String.length str then i
      else
        match str.[i] with
        | ')' -> i
        | '(' -> close_paren (1 + close_paren (1 + i))
        | _ -> close_paren (1 + i) in
    if close_paren 1 = String.length str - 1 then str
    else "(" ^ str ^ ")"
  ) else "(" ^ str ^ ")"

let rec print_type_args ?(deb="<") print = function
  | [] -> ""
  | x :: [] -> deb ^ print x ^ ">"
  | x :: q -> deb ^ print x ^ print_type_args ~deb:(", ") print q

let print_space_com com =
  begin match com with
  | None -> ""
  | Some s -> " (**" ^ s ^ "*)"
  end

let print_com_nl com =
  begin match com with
  | None -> ""
  | Some s -> "(**" ^ s ^ "*)\n"
  end



(* Printing functions *)

let rec string_of_type ty =
  let string_of_type = string_of_type in
  begin match ty.tydesc with
  | Variable s -> s
  | UserType (s, []) ->
      print_from Fun.id s
  | UserType (s, args) ->
      print_from Fun.id s ^ print_type_args string_of_type args
  | Product l ->
      let l' = (List.map string_of_type l) in
      "(" ^ String.concat ", " l' ^ ")"
  | Arrow (s1, s2) ->
      let print_s1 =
        begin match s1.tydesc with
        | Arrow _ -> "(" ^ string_of_type s1 ^ ")"
        | Variable _ | UserType _ | Product _ -> string_of_type s1
        end
      in
      print_s1 ^ " → " ^ string_of_type s2
  end

let rec string_of_pattern p =
  begin match p.pdesc with
  | PTuple [] -> "()"
  | PTuple l -> "(" ^ String.concat ", " (List.map string_of_pattern l) ^ ")"
  | PVar x -> x
  | PWild -> "_"
  | PConstr ((c,_), p) ->
      let c = print_from Fun.id c in
      let args = string_of_pattern p in
      if args = "()" then c else
      c ^ " " ^ add_parentheses args
  | PRecord pl ->
      let aux (((c, _), p): field * pattern) =
        let args = string_of_pattern p in
        print_from Fun.id c ^ "= " ^ args
      in
      "(" ^ String.concat ", " (List.map aux pl) ^ ")"
  | POr (p1, ({pdesc=POr _;_} as p2)) ->
      string_of_pattern p1 ^ "|(" ^ string_of_pattern p2 ^ ")"
  | POr (p1, p2) ->
      string_of_pattern p1 ^ "|" ^ string_of_pattern p2
  | PType (p, ty) ->
      Format.sprintf "(%s: %s)"
      (string_of_pattern p)
      (string_of_type ty)
  end

let string_of_typed_var tv =
  let name, args =
    begin match tv with
    | LetBound (n, _) -> here n, []
    | TopLevel (_, n, ta, _) -> n, ta
    end
  in
  print_from Fun.id name ^ print_type_args string_of_type args

let rec monoline_term t =
  begin match t.tdesc with
  | TVar _ -> true
  | TConstr (_, t) -> monoline_term t
  | TTuple tl -> List.for_all monoline_term tl
  | TFunc _ -> false
  | TField (t, _) | TNth (t, _, _) -> monoline_term t
  | TRecMake ((_, t) :: []) -> monoline_term t
  | TRecMake _ -> false
  | TRecSet (_, ((_, t) :: [])) -> monoline_term t
  | TRecSet _ -> false
  end

let rec monoline_skel sk =
  begin match sk.sdesc with
  | Branching (_, []) | Exists _ -> true
  | Branching (_, sk :: []) -> monoline_skel sk
  | Branching _ | Match _ | LetIn _ -> false
  | Return t -> monoline_term t
  | Apply (t, arg) -> List.for_all monoline_term (t::arg::[])
  end

(* [protect] says whether the term needs to be protected *)
let rec string_of_term ?(protect=false) ~indent term =
  let nl indent = "\n" ^ String.make indent '\t' in
  begin match term.tdesc with
  | TVar tv -> string_of_typed_var tv
  | TConstr ((c, (_, args)), t) ->
      let str_constr =
        print_from Fun.id c ^ print_type_args string_of_type args
      in
      if t.tdesc = TTuple [] then str_constr else
      let str_t = string_of_term ~protect:true ~indent t in
      str_constr ^ " " ^ str_t |>
      if protect then sprintf "(%s)" else Fun.id
  | TTuple tl -> sprintf "(%s)"
    (String.concat ", " (List.map (string_of_term ~indent) tl))
  | TFunc (pat, sk) ->
      let str_pat = string_of_pattern pat in
      let str_ty = add_parentheses (string_of_type pat.ptyp) in
      let str_sk = string_of_skeleton ~indent:(indent+1) sk in
      let unprotected =
        if monoline_skel sk then
          sprintf "λ %s : %s → %s" str_pat str_ty str_sk
        else
          sprintf "λ %s : %s →%s%s"
            str_pat str_ty (nl (indent + 1)) str_sk
      in
      if protect then sprintf "(%s)" unprotected else unprotected
  | TField (t, (f, _)) ->
      let str_field = print_from Fun.id f in
      let str_t = string_of_term ~protect:true ~indent t
      in
      sprintf "%s.%s" str_t str_field
  | TNth (t, _, n) ->
      let str_t = string_of_term ~protect:true ~indent t in
      sprintf "%s.%d" str_t n
  | TRecMake ftl ->
      let string_of_ft (((f, _), t):field * term) =
        let str_t = string_of_term ~indent:(indent + 1) t in
        sprintf "%s = %s" (fst f) str_t
      in
      begin match ftl with
      | [] -> assert false
      | ((f1, _), _) :: _ ->
          let sep =
            if List.for_all (Util.comp snd monoline_term) ftl
            then ", "
            else "," ^ nl indent
          in
          ftl |>
          List.map string_of_ft |>
          String.concat sep |>
          sprintf "%s(%s)" (print_from (Fun.const "") f1)
      end
  | TRecSet (t, ftl) ->
      let recmake =
        {tdesc=TRecMake ftl; tloc=ghost_loc; ttyp=ghost_typ}
      in
      sprintf "%s ← %s"
        (string_of_term ~indent t)
        (string_of_term ~indent:(indent+1) recmake)
      |> if protect then sprintf "(%s)" else Fun.id
  end

(* [protect] says whether the skeleton needs to be protected *)
and string_of_skeleton ?(protect=false) ~indent sk =
  let nl indent = "\n" ^ String.make indent '\t' in
  begin match sk.sdesc with
  | Return t -> string_of_term ~protect ~indent t
  | Branching (ty, skeletons) ->
      begin match skeletons with
      | [] -> sprintf "(branch end: %s)" (string_of_type ty)
      | sk :: [] ->
          sprintf "branch %s end" (string_of_skeleton ~indent sk)
      | _ ->
          sprintf "branch%s%s%send"
            (nl (indent + 1))
            (skeletons
              |> List.map (string_of_skeleton ~indent:(indent + 1))
              |> String.concat (nl indent ^ "or" ^ nl (indent + 1)))
            (nl indent)
      end
  | Match (matched, branches) ->
      let str_matched = string_of_skeleton ~indent matched in
      let string_of_branch (pat, skel) =
        let str_pat = string_of_pattern pat in
        let str_skel i = string_of_skeleton ~indent:i skel in
        if monoline_skel skel then
          sprintf "| %s → %s" str_pat (str_skel indent)
        else
          sprintf "| %s →%s%s" str_pat (nl (indent + 1)) (str_skel (indent + 1))
      in
      let str_branches = branches
        |> List.map string_of_branch
        |> String.concat (nl indent)
      in
      sprintf "match %s with%s%s%send"
        str_matched (nl indent) str_branches (nl indent)
  | Apply (f, arg) ->
      let str_f = string_of_term ~protect:true ~indent f in
      let str_arg = string_of_term ~protect:true ~indent arg in
      let unprotected = sprintf "%s %s" str_f str_arg in
      if protect then sprintf "(%s)" unprotected else unprotected
  | Exists ty ->
      let str_ty = string_of_type ty in
      sprintf "∃ %s" str_ty
  | LetIn (annotation, pat, bound_sk, res_sk) ->
      let is_semicolon =
        (match pat with
          {ptyp={tydesc=Product [];_}; pdesc=(PWild|PTuple []); _} -> true | _ -> false) in
      (* if bound_sk is a function, extract arguments *)
      let rec extract_args sk =
        begin match sk.sdesc with
        | Return {tdesc=TFunc (p, sk');_} ->
            let args, body = extract_args sk' in
            (p :: args, body)
        | _ -> ([], sk)
        end
      in
      let bound_args, bound_body =
        (* only extract if no bind annotation *)
        begin match annotation with
        | NoBind -> extract_args bound_sk
        | _ -> [], bound_sk
        end
      in
      let str_args = bound_args
        |> List.map (fun p -> sprintf " %s" (string_of_pattern p))
        |> String.concat ""
      in
      let str_pat = sprintf "%s%s" (string_of_pattern pat) str_args in
      let str_annot =
        begin match annotation with
        | NoBind -> ""
        | TermBind (_, b, _, _) -> "%" ^ print_from Fun.id b
        | SymbolBind (s, _, _, _, _) -> s
        end
      in
      let str_bound i =
        let protect =
          is_semicolon &&
          begin match bound_body.sdesc with
          | LetIn (_, {ptyp={tydesc=Product [];_};pdesc=(PWild|PTuple[]);_}, _, _) -> true
          | _ -> false
          end
        in
        string_of_skeleton ~protect ~indent:i bound_body in
      let str_sk =
        string_of_skeleton ~indent res_sk
      in
      if is_semicolon then begin
        let unprotected =
          sprintf "%s ;%s%s%s" (str_bound indent) str_annot (nl indent) str_sk
        in
        if protect then sprintf "(%s)" unprotected else unprotected
      end else begin
        if monoline_skel bound_body then
          sprintf "let %s =%s %s in%s%s" str_pat str_annot (str_bound indent) (nl indent) str_sk
        else
          sprintf "let %s =%s%s%s%sin%s%s" str_pat str_annot (nl (indent + 1))
            (str_bound (indent + 1)) (nl indent) (nl indent) str_sk
      end
    end

let string_of_term_decl name ta ty com def =
  let nl = "\n" in
  (* check whether a pattern p is explicitly typed, add type if it is not *)
  let rec add_type p =
    (fun pdesc -> {p with pdesc}) @@
    match p.pdesc with
    | PType _ -> (* already typed -> ok *)
        p.pdesc
    | PTuple pl -> (* type component by component *)
        PTuple (List.map add_type pl)
    | POr _ ->
        let rec splitor accu p =
          match p.pdesc with
          | POr (a, b) -> splitor (splitor accu a) b
          | _ -> p :: accu
        in
        let pl = splitor [] p in
        if List.exists (function {pdesc=PType _;_} -> true | _ -> false) pl
        then (* one of the alternative is typed -> ok *)
          p.pdesc
        else (* add type to whole alternative *)
          PType (p, p.ptyp)
    | PVar _ | PWild | PConstr _ | PRecord _ -> (* anything else: add type *)
        PType (p, p.ptyp)
  in
  (* [aux] analyzes the skeleton and extracts the arguments if it is a function,
     in order to use the [val f (p:τ_p) : τ = _] meta-notation
     (rather than [val f: τ_p -> τ = λ (p:τ_p) -> …]) *)
  let rec aux sk =
    begin match sk.sdesc with
    | Return {tdesc=TFunc (p, sk);_} ->
        " " ^ string_of_pattern (add_type p) ^ aux sk
    | _ -> ": " ^ string_of_type sk.styp ^ " =" ^ nl ^ "\t" ^ string_of_skeleton ~indent:1 sk ^ nl
    end
  in
  begin match def with
  | None ->
      print_com_nl com ^
      "val " ^ name ^ print_type_args Fun.id ta ^ " : " ^ string_of_type ty ^ nl
  | Some t ->
      print_com_nl com ^
      "val " ^ name ^ print_type_args Fun.id ta ^
      aux {sdesc=Return t; sloc=ghost_loc; styp=ty}
  end

let string_of_type_decl name com td =
  let nl = "\n" in
  let nl2 = "\n\t" in
  begin match td with
  | TDUnspec i ->
      let args = List.init i (fun _ -> "_") in
      print_com_nl com ^
      "type " ^ name ^ print_type_args Fun.id args ^ nl
  | TDVariant (args, constructors) ->
      print_com_nl com ^
      "type " ^ name ^ print_type_args Fun.id args ^ " =" ^
      List.fold_left (fun str csig ->
        let () = if csig.cs_variant_type <> name then assert false in
          (str ^ nl ^ "| " ^ csig.cs_name ^
          (if csig.cs_input_type.tydesc = Product []
          then "" else (" " ^ string_of_type csig.cs_input_type))
          ^ print_space_com csig.cs_comment)) "" constructors ^ nl
  | TDRecord (args, fields) ->
      print_com_nl com ^
      "type " ^ name ^ print_type_args Fun.id args ^ " = (" ^
      snd (List.fold_left (fun (notfst, str) fsig ->
        let () = if fsig.fs_record_type <> name then assert false in
          (true, nl2 ^
          fsig.fs_name ^ " : " ^ string_of_type fsig.fs_field_type ^
          print_space_com fsig.fs_comment ^
          (if notfst then "," else "") ^ str
        )
        ) (false, "") fields) ^ nl ^ ")" ^ nl
  | TDAlias (args, unfolding) ->
      print_com_nl com ^
      "type " ^ name ^ print_type_args Fun.id args ^ " = "
      ^ string_of_type unfolding ^ nl
  end

let rec string_of_decl_ss ss =
  let open ParsedAST in
  begin function
  | Comment c -> "(**" ^ c.contents ^ "*)\n"
  | TypeDecl name ->
      let com, td = SMap.find name ss.ss_types in
      string_of_type_decl name com td
  | TermDecl name ->
      let (com, (ta, ty, def)) = SMap.find name ss.ss_terms in
      string_of_term_decl name ta ty com def
  | BinderDecl c ->
      let (_, n) = SMap.find c ss.ss_binders in
      "binder " ^ c ^ " = " ^ print_from Fun.id n
  end

and string_of_ss ss =
  let nl = "\n" in
  List.fold_left (fun str decl ->
    str ^ string_of_decl_ss ss decl ^ nl) "" ss.ss_order



