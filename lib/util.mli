(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

module SMap : Map.S with type key = String.t
(** Maps indexed by strings *)

module SSet : Set.S with type elt = String.t
(** Sets of strings *)

val sset_list_union: SSet.t list -> SSet.t
(** [sset_list_union sl] is the union of all the sets in the list [sl]. *)

val smap_list_union: 'a SMap.t list -> 'a SMap.t
(** [smap_list_union ml] is the union of all the maps in the list [ml]. *)

val smap_diff: 'a SMap.t -> 'b SMap.t -> 'a SMap.t
(** [smap_diff m1 m2] removes from m2 all bindings whose key is in m1 *)

val swap : ('a -> 'b -> 'c) -> ('b -> 'a -> 'c)
(** [swap f x y] is equal to [f y x] *)

val comp: ('a -> 'b) -> ('b -> 'c) -> ('a -> 'c)
(** The function composition:
    [comp f g x = g (f x)]. *)

val smap_domain : 'a SMap.t -> SSet.t
(** [smap_domain m] returns the domain of a map, i.e the set of all [a] s.t.
    [SMap.mem a m]. *)

val list_drop: int -> 'a list -> 'a list
(** [list_drop n [x0; …; xm]] returns [[xn; …; xm]]. If n > m, it returns [[]] *)

val list_iter3 : ('a -> 'b -> 'c -> unit) -> 'a list -> 'b list -> 'c list -> unit
(** Iter all the way through the three lists, which must be of the same size.
    @raise Invalid_argument if two of the list have a different length. *)

val list_map3 : ('a -> 'b -> 'c -> 'd) -> 'a list -> 'b list -> 'c list -> 'd list
(** Same as [List.map2], but with 3 lists.
    @raise Invalid_argument if two of the list have a different length. *)

val list_square : 'a list -> 'b list -> ('a * 'b) list
(** [list_square l1 l2] returns then list of all [(a, b)] s.t. [a] is in the
    list [l1] and [b] is in the list [l2]. *)

val product: 'a list list -> 'a list list
(** [product [l1; ...; ln]] returns a list of all the possible lists
    [[a1, ..., an]] s.t. [a1] is in [l1], ..., [an] is in [ln].
    It is very similar to {!list_square}, but with any number of lists. *)

val list_map_option : ('a -> 'b option) -> 'a list -> 'b list option
(** [list_map_option f [a1; ...; an]] applies [f] to [a1, ..., an].
    If any of the [a1, ..., an] is mapped by [f] to [None], then it returns
    [None].  Else, it returns
    [Some [Option.get (f a1); ...; Option.get (f an)]]. *)

val fresh : (string -> bool) -> string -> string
(** [fresh ok n] returns a name [n'] such that [ok n'].
    The name [n'] will be inspired from [n].
    This function may not terminate if [ok] returns [false]
    on an infinite number of terms. *)

val uniq : 'a list -> 'a list
(** [uniq l] returns a list with the same elements as [l] but where all element
    appear at most once.  The order in the original list is preserved if
    considering only the first occurence of each element. *)

val find_duplicate : 'a list -> ('a -> 'b) -> ('a * 'a) option
(** If the given list has a duplicated element, returns one of them. *)

val find_overlap : 'a list -> 'a list -> ('a -> 'b) -> ('a * 'a) option
(** If the lists overlap, return an element that is in both lists. *)

val write_nth : int -> 'a -> 'a list -> 'a list option
(** [write_nth n x l] replace the [n]{^th} element of the list [l] by [x], or
    returns [None] if the list is too short. *)

val seq : int -> int list
(** [seq n] returns the list [[0; ...; n-1]].
    @raise Invalid_argument if n < 0. *)

val print_list: ?sep:string -> ?empty:(Format.formatter -> unit) ->
  (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a list -> unit
(** Printing a list using Format.
 [print_list ~sep ~empty print ff l] prints the list [l] in formatter [ff]
 using [print] to print each individual element and using [sep] as a separator.
 If [l] is empty, then only use [empty].*)

val last : 'a list -> 'a option
(** Get the last element of a list *)

val print_in_file: string -> string option -> unit
(** [print_in_file str (Some f)] prints the string [str] in the file whose
    name is [f]. If [None] is provided, then [str] is printed in [stdout]. *)

val fail: ('a, Format.formatter, unit, 'b) format4 -> 'a
(** fail with given message, using Format syntax *)



(** A type to store location information along with some term *)
type 'a with_loc = {
		contents : 'a ;
		loc : location
	}
and location = Lexing.position * Lexing.position

(** Extract the contents of a located expression *)
val contents: 'a with_loc -> 'a

(** Extract the contents of a located expression *)
val loc: 'a with_loc -> location

(** Apply a function to the contents of a located expression *)
val loc_map: ('a -> 'b) -> 'a with_loc -> 'b with_loc

(** Take an expression and attach a location to it *)
val mkloc: location -> 'a -> 'a with_loc

(** Dummy location *)
val ghost_loc: location

(** Print a message on stderr giving the location of the error *)
val report_error_location: location -> unit

(** [partition l f] partitions [l] according to [f]'s second component.
    The order of the lists in the resulting partition is unspecified. *)
val partition: ('a -> string option * 'b) -> 'a list -> 'b list SMap.t * 'b list

type ('l, 'n) tree = ('l, 'n) Tree.t
type ('l, 'n) zipper = ('l, 'n) Zipper.t
