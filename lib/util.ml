(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

module SMap = Map.Make(String)
module SSet = Set.Make (String)

let sset_list_union =
	List.fold_left SSet.union SSet.empty

let smap_list_union m =
	List.fold_left (SMap.union (fun _ _ x -> Some x)) SMap.empty m

let swap f x y = f y x

let comp f g x = g (f x)

let smap_domain m =
	SMap.fold (fun a _ s -> SSet.add a s) m SSet.empty

let smap_diff m1 m2 =
	SMap.merge (fun _ x1 x2 ->
		begin match x1, x2 with
		| _, Some _ -> None
		| Some _, None -> x1
		| None, None -> None (* doesn't matter *)
		end) m1 m2

let rec list_drop n l =
	if n <= 0 then l else
	begin match l with
	| [] -> []
	| _ :: q -> list_drop (n-1) q
	end

let list_iter3 f la lb lc =
	let rec aux = function
	| [], [], [] -> ()
	| a :: la, b :: lb, c :: lc -> f a b c ; aux (la, lb, lc)
	| _, _, _ -> raise (Invalid_argument "list_iter3: different list sizes.") in
	aux (la, lb, lc)

let list_map3 f la lb lc =
	let rec aux = function
	| [], [], [] -> []
	| a :: la, b :: lb, c :: lc -> f a b c :: aux (la, lb, lc)
	| _, _, _ -> raise (Invalid_argument "list_map3: different list sizes.") in
	aux (la, lb, lc)

let list_square l =
	let rec aux stack = function
		| [] -> List.concat stack
		| b :: lb ->
			aux (List.map (fun a -> (a, b)) l :: stack) lb
	in aux []

(* Give the product of lists *)
let rec product: 'a list list -> 'a list list =
	begin function
	| [] -> [[]]
	| x :: q -> let q' = product q in
		List.flatten (List.map (fun a -> List.map (List.cons a) q') x)
	end

let list_map_option f l =
	Option.map List.rev (List.fold_left (fun r e ->
		Option.bind r (fun l -> Option.bind (f e) (fun v -> Some (v :: l)))) (Some []) l)

let fresh ok n =
	let rec aux i =
		let n = n ^ "_" ^ string_of_int i in
		if ok n then n else aux (1 + i) in
	if ok n then n else
	if ok (n^"'") then (n^"'") else
		aux 0

let uniq l =
	let rec aux res = function
		| [] -> List.rev res
		| a :: l ->
			aux (if List.mem a res then res else a :: res) l in
	aux [] l

let find_duplicate l f =
	let l = List.sort (fun x y -> compare (f x) (f y)) l in
	let rec find_dup_aux =
		begin function
		| [] -> None
		| _ :: [] -> None
		| a :: b :: _ when f a = f b -> Some (a, b)
		| _ :: l -> find_dup_aux l
		end
	in
	find_dup_aux l

let rec find_overlap l1 l2 f =
	begin match l2 with
	| [] -> None
	| a :: q ->
			begin match List.find_opt (fun x -> f x = f a) l1 with
			| None -> find_overlap l1 q f
			| Some b -> Some (b, a)
			end
	end

let write_nth i e l =
	if List.length l <= i then
		None
	else
		Some (List.mapi (fun j v -> if i = j then e else v) l)

let seq i =
	List.init i Fun.id

(* Printing a list using Format *)
let print_list ?(sep="") ?(empty=fun _ -> ()) print ff =
	begin function
	| [] -> empty ff
	| l -> Format.pp_print_list ~pp_sep:(fun ff () -> Format.fprintf ff "%s" sep)
	print ff l
	end

let rec last =
	begin function
	| [] -> None
	| a :: [] -> Some a
	| _ :: q -> last q
	end

let print_in_file str =
	begin function
	| None -> print_endline str
	| Some f ->
			let oc = open_out f in
			let () = output_string oc str in
			close_out oc
	end

let fail fmt =
	Format.kasprintf (fun s -> failwith s) fmt


type 'a with_loc = {
		contents : 'a ;
		loc : location
	} and location = Lexing.position * Lexing.position

let contents x = x.contents

let loc x = x.loc

let loc_map f x = {
		contents=f x.contents;
		loc=x.loc
	}

let mkloc loc contents = {contents; loc}

let ghost_loc = (Lexing.dummy_pos, Lexing.dummy_pos)

let report_error_location (start_pos, end_pos) =
	let open Lexing in
	let start_col = start_pos.pos_cnum - start_pos.pos_bol + 1 in
	let end_col = end_pos.pos_cnum - end_pos.pos_bol + 1 in
	if start_pos.pos_lnum = end_pos.pos_lnum then
		Format.eprintf "File %S, line %d, characters %d-%d:@."
		start_pos.pos_fname start_pos.pos_lnum start_col end_col
	else
		Format.eprintf "File %S, lines %d-%d, characters %d-%d:@."
		start_pos.pos_fname start_pos.pos_lnum end_pos.pos_lnum start_col end_col

let partition (f:'a -> (string option * 'b)) (l:'a list):
					'b list SMap.t * 'b list =
	let rec partition_aux
			(l:'a list) (accu_some: 'b list SMap.t) (accu_none: 'b list):
					'b list SMap.t * 'b list =
		begin match l with
		| [] -> accu_some, accu_none
		| a :: q ->
				let (s, b) = f a in
				begin match s with
				| Some s ->
					let l = SMap.find_opt s accu_some
						|> Option.value ~default:[]
					in
					let accu_some = SMap.add s (b :: l) accu_some in
					partition_aux q accu_some accu_none
				| None -> partition_aux q accu_some (b :: accu_none)
				end
		end
	in
	partition_aux l SMap.empty []

type ('l, 'n) tree = ('l, 'n) Tree.t
type ('l, 'n) zipper = ('l, 'n) Zipper.t
