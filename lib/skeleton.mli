(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

open TypedAST
open Util


val pattern_variables: pattern -> typ SMap.t
(** Give the list of variables bound by a given pattern *)

val free_variables_skel : skeleton -> typ SMap.t
(** Return the free variables of a skeleton. *)

val free_variables_term : term -> typ SMap.t
(** Return the free variables of a term. *)

(** {1 Navigate skeletal semantics} *)

type dependencies = (string * skeletal_semantics) list
(** Skeletal semantics for the dependencies, given in a associative list *)

val get_constructor_signature:
	dependencies -> skeletal_semantics ->
		string with_path -> constructor_signature

val get_field_signature:
	dependencies -> skeletal_semantics ->
		string with_path -> field_signature

val get_type_definition:
	dependencies -> skeletal_semantics ->
		string with_path -> type_definition

val get_type_kind:
	dependencies -> skeletal_semantics ->
		string with_path -> type_kind

val get_all_constructors: skeletal_semantics -> constructor_signature list

val get_all_fields: skeletal_semantics -> field_signature list

val get_constructors_by_type:
	skeletal_semantics -> (string * string list * constructor_signature list) list

val get_fields_by_type:
	skeletal_semantics -> (string * string list * field_signature list) list

(** {1 Handle types} *)

val type_equals: dependencies -> skeletal_semantics -> typ -> typ -> bool
(** Compare two types. *)

val unalias: dependencies -> skeletal_semantics -> typ -> typ
(** [unalias _ _ ty] returns a type that is equivalent to [ty], and that is
		not an alias. Formally, for any type [ty], and any [deps] and [ss]:
		- [type_equals deps ss ty (unalias deps ss ty) = true]
		- [unalias deps ss ty] does not match [UserType (_, Alias, _)]*)

val unalias_rec: dependencies -> skeletal_semantics -> typ -> typ
(** [unalias_rec _ _ ty] returns a type that is equivalent to [ty], and that
		contains no alias. Formally, for any type [ty], and any [deps] and [ss]:
		- [type_equals deps ss ty (unalias_rec deps ss ty) = true].
		- no subterm of [unalias_rec deps ss ty] matches [UserType (_, Alias, _)]. *)

val unify: dependencies -> skeletal_semantics -> typ -> typ ->
  typ Util.SMap.t option

(** {1 Pattern-matching} *)

val useful:
  dependencies -> skeletal_semantics ->
    pattern list list -> pattern list -> bool

(** {1 Graph-matching} *)

module PGraph : Graph.Sig.P with type V.t = string * string option

val term_dep_graph: dependencies ->
		skeletal_semantics -> PGraph.t
(** [term_dep_graph deps sem] creates a graph of dependencies between terms of
    [sem]. Term [t1] has an edge pointing towards term [t2] iff the body of [t1]
    calls [t2]. *)

val type_dep_graph: dependencies -> skeletal_semantics -> PGraph.t
(** [type_dep_graph deps sem] creates a graph of dependencies between types of
    [sem]. Type [t1] has an edge pointing towards term [t2] iff the definition
    of [t1] calls [t2]. That is, either
    - [t1] is an alias of a type whose definition contains [t2]
    - [t1] is a variant type, one of its constructor's input type contains [t2]
    - [t1] is a record type, one of its field's type contains [t2] *)

(** {1 Renaming} *)

type protect_remapping =
	{ fields : string SMap.t
	; types : string SMap.t
	; terms : string SMap.t
	}

val protect_ss:
	SSet.t -> skeletal_semantics -> skeletal_semantics * protect_remapping
(** [protect_ss kw ss] returns a skeletal semantics where all names in [kw] are
		replaced by names not in [kw]. Only to be used for a skeletal semantics
		with no module. It also returns the list of fields/types/terms that have
    been renamed. *)
