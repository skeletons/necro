(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

(** This file performs all error handling *)
open TypedAST

exception TypeError

(** test files for warnings are in test/units.t/warnings *)
type warning =
	| NotExhaustive                       (* 1 *)
	(** see match_non_exhaustive.sk *)
	| OneBranch                           (* 2 *)
	(** see one_branch.sk *)
	| RedundantPattern                    (* 3 *)
	(** see match_useless_case.sk *)
	| Unused of {typ:string; name:string} (* 4 *)
	(** see unused_variable.sk *)

type warn_list = int list
let warning_infos = [
	(1, "Non exhaustive pattern matching") ;
	(2, "Branching with only one branch") ;
	(3, "Useless case in pattern matching") ;
	(4, "Unused variable") ]
let all_warnings: warn_list = List.map fst warning_infos

let number w =
	begin match w with
	| NotExhaustive    -> 1
	| OneBranch        -> 2
	| RedundantPattern -> 3
	| Unused _         -> 4
	end


(** test files for errors are in test/units.t/fail *)
type error =
	| CannotInfer
	(** see bind_cannot_infer*.sk *)
	| CannotInferPatternType
	(** see pattern_cannot_infer.sk *)
	| ComponentZero
	(** see component_zero.sk *)
	| Cycle of {typ:string; cycle:string list}
	(** see cyclical_alias*.sk *)
	| DifferentVariableOrPattern of string
	(** see or_pattern_variable.sk *)
	| DifferentVariableTypeOrPattern of {name:string; left:typ; right:typ}
	(** see or_pattern_variable_different_type.sk *)
	| EmptyBranching
	(** see empty_branch.sk *)
	| EmptyMatch
	(** syntactically impossible *)
	| IncompatibleBranches of string * typ list
	(** see incompatible_branch.sk *)
	| MissingFields of string list
	(** see missing_fields.sk *)
	| NoSuchComponent of int
	(** see wrong_component.sk *)
	| NoSuchConstructor of string * typ
	(** see pattern_constr_wrong_type.sk *)
	| NoSuchField of string * typ
	(** see no_such_field.sk *)
	| NotAnArrow of typ
	(** see apply_not_arrow.sk *)
	| NotAProduct
	(** see component_not_tuple.sk *)
	| NotARecord
	(** see not_a_record_set.sk *)
	| NotArrowType of typ
	(** see func_not_arrow.sk *)
	| NotBindType of {name:string; typ:typ}
	(** see not_a_bind_*.sk *)
	| NotProductType of typ
	(** see tuple_not_prod.sk *)
	| NoTypeMatch of typ
	(** see for instance pattern_constr_not_variant.sk *)
	| ReboundPattern of string
	(** see pattern_rebound*.sk *)
	| Shadowing of string
	(** see shadowing.sk *)
	| SkeletonInsteadTerm
	(** see skeleton_instead_term.sk *)
	| TupleWrongLength of {got:int; expected:typ}
	(** see pattern_tuple_wrong_length.sk *)
	| TypeVariablePoly of string
	(** see type_variable_poly.sk *)
	| UnboundBinder of string
	(** see unbound_binder.sk *)
	| UnboundFile of string
	| Unbound of {typ:string; name:string}
	(** see for instance unbound_field.sk *)
	| UntypablePattern
	(** see pattern_cannot_infer.sk *)
	| VarUnification of {name:string; typ1:typ; typ2:typ}
	(** see bind_cannot_unify.sk *)
	| WrongNumberArgs of {typ:string; name:string; given:int; expected:int}
	(** see wrong_number args.sk *)
	| WrongType of {given:typ; expected:typ}
	(** see wrong_type.sk *)

type error2 =
	| Redefinition of {typ: string; name:string}

let report_error_location (start_pos, end_pos) =
	let open Lexing in
	let start_col = start_pos.pos_cnum - start_pos.pos_bol + 1 in
	let end_col = end_pos.pos_cnum - end_pos.pos_bol + 1 in
	if start_pos.pos_lnum = end_pos.pos_lnum then begin
		Format.eprintf "File %S, line %d, characters %d-%d:@."
			start_pos.pos_fname start_pos.pos_lnum start_col end_col;
			(* For one-line errors, print the corresponding line *)
			begin try
				let contents =
					let file = open_in start_pos.pos_fname in
					let res = really_input_string file (in_channel_length file) in
					close_in file; res in
						let len = Fun.flip (-) start_pos.pos_bol end_pos.pos_cnum in
				let substring = String.sub contents start_pos.pos_bol len in
				Format.eprintf "%4d | %s\n" start_pos.pos_lnum substring;
				Format.eprintf "       %s%s\n"
					(String.init (start_pos.pos_cnum - start_pos.pos_bol) (Fun.const ' '))
					(String.init (end_pos.pos_cnum - start_pos.pos_cnum) (Fun.const '^'))
			with _ -> ()
			end
    end
	else
		Format.eprintf "File %S, lines %d-%d, characters %d-%d:@."
		start_pos.pos_fname start_pos.pos_lnum end_pos.pos_lnum start_col end_col


let report loc (msg:string) =
	if loc <> Util.ghost_loc then report_error_location loc ;
	Printf.eprintf "%s\n" msg


let error_message (e: error): string =
	let open Format in
	let open Printer in
	begin match e with
	| CannotInfer ->
			"The type of this expression can not be inferred."
	| CannotInferPatternType ->
			"The type of this pattern could not be inferred."
	| ComponentZero ->
			"The components of a tuple are numbered starting with 1."
	| DifferentVariableOrPattern s ->
			sprintf "The variable %s appears only on one side of this or-pattern." s
	| DifferentVariableTypeOrPattern {name; left; right} ->
			sprintf "The variable %s has type %s in the left-hand side of this \
			or-pattern, and type %s in the right-hand side."
			name (string_of_type left) (string_of_type right)
	| EmptyBranching ->
			sprintf "Cannot infer a type for this empty branching."
	| EmptyMatch ->
			sprintf "Cannot infer a type for this empty pattern-matching."
	| NotArrowType ty ->
			sprintf "This expression is an arrow, it cannot have type %s."
			(string_of_type ty)
	| NotProductType ty ->
			sprintf "This expression is a product, it cannot have type %s."
			(string_of_type ty)
	| IncompatibleBranches (ctxt, types) ->
			sprintf "In this %s, some branches have incompatible types. The types \
			of the branches are [%s]." ctxt
			(List.map string_of_type types |> String.concat "; ")
	| MissingFields [field] ->
			sprintf "The field %s is missing in this record." field
	| MissingFields fields ->
			sprintf "The following fields are missing in this record: %s."
			(String.concat ", " fields)
	| NoSuchComponent n ->
			sprintf "This type has less than %d components." n
	| NoSuchConstructor (c, typ) ->
			sprintf "The type %s has no constructor named %s." (string_of_type typ) c
	| NoSuchField (f, typ) ->
			sprintf "The type %s has no field named %s." (string_of_type typ) f
	| NotAnArrow ty ->
			sprintf "This expression has type %s. It cannot be applied."
			(string_of_type ty)
	| NotAProduct ->
			sprintf "This expression is not a tuple."
	| NotARecord ->
			sprintf "This expression is not a record."
	| NotBindType {name; typ} ->
			sprintf "The term %s has type %s. It cannot be used as a binding \
			operator. Binding operators need to have a type matching (_, (_ → _)) → _."
			name (string_of_type typ)
	| NoTypeMatch typ ->
			sprintf "A pattern was expected of type %s." (string_of_type typ)
	| ReboundPattern x ->
			sprintf "The variable %s is multiply bound in this pattern." x
	| TupleWrongLength {got; expected} ->
			sprintf "This tuple has length %d, but a term was expected of type %s."
			got (string_of_type expected)
	| UnboundBinder sym ->
			sprintf "The binding symbol %s was not defined." sym
	| Unbound {typ;name} ->
			sprintf "Unbound %s %s." typ name
	| UnboundFile f ->
			sprintf "Unbound required file %s." f
	| VarUnification {name; typ1; typ2} ->
			sprintf "Unification error. Hint: is the type variable %s equal to \
			%s or %s?" name (string_of_type typ1) (string_of_type typ2)
	| WrongNumberArgs {typ; name; given; expected} ->
			sprintf "The %s %s was given %d type arguments instead of %d expected." typ
			name given expected
	| WrongType {given; expected} ->
		sprintf "This expression has type %s, but an expression was expected of \
		type %s." (string_of_type given) (string_of_type expected)
	| TypeVariablePoly name ->
			Format.sprintf "The type variable %s is not polymorphic." name
	| Cycle {typ; cycle} ->
			Format.asprintf "Cycle detected in %s definitions: %a." typ
			(Util.print_list ~sep:" -> " Format.pp_print_string) (cycle @ [List.hd cycle])
	| UntypablePattern -> "The type of this pattern can not be inferred."
	| Shadowing m ->
		sprintf "A module named %s is already defined at an higher level, this \
		definition would shadow it." m
	| SkeletonInsteadTerm ->
			"Syntax Error. Got a skeleton while a term was expected."
	end

let error2_messages (e:error2) =
	begin match e with
	| Redefinition {typ;name} ->
			Format.sprintf "The %s %s has already been defined." typ name,
			"First defined here."
	end

let warning_message (w: warning): string =
	begin match w with
	| NotExhaustive -> "This pattern matching is not exhaustive"
	| OneBranch -> "This branching has only one branch. It can be removed"
	| RedundantPattern -> "This pattern is redundant"
	| Unused {typ;name} ->
			Format.sprintf "Unused %s %s" typ name
	end


let type_error loc error =
	report loc (error_message error) ;
	raise TypeError

let type_warning warn_list loc warning =
	if List.mem (number warning) warn_list then
	report loc (warning_message warning)

let type_error2 loc1 loc2 error2 =
	let msg1, msg2 = (error2_messages error2) in
	report loc1 msg1 ;
	report loc2 msg2 ;
	raise TypeError
