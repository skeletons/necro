(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

type ('a, 'b) t = 'a * 'b
(** An alias for the pair type. *)

val equal: ('a -> 'a -> bool) -> ('b -> 'b -> bool) -> ('a, 'b) t -> ('a, 'b) t -> bool
(** [equal eqa eqb (a1, b1) (a2, b2)] holds iff [eqa a1 a2] and [eqb b1 b2] hold. *)

val compare: ('a -> 'a -> int) -> ('b -> 'b -> int) -> ('a, 'b) t -> ('a, 'b) t -> int
(** [compare cmpa cmpb (a1, b1) (a2, b2)] compares [(a1, b2)] and [(a2, b2)]
		according to lexicographical order. *)

val swap: ('a, 'b) t -> ('b, 'a) t
(** [swap (a, b)] returns [(b, a)]. *)

val fold: ('a -> 'b -> 'c) -> ('a, 'b) t -> 'c
(** [fold f (a, b)] returns [f a b]. *)

val create: 'a -> 'b -> ('a, 'b) t
(** [create a b] returns [(a, b)]. *)

val map: ('a -> 'c) -> ('b -> 'd) -> ('a, 'b) t -> ('c, 'd) t
(** [map f g (a, b)] returns [(f a, g b)] *)

val map2:
	('a -> 'c -> 'e) -> ('b -> 'd -> 'f) -> ('a, 'b) t -> ('c, 'd) t -> ('e, 'f) t
(** [map2 f g (a, b) (c, d)] returns [(f a c, g b d)]. *)

val map_fst: ('a -> 'c) -> ('a, 'b) t -> ('c, 'b) t
(** [map_fst f p] applies [f] to [p]'s first component *)

val map_snd: ('b -> 'c) -> ('a, 'b) t -> ('a, 'c) t
(** [map_snd f p] applies [f] to [p]'s second component *)

val iter: ('a -> unit) -> ('b -> unit) -> ('a, 'b) t -> unit
(** [iter f g (a, b)] returns [f a; g b] *)

val iter_fst: ('a -> unit) -> ('a, 'b) t -> unit
(** [iter_fst f p] applies [f] to [p]'s first component *)

val iter_snd: ('b -> unit) -> ('a, 'b) t -> unit
(** [iter_snd f p] applies [f] to [p]'s second component *)
