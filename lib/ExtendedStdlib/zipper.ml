(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

type ('l, 'n) path =
	| Root (** We are currently inspecting the root of the Tree.t *)
	| Child of ('l, 'n) Tree.t list * 'n * ('l, 'n) Tree.t list * ('l, 'n) path
	(** [Child (l, n, r, p)] means that we are currently looking at a child of
			node [n]. The siblings on the left are [l] and on the right are [r]. The
			path from [n] is [p]. *)

type ('l, 'n) t =
	{ cur: ('l, 'n) Tree.t
	; path: ('l, 'n) path}

let up (z: ('l, 'n) t): ('l, 'n) t =
	begin match z.path with
	| Root -> raise (Invalid_argument "cannot go up in zipper")
	| Child (l, node, r, q) ->
			let children = List.rev_append l (z.cur :: r) in
			{ cur = Node (node, children) ; path = q }
	end

let up_opt (z: ('l, 'n) t): ('l, 'n) t option =
	try Some (up z) with _ -> None

let find (f: ('l, 'n) Tree.t -> bool) (z:('l, 'n) t): ('l, 'n) t =
	let open Tree in
	let rec aux left right node =
		begin match right with
		| [] -> raise Not_found
		| a :: right when f a ->
				{ path = Child(left, node, right, z.path) ; cur = a }
		| a :: right -> aux (a :: left) right node
		end
	in
	begin match z.cur with
	| Leaf _ -> raise (Invalid_argument
		"Zipper.find should not be called on a leaf.")
	| Node (n, l) -> aux [] l n
	end

let find_opt f z =
	try Some (find f z) with _ -> None

let next_opt (z:('l, 'n) t): ('l, 'n) t option =
	begin match z.path with
	| Root -> None
	| Child  (l, m, x :: r, q) ->
			Some {cur = x; path = Child (z.cur :: l, m, r, q)}
	| _ -> None
	end

(* f acts on a zipper, and returns it at the same place.
	 fold f z launches f on every submodule of zipper z *)
let fold (f:('l, 'n) t -> ('l, 'n) t) (z:('l, 'n) t): ('l, 'n) t =
	begin match z.cur with
	| Node (_, []) -> z
	| Node (node, a :: q) ->
			(* go down *)
			let z = { cur = a ; path = Child ([], node, q, z.path) } in
			(* iterate f *)
			let rec aux (z:('l, 'n) t): ('l, 'n) t =
				let z = f z in
				begin match next_opt z with
				| None -> z
				| Some z -> aux z
				end
			in
			let z = aux z in
			(* go up *)
			let z = up z in
			z
	| Leaf _ -> raise (Invalid_argument "Zipper.fold: only acts on (Node _)")
	end

let rec top (zip: ('l, 'n) t): ('l, 'n) t =
	if zip.path = Root then zip else top (up zip)

let to_tree (zip: ('l, 'n) t): ('l, 'n) Tree.t =
	(top zip).cur

let of_tree (t: ('l, 'n) Tree.t): ('l, 'n) t =
	{ cur = t ; path = Root }

let current_path (z: ('l, 'n) t): 'n list =
	let rec aux (p:('l, 'n) path) (accu: 'n list): 'n list =
		begin match p with
		| Root -> accu
		| Child (_, n, _, q) -> aux q (n :: accu)
		end
	in aux z.path []

let add_sibling sib zip =
	begin match zip.path with
	| Root -> invalid_arg "add_sibling: zipper is at root."
	| Child (l, n, r, q) ->
			{zip with path = Child (l, n, sib::r, q) }
	end
