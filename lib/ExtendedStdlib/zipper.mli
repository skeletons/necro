(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

type ('l, 'n) path =
	| Root (** We are currently inspecting the root of the Tree.t *)
	| Child of ('l, 'n) Tree.t list * 'n * ('l, 'n) Tree.t list * ('l, 'n) path
	(** [Child (l, n, r, p)] means that we are currently looking at a child of
			node [n]. The siblings on the left are [List.rev l] and on the right are
			[r]. The path from [n] is [p]. *)

type ('l, 'n) t =
	{ cur: ('l, 'n) Tree.t
	; path: ('l, 'n) path}

val up: ('l, 'n) t -> ('l, 'n) t
val up_opt: ('l, 'n) t -> ('l, 'n) t option
val find: (('l, 'n) Tree.t -> bool) -> ('l, 'n) t -> ('l, 'n) t
val find_opt: (('l, 'n) Tree.t -> bool) -> ('l, 'n) t -> ('l, 'n) t option
val next_opt: ('l, 'n) t -> ('l, 'n) t option
val fold: (('l, 'n) t -> ('l, 'n) t) -> ('l, 'n) t -> ('l, 'n) t
val top: ('l, 'n) t -> ('l, 'n) t
val to_tree: ('l, 'n) t -> ('l, 'n) Tree.t
val of_tree: ('l, 'n) Tree.t -> ('l, 'n) t
val current_path: ('l, 'n) t -> 'n list
val add_sibling: ('l, 'n) Tree.t -> ('l, 'n) t -> ('l, 'n) t
