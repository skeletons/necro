(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

type ('leaf, 'node) t =
	| Leaf of 'leaf
	| Node of 'node * ('leaf, 'node) t list

val get_leaf: ('l, 'n) t -> 'l option
val get_node: ('l, 'n) t -> ('n * ('l, 'n) t list) option
val is_leaf: ('l, 'n) t -> bool
val is_node: ('l, 'n) t -> bool
val exists: (('leaf, 'node) t -> bool) -> ('leaf, 'node) t -> bool
val find_opt: (('leaf, 'node) t -> bool) -> ('leaf, 'node) t ->
	('leaf, 'node) t option
val fold:
	('a -> 'leaf -> 'a) ->
	('a -> 'node -> 'a) ->
	'a -> ('leaf, 'node) t -> 'a
val map: ('leaf -> 'l2) -> ('node -> 'n2) -> ('leaf, 'node) t -> ('l2, 'n2) t
val iter_prefix: ('leaf -> unit) -> ('node -> unit) -> ('leaf, 'node) t -> unit
val iter_postfix: ('leaf -> unit) -> ('node -> unit) -> ('leaf, 'node) t -> unit
