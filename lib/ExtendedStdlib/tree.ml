(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

type ('leaf, 'node) t =
	| Leaf of 'leaf
	| Node of 'node * ('leaf, 'node) t list

let get_leaf (tree: ('leaf, 'node) t): 'leaf option =
	begin match tree with
	| Leaf a -> Some a
	| Node _ -> None
	end

let get_node (tree: ('leaf, 'node) t): ('node * ('leaf, 'node) t list) option =
	begin match tree with
	| Leaf _ -> None
	| Node (a, t) -> Some (a, t)
	end

let is_leaf (tree: ('leaf, 'node) t): bool =
	begin match tree with
	| Leaf _ -> true
	| Node _ -> false
	end

let is_node (tree: ('leaf, 'node) t): bool =
	begin match tree with
	| Leaf _ -> false
	| Node _ -> true
	end

let exists (f: ('leaf, 'node) t -> bool) (tree: ('leaf, 'node) t): bool =
	begin match tree with
	| Leaf _ -> false
	| Node (_, l) -> List.exists f l
	end

let find_opt (f: ('leaf, 'node) t -> bool) (tree: ('leaf, 'node) t):
	('leaf, 'node) t option =
	begin match tree with
	| Leaf _ -> None
	| Node (_, l) -> List.find_opt f l
	end

let rec fold (fl: 'a -> 'leaf -> 'a) (fn: 'a -> 'node -> 'a) (accu: 'a) (t: ('leaf, 'node) t): 'a =
	begin match t with
	| Leaf l -> fl accu l
	| Node (n, l) ->
		let accu' = List.fold_left (fold fl fn) accu l in
		fn accu' n
	end

let rec map fl fn t =
	begin match t with
	| Leaf l -> Leaf (fl l)
	| Node (n, l) -> Node (fn n, List.map (map fl fn) l)
	end

let rec iter_prefix fl fn t =
	begin match t with
	| Leaf l -> fl l
	| Node (n, l) -> fn n ; List.iter (iter_prefix fl fn) l
	end

let rec iter_postfix fl fn t =
	begin match t with
	| Leaf l -> fl l
	| Node (n, l) -> List.iter (iter_postfix fl fn) l; fn n
	end
