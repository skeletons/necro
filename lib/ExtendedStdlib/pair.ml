(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

type ('a, 'b) t = 'a * 'b

let equal eqa eqb (a, b) (a', b')  = eqa a a' && eqb b b'
let compare cmpa cmpb (a, b) (a', b')  =
	let r = cmpa a a' in
	if r <> 0 then r else
	cmpb b b'
let map f g (x, y) = (f x, g y)
let map2 f g (x, y) (z, w) = (f x z, g y w)
let swap (x, y) = (y, x)
let fold f (x, y) = f x y
let create x y = (x, y)
let map_fst f (x, y) = (f x, y)
let map_snd g (x, y) = (x, g y)
let iter f g (a, b) = f a; g b
let iter_fst f (a, _) = f a
let iter_snd g (_, b) = g b
