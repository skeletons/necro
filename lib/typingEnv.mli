(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

open Util
open ParsedAST
open TypedAST


type te_type_decl = string option * type_kind * string list
(** for each type name, its special comment, the kind of the type, and its
type arguments *)

type te_variant_decl = constructor_signature list
(** for each variant type name, the list of constructors and their signatures *)

type te_record_decl = field_signature list
(** for each record type name, the list of fields and their signatures *)

type te_alias_decl = location * string list * typ
(** for each alias type name, the location where it was defined, the type
arguments and the aliased type. We keep the location for alias circularity
reporting *)

type te_constructor_decl = location * constructor_signature
(** for each constructor name, its signature *)

type te_field_decl = location * field_signature
(** for each field name, its signature *)

type te_term_decl = string option * term_kind * string list * typ * pterm option
(** for each term name, its special comment, its kind, its type arguments and
		its signature, plus optionnally the parsed term *)

type te_binder_decl = string TypedAST.with_path with_loc
(** for each binder, the associated term *)



(** Typing environments are created from the parsed AST, and are used in order
    to type the AST. They contain the structure and signature of defined types,
    constructors, fields, terms.
    There is some redundancy in them, this is done on purpose This way, it takes
    twice the time to create, but spares a lot of time when using it. For
    instance, te_variants and te_constructors give the same information, but if
    we had to find a constructor by looking at all variant types, it could be a
    lot longer *)
type t = {
	te_order : decl_kind list ;
	te_types : te_type_decl SMap.t ;
	te_variants : te_variant_decl SMap.t ;
	te_records : te_record_decl SMap.t ;
	te_aliases : te_alias_decl SMap.t ;
	te_constructors : te_constructor_decl SMap.t ;
	te_fields : te_field_decl SMap.t ;
	te_terms : te_term_decl SMap.t ;
	te_binders : te_binder_decl SMap.t ;
  te_includes : string with_loc list
}

val te_types        : t -> te_type_decl        SMap.t
val te_variants     : t -> te_variant_decl     SMap.t
val te_records      : t -> te_record_decl      SMap.t
val te_aliases      : t -> te_alias_decl       SMap.t
val te_constructors : t -> te_constructor_decl SMap.t
val te_fields       : t -> te_field_decl       SMap.t
val te_terms        : t -> te_term_decl        SMap.t
val te_binders      : t -> te_binder_decl      SMap.t

val empty_te: t






(* [create te_deps ast] takes the parsed AST [ast], and the typing
	 environments for dependencies [te_deps], and produces a typing environment
	 for [ast]. *)
val create: t SMap.t -> string with_loc list * ParsedAST.t -> t








val ptyp_to_typ: SSet.t -> ParsedAST.ptyp -> TypedAST.typ
(** [ptyp_to_typ tvars ty] gives the typed AST version of [ty], where [tvars] is
    the set of defined typed variables in the scope of [ty]. *)

val te_get: t SMap.t -> t -> (t -> 'a SMap.t) -> string with_path -> 'a
(** [te_get] finds a value which path is given in a typing environment *)

(*
val unalias_te: t SMap.t -> t -> (typ -> typ)
(** [unalias_te _ ty] returns a type that is equivalent to [ty], and that is
		not an alias. Formally, for any type [ty], and any [deps_te] and [te]:
		- [type_equals_te deps_te te ty (unalias_te deps_te te ty) = true]
		- [unalias_te deps_te te ty] does not match [UserType (_, Alias, _)]*)

val unalias_rec_te: t SMap.t -> t -> (typ -> typ)
(** [unalias_rec_te _ ty] returns a type that is equivalent to [ty], and that
		contains no alias. Formally, for any type [ty], and any [deps_te] and [te]:
		- [type_equals_te deps_te te ty (unalias_rec_te deps_te te ty) = true]
		- no subterm of [unalias_rec_te deps_te te ty] matches [UserType (_, Alias, _)]*)
*)

val te_of_ss: TypedAST.t -> t
(** [te_of_ss deps t] produces a {!TypingEnv.t} out of the typed AST [t]. *)

