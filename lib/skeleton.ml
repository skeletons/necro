(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

open TypedAST
open Interpretation
open Util

type dependencies = (string * skeletal_semantics) list

(* Check that the number of given type arguments fits *)
let check_type_args loc typ name got expected =
  let ng = List.length got in
  let ne = List.length expected in
  if ng <> ne then
    Errors.type_error loc (WrongNumberArgs {typ;name;expected=ne;given=ng})

(* Get the list of variables in a pattern *)
let pattern_variables: pattern -> typ SMap.t =
  let rec aux accu p =
    begin match p.pdesc with
    | PWild -> accu
    | PVar x -> SMap.add x p.ptyp accu
    | PTuple l -> List.fold_left aux accu l
    | PConstr (_, p) -> aux accu p
    | PRecord l -> List.fold_left (fun accu (_, p) -> aux accu p) accu l
    | POr (p, _) -> aux accu p
    | PType (p, _) -> aux accu p
    end
  in aux SMap.empty

let (free_variables_skel , free_variables_term) =
    let interp : (typ SMap.t, typ SMap.t) interpretation =
      {
        var_interp = (fun v ->
          begin match v with
          | TopLevel _ -> SMap.empty
          | LetBound (x, ty) -> SMap.singleton x ty
          end) ;
        constr_interp = (fun _ t -> t) ;
        tuple_interp = (fun l -> smap_list_union l) ;
        function_interp = (fun p s ->
          smap_diff s (pattern_variables p)) ;
        field_interp = (fun t _ -> t) ;
        nth_interp = (fun t _ _ -> t) ;
        record_interp = (fun l ->
          List.map snd l |> smap_list_union ) ;
        recset_interp = (fun t l ->
          List.map snd l
          |> smap_list_union
          |> SMap.union (fun _ _ x -> Some x) t) ;
        merge_interp = (fun _ l -> smap_list_union l) ;
        match_interp = (fun s psl ->
          List.map (fun (p, s) -> smap_diff s (pattern_variables p)) psl
          |> smap_list_union
          |> SMap.union (fun _ _ x -> Some x) s) ;
        letin_interp = (fun _ p s1 s2 ->
          SMap.union (fun _ _ x -> Some x) s1
            (smap_diff s2 (pattern_variables p))) ;
        exists_interp = (fun _ -> SMap.empty) ;
        return_interp = (fun t -> t) ;
        apply_interp = (fun f x -> SMap.union (fun _ _ x -> Some x) f x)
      }
    in
   (interpret_skeleton interp,
    interpret_term     interp)





let get_constructors_by_type ss =
  SMap.fold (fun ty (_, td) l ->
    begin match td with
    | TDVariant (ta, cl) ->
        (ty, ta, cl) :: l
    | _ -> l
    end) ss.ss_types []

let get_fields_by_type ss =
  SMap.fold (fun ty (_, td) l ->
    begin match td with
    | TDRecord (ta, fl) -> (ty, ta, fl) :: l
    | _ -> l
    end) ss.ss_types []

let get_all_constructors ss =
  List.concat_map (fun (_,_,x) -> x) (get_constructors_by_type ss)

let get_all_fields ss =
  List.concat_map (fun (_,_,x) -> x) (get_fields_by_type ss)





(* Generate dependency graph *)

module Vertex = struct
  type t = (string * string option)
  let compare = Pair.compare String.compare (Option.compare String.compare)
  let hash = Hashtbl.hash
  let equal = (=)
end

module PSet = Set.Make(Vertex)
module PGraph = Graph.Persistent.Digraph.Concrete(Vertex)

let dependencies_interpretation: (PSet.t, PSet.t) interpretation =
  let pset_list_union =
    List.fold_left PSet.union PSet.empty
  in
  { var_interp = (fun x ->
      begin match x with
      | LetBound _ -> PSet.empty
      | TopLevel (_, x, _, _) ->
          PSet.singleton x
      end)
  ; constr_interp = (fun _ t -> t)
  ; tuple_interp = (fun tl ->
    pset_list_union tl)
  ; function_interp = (fun _ s -> s)
  ; field_interp = (fun t _ -> t)
  ; nth_interp = (fun t _ _ -> t)
  ; record_interp = (fun tl ->
        pset_list_union (List.map (fun (_, x) -> x) tl))
  ; recset_interp = (fun t tl ->
        PSet.union t
        (pset_list_union (List.map (fun (_, x) -> x) tl)))
  ; letin_interp = (fun b _ s1 s2 ->
      let bind =
        begin match b with
        | NoBind -> Fun.id
        | TermBind (_, x, _, _) | SymbolBind (_, _, x, _, _) ->
            PSet.add x
        end
      in
      bind @@ PSet.union s1 s2)
  ; exists_interp = (fun _ -> PSet.empty)
  ; apply_interp = (fun f x -> PSet.union f x)
  ; merge_interp = (fun _ bl -> pset_list_union bl)
  ; match_interp = (fun s psl ->
    PSet.union s (pset_list_union (List.map snd psl)))
  ; return_interp = (fun s -> s)
  }

let term_dep_graph deps sem =
  let g = PGraph.empty in
  let aux_vertices sem path g =
    (* add vertices *)
    SMap.fold (fun name _ g ->
      PGraph.add_vertex g (name, path)) sem.ss_terms g
  in
  let aux_edges sem path g =
    (* add edges *)
    SMap.fold (fun name (_, (_, _, t)) g ->
      begin match t with
      | None -> g
      | Some t ->
          let deps = interpret_term dependencies_interpretation t in
          PSet.fold (fun b g ->
            PGraph.add_edge g b (name, path)) deps g
      end) sem.ss_terms g
  in

  let g = aux_vertices sem None g in
  let g = List.fold_left (fun g (file, file_sem) ->
    aux_vertices file_sem (Some file) g) g deps in

  let g = aux_edges sem None g in
  let g = List.fold_left (fun g (file, file_sem) ->
    aux_edges file_sem (Some file) g) g deps in
  g

let type_dep_graph deps sem =
  let g = PGraph.empty in

  (* Vertices *)
  let aux_vertices sem path g =
    SMap.fold (fun name _ g ->
      PGraph.add_vertex g (name, path)) sem.ss_types g
  in
  let g = aux_vertices sem None g in
  let g = List.fold_left (fun g (file, file_sem) ->
    aux_vertices file_sem (Some file) g) g deps in

  (* Edges *)
  let types_in ty =
    let rec add_types_in ty accu =
      begin match ty.tydesc with
      | Variable _ -> accu
      | Arrow (i, o) ->
          add_types_in i @@ add_types_in o @@ accu
      | Product tyl -> List.fold_right add_types_in tyl accu
      | UserType (ty, _) -> PSet.add ty accu
      end
    in
    add_types_in ty PSet.empty
  in
  (* add edges for every type *)
  let aux_edges sem path g =
    SMap.fold (fun name (_, td) g ->
      let deps =
        begin match td with
        | TDUnspec _ -> PSet.empty
        | TDVariant (_, csl) ->
            List.map (fun cs -> cs.cs_input_type) csl |>
            List.fold_left (fun accu ty ->
              PSet.union accu (types_in ty)) PSet.empty
        | TDRecord (_, fsl) ->
            List.map (fun fs -> fs.fs_field_type) fsl |>
            List.fold_left (fun accu ty ->
              PSet.union accu (types_in ty)) PSet.empty
        | TDAlias (_, ty) -> types_in ty
        end
      in PSet.fold (fun b g ->
        PGraph.add_edge g b (name, path)) deps g) sem.ss_types g
  in
  let g = aux_edges sem None g in
  let g = List.fold_left (fun g (file, file_sem) ->
    aux_edges file_sem (Some file) g) g deps in
  g

let all_variables_ss ss =
  let rec all_variables_pattern accu (p: pattern) =
    begin match p.pdesc with
    | PWild -> accu
    | PVar x -> SSet.add x accu
    | PConstr (_, p) -> all_variables_pattern accu p
    | PTuple pl -> List.fold_left all_variables_pattern accu pl
    | PRecord fpl ->
        List.fold_left (fun accu (_,p) ->
          all_variables_pattern accu p) accu fpl
    | POr (p1, p2) ->
        let accu = all_variables_pattern accu p1 in
        all_variables_pattern accu p2
    | PType (p, _) -> all_variables_pattern accu p
    end
  in
  let rec all_variables_term accu t =
    begin match t.tdesc with
    | TVar (LetBound (x, _)) -> SSet.add x accu
    | TVar _ -> accu
    | TConstr (_, t) -> all_variables_term accu t
    | TTuple tl -> List.fold_left all_variables_term accu tl
    | TFunc (p, s) ->
        let accu = all_variables_pattern accu p in
        all_variables_skel accu s
    | TField (t, _) -> all_variables_term accu t
    | TNth (t, _, _) -> all_variables_term accu t
    | TRecMake ftl ->
        List.fold_left (fun accu (_, t) ->
          all_variables_term accu t) accu ftl
    | TRecSet (t, ftl) ->
        let accu = all_variables_term accu t in
        List.fold_left (fun accu (_, t) ->
          all_variables_term accu t) accu ftl
    end
  and all_variables_skel accu s =
    begin match s.sdesc with
    | Branching (_, sl) ->
        List.fold_left all_variables_skel accu sl
    | Match (s, psl) ->
        let accu = all_variables_skel accu s in
        List.fold_left (fun accu (p, s) ->
          let accu = all_variables_pattern accu p in
          all_variables_skel accu s) accu psl
    | LetIn (_, p, s1, s2) ->
        let accu = all_variables_pattern accu p in
        let accu = all_variables_skel accu s1 in
        all_variables_skel accu s2
    | Exists _ -> accu
    | Return t -> all_variables_term accu t
    | Apply (f, x) ->
        let accu = all_variables_term accu f in
        all_variables_term accu x
    end
  in
  SMap.fold (fun x (_, (_, _, t)) set ->
    let set = SSet.add x set in
    begin match t with
    | None -> set
    | Some t -> all_variables_term set t
    end) ss.ss_terms SSet.empty

(* We return this, it forgets renaming of let-bound variables, because
   alpha-conversion says it doesn't matter *)
type protect_remapping =
  { fields : string SMap.t
  ; types : string SMap.t
  ; terms : string SMap.t
  }
type remapping =
  { fields : string SMap.t
  ; types : string SMap.t
  ; vars : string SMap.t
  }


let protect_ss kw ss =
  let all_fields = get_all_fields ss |> List.map (fun fsig -> fsig.fs_name) in
  let all_types = ss.ss_types |> SMap.bindings |> List.map fst in
  let all_vars = SSet.elements (all_variables_ss ss) in
  let all_terms = ss.ss_terms |> SMap.bindings |> List.map fst in
  let remap l =
    let rec aux reserved l map =
      begin match l with
      | [] -> map
      | a :: q when not (SSet.mem a reserved) ->
          aux (SSet.add a reserved) q map
      | a :: q ->
          let a' = Util.fresh (fun x -> not (SSet.mem x reserved)) a in
          aux (SSet.add a' reserved) q (SMap.add a a' map)
      end
    in aux kw l SMap.empty
  in
  let remapping =
    { fields = remap all_fields
    ; types = remap all_types
    ; vars = remap all_vars}
  in
  let remapping' =
    { fields = remapping.fields
    ; types = remapping.types
    ; terms = SMap.filter (fun x _ -> List.mem x all_terms) remapping.vars}
  in
  let rename x m =
    begin match SMap.find_opt x m with
    | None -> x
    | Some y -> y
    end
  in
  let rename_with_path (x, path) m =
    (rename x m, path)
  in
  let rec rename_type ty =
    {ty with tydesc =
      begin match ty.tydesc with
      | Variable x -> Variable x
      | UserType (n, tyl) ->
          UserType (
            rename_with_path n remapping.types,
            List.map rename_type tyl)
      | Arrow (i, o) ->
          Arrow (rename_type i, rename_type o)
      | Product tyl ->
          Product (List.map rename_type tyl)
      end}
  in
  let rename_constr (c, (ty, tyl)) =
    (c, (rename ty remapping.types, List.map rename_type tyl))
  in
  let rename_field (f, (ty, tyl)) =
    (rename_with_path f remapping.fields,
      (rename ty remapping.types, List.map rename_type tyl))
  in
  let rename_bind b =
    begin match b with
    | NoBind -> NoBind
    | TermBind (k, n, ta, ty) ->
        TermBind (
          k,
          rename_with_path n remapping.vars,
          List.map rename_type ta,
          rename_type ty)
    | SymbolBind (sym, k, n, ta, ty) ->
        SymbolBind (
          sym, k,
          rename_with_path n remapping.vars,
          List.map rename_type ta,
          rename_type ty)
    end
  in
  let rec rename_pattern p =
    (fun pdesc -> {p with pdesc})
    begin match p.pdesc with
    | PWild -> PWild
    | PVar x ->
        PVar (rename x remapping.vars)
    | PConstr (c, p) ->
        PConstr (rename_constr c, rename_pattern p)
    | PTuple pl ->
        PTuple (List.map rename_pattern pl)
    | PRecord fpl ->
        PRecord (List.map (fun (f, p) ->
          (rename_field f, rename_pattern p)) fpl)
    | POr (p1, p2) ->
        POr (rename_pattern p1, rename_pattern p2)
    | PType (p, ty) ->
        PType (rename_pattern p, rename_type ty)
    end
  in
  let rec rename_term t =
    (fun tdesc ->
      {t with tdesc; ttyp=rename_type t.ttyp})
    begin match t.tdesc with
    | TVar (LetBound (x, ty)) ->
        TVar (LetBound (rename x remapping.vars, rename_type ty))
    | TVar (TopLevel (k, x, ta, ty)) ->
        TVar (TopLevel (k, rename_with_path x remapping.vars,
          List.map rename_type ta, rename_type ty))
    | TConstr (c, t) ->
        TConstr (rename_constr c, rename_term t)
    | TTuple tl ->
        TTuple (List.map rename_term tl)
    | TFunc (p, s) ->
        TFunc (rename_pattern p, rename_skel s)
    | TField (t, f) ->
        TField (rename_term t, rename_field f)
    | TNth (t, tyl, n) ->
        TNth (rename_term t, List.map rename_type tyl, n)
    | TRecMake ftl ->
        TRecMake (List.map (fun (f, t) ->
          (rename_field f, rename_term t)) ftl)
    | TRecSet (t, ftl) ->
        TRecSet (rename_term t, List.map (fun (f, t) ->
          (rename_field f, rename_term t)) ftl)
    end
  and rename_skel s =
    (fun sdesc ->
      {s with sdesc; styp=rename_type s.styp})
    begin match s.sdesc with
    | Branching (ty, sl) ->
        Branching (rename_type ty, List.map rename_skel sl)
    | Match (s, psl) ->
        Match (rename_skel s, List.map (fun (p, s) ->
          (rename_pattern p, rename_skel s)) psl)
    | LetIn (bind, p, s1, s2) ->
        LetIn (rename_bind bind, rename_pattern p, rename_skel s1, rename_skel s2)
    | Exists ty ->
        Exists (rename_type ty)
    | Return t -> Return (rename_term t)
    | Apply (f, x) -> Apply (rename_term f, rename_term x)
    end
  in
  let ss_terms =
    SMap.fold (fun x (com, (ta, ty, t)) map ->
      let x' = rename x remapping.vars in
      let ty' = rename_type ty in
      let t' = Option.map rename_term t in
      let a' = (com, (ta, ty', t')) in
      SMap.add x' a' map) ss.ss_terms SMap.empty
  in
  let ss_types =
    SMap.fold (fun x (com, td) map ->
      let x' = rename x remapping.types in
      let td' =
        begin match td with
        | TDUnspec _ -> td
        | TDVariant (ta, csl) ->
            let csl' = List.map (fun cs ->
              { cs_name = cs.cs_name
              ; cs_input_type = rename_type cs.cs_input_type
              ; cs_variant_type =
                rename cs.cs_variant_type remapping.types
              ; cs_comment = cs.cs_comment
              ; cs_type_args = cs.cs_type_args}) csl in
            TDVariant (ta, csl')
        | TDRecord (ta, fsl) ->
            let fsl' = List.map (fun fs ->
              { fs_name = rename fs.fs_name remapping.fields
              ; fs_field_type = rename_type fs.fs_field_type
              ; fs_record_type =
                rename fs.fs_record_type remapping.types
              ; fs_comment = fs.fs_comment
              ; fs_type_args = fs.fs_type_args}) fsl in
            TDRecord (ta, fsl')
        | TDAlias (ta, ty) ->
            TDAlias (ta, rename_type ty)
        end
      in
      let a' = (com, td') in
      SMap.add x' a' map) ss.ss_types SMap.empty
  in
  let ss_binders =
    SMap.fold (fun x (k, name) map ->
      let x' = x in
      let name' = rename_with_path name remapping.vars in
      let a' = (k, name') in
      SMap.add x' a' map) ss.ss_binders SMap.empty
  in
  let ss_order = List.map (begin function
    | ParsedAST.TypeDecl t ->
        ParsedAST.TypeDecl (rename t remapping.types)
    | TermDecl t ->
        TermDecl (rename t remapping.vars)
    | BinderDecl _ | Comment _ as d -> d
    end) ss.ss_order in
  let ss_includes = ss.ss_includes in
  { ss_order; ss_types; ss_terms; ss_binders; ss_includes}, remapping'


let rec add_from m ty =
  let replace n = match n with | None -> m | Some _ -> n in
  let ret tydesc = {ty with tydesc} in
  begin match ty.tydesc with
  | UserType ((a, n), b) ->
      ret @@ UserType ((a, replace n), List.map (add_from m) b)
  | Variable _ -> ty
  | Arrow (i, o) -> ret @@ Arrow (add_from m i, add_from m o)
  | Product tyl -> ret @@ Product (List.map (add_from m) tyl)
  end


let get_sem deps sem m =
  begin match m with
  | None -> sem
  | Some x -> List.assoc x deps
  end

let update_constr m csig =
  { cs_name = csig.cs_name
  ; cs_type_args = csig.cs_type_args
  ; cs_comment = csig.cs_comment
  ; cs_variant_type = csig.cs_variant_type
  ; cs_input_type = add_from m csig.cs_input_type }

let update_field m fsig =
  { fs_name = fsig.fs_name
  ; fs_type_args = fsig.fs_type_args
  ; fs_comment = fsig.fs_comment
  ; fs_record_type = fsig.fs_record_type
  ; fs_field_type = add_from m fsig.fs_field_type }

let get_constructor_signature deps sem (c, m) =
  let sem_ty = get_sem deps sem m in
  let all_constr = get_all_constructors sem_ty in
  let csig = List.find (fun cs -> cs.cs_name = c) all_constr in
  update_constr m csig

let get_field_signature deps sem (f, m) =
  let sem_ty = get_sem deps sem m in
  let all_fields = get_all_fields sem_ty in
  let fsig = List.find (fun fs -> fs.fs_name = f) all_fields in
  update_field m fsig

let get_type_kind deps sem (ty, m) =
  let sem_ty = get_sem deps sem m in
  match SMap.find ty sem_ty.ss_types with
  | _, TDUnspec _ -> Unspec
  | _, TDVariant _ -> Variant
  | _, TDRecord _ -> Record
  | _, TDAlias _ -> Alias

let get_type_definition deps sem (ty, m)=
  let sem_ty = get_sem deps sem m in
  match SMap.find ty sem_ty.ss_types with
  | _, TDUnspec i -> TDUnspec i
  | _, TDVariant (ta, csl) -> TDVariant (ta, List.map (update_constr m) csl)
  | _, TDRecord (ta, fsl) -> TDRecord (ta, List.map (update_field m) fsl)
  | _, TDAlias (ta, ty) -> TDAlias (ta, add_from m ty)

let rec unalias
    (deps: dependencies)
    (sem: skeletal_semantics)
    (ty: typ):
      typ =
  begin match ty.tydesc with
  | UserType ((n, m), ta_got) ->
      let sem_ty =
        begin match m with
        | None -> sem
        | Some x -> List.assoc x deps
        end
      in
      begin match SMap.find_opt n sem_ty.ss_types with
      | None -> Errors.type_error ty.tyloc (Unbound {typ="type";name=n})
      | Some (_, TDAlias (ta, ty)) ->
        let ty = add_from m ty in
        check_type_args ghost_loc "type alias" n ta_got ta ;
        unalias deps sem (Types.subst_mult ta ta_got ty)
      | _ -> ty
      end
  | _ -> ty
  end

let rec unalias_rec
    (deps: dependencies)
    (sem: skeletal_semantics)
    (ty: typ):
      typ =
  {ty with tydesc=
    begin match ty.tydesc with
    | Variable x -> Variable x
    | UserType ((n, m), ta) ->
        let sem_ty =
          begin match m with
          | None -> sem
          | Some x -> List.assoc x deps
          end
        in
        begin match SMap.find_opt n sem_ty.ss_types with
        | None -> Errors.type_error ty.tyloc (Unbound {typ="type";name=n})
        | Some (_, TDAlias _) ->
            (unalias_rec deps sem (unalias deps sem ty)).tydesc
        | _ ->
            UserType ((n, m), List.map (unalias_rec deps sem) ta)
        end
    | Arrow (i, o) ->
        Arrow (unalias_rec deps sem i, unalias_rec deps sem o)
    | Product tyl ->
        Product (List.map (unalias_rec deps sem) tyl)
    end}

let rec type_equals
    (deps: dependencies)
    (sem: skeletal_semantics)
    (ty1: typ)
    (ty2: typ):
      bool =
  begin match
    (unalias deps sem ty1).tydesc,
    (unalias deps sem ty2).tydesc with
  | UserType (n1, ta1), UserType (n2, ta2) ->
      Pair.equal String.equal (Option.equal String.equal) n1 n2 &&
      List.compare_lengths ta1 ta2 = 0 &&
      List.for_all2 (type_equals deps sem) ta1 ta2
  | UserType _, _ | _, UserType _ -> false
  | Arrow (i1, o1), Arrow (i2, o2) ->
      type_equals deps sem i1 i2 &&
      type_equals deps sem o1 o2
  | Arrow _, _ | _, Arrow _ -> false
  | Variable x, Variable y -> String.equal x y
  | Variable _, _ | _, Variable _ -> false
  | Product tyl1, Product tyl2 ->
      List.compare_lengths tyl1 tyl2 = 0 &&
      List.for_all2 (type_equals deps sem) tyl1 tyl2
  end

let unify deps sem ty1 ty2 =
  let (let$) x f =
    begin match x with
    | None -> None
    | Some y -> f y
    end
  in
  let rec aux ty1 ty2 accu =
    begin match
      (unalias deps sem ty1).tydesc,
      (unalias deps sem ty2).tydesc with
    | Variable a, _ ->
        begin match SMap.find_opt a accu with
        | None -> Some (SMap.add a ty2 accu)
        | Some ty' -> if type_equals deps sem ty' ty2 then Some accu else None
        end
    | Arrow (ty1i, ty1o), Arrow (ty2i, ty2o) ->
        let$ accu = aux ty1i ty2i accu in
        aux ty1o ty2o accu
    | Arrow _, _ -> None
    | UserType (s1, a1), UserType (s2, a2) ->
      if Pair.equal String.equal (Option.equal String.equal) s1 s2 (* same type *)
      then aux {ty1 with tydesc=Product a1} {ty2 with tydesc=Product a2} accu (* same type arguments *)
      else None
    | UserType _, _ -> None
    | Product [], Product [] -> Some accu
    | Product (a1 :: q1), Product (a2 :: q2) ->
        let$ accu = aux a1 a2 accu in
        aux {ty1 with tydesc = Product q1} {ty2 with tydesc = Product q2} accu
    | Product _, _ -> None
    end
  in
  aux ty1 ty2 SMap.empty

(** Using the algorithm described by Luc Maranget in "Les avertissements du
    filtrage", proceedings of the JFLA 2003 *)
let rec useful (deps:dependencies) sem pll pl =
  (* if [pll] is empty, then [pl] matches something, which is therefore
     something new (no empty types) *)
  if pll = [] then true else
  (* No column: the only value is (), so [pl] is useful iff [pll] is empty *)
  if pl = [] then false else
  (* case (c): p is an or-pattern *)
  let rec untype p =
    begin match p.pdesc with
    | PType (p, _) -> untype p
    | _ -> p
    end
  in
  let untype_head pl =
    begin match pl with
    | p :: pq -> untype p :: pq
    | [] -> []
    end
  in
  let is_or_pattern, result =
    begin match untype_head pl with
    | {pdesc=POr (p1, p2);_} :: pq ->
        (true, fun () ->
        useful deps sem pll (p1::pq) ||
        useful deps sem pll (p2::pq))
    | _ -> false, fun () -> assert false
    end
  in
  if is_or_pattern then result () else
  (* remove or-patterns from the first column of pll *)
  let rec remove_or p =
    begin match untype p with
    | {pdesc=POr (p1, p2);_} -> remove_or p1 @ remove_or p2
    | _ -> [p]
    end
  in
  let pll = List.concat_map (fun pl ->
    begin match pl with
    | p :: pq -> List.map (fun x -> x :: pq) (remove_or p)
    | [] -> assert false (* pl≠[] so elements of pll should be ≠[] as well *)
    end) pll
  in
  let p1, pq =
    begin match pl with
    | a :: q -> a, q
    | [] -> assert false (* we tested it above *)
    end
  in
  let ty = p1.ptyp in
  begin match (unalias deps sem ty).tydesc with
  | Product tyl -> (* decompose the patterns *)
      let untuple p =
        begin match (untype p).pdesc with
        | PTuple pl -> pl
        | PWild | PVar _ ->
            List.map (fun ty -> {pdesc=PWild;ploc=ghost_loc; ptyp=ty}) tyl
        | _ -> assert false
        end
      in
      let pll =
        List.map (function | [] -> assert false | a :: q -> untuple a @ q) pll
      in
      let pl =
        begin match pl with | [] -> assert false | a :: q -> untuple a @ q end
      in useful deps sem pll pl
  | Arrow _ | Variable _ ->
      (* Only _ matches these types *)
      useful deps sem (List.map List.tl pll) pq
  | UserType (ty, ta) ->
      begin match get_type_kind deps sem ty with
      | Unspec ->
        useful deps sem (List.map List.tl pll) pq (* Only _ matches this types *)
      | Alias -> assert false (* unaliased *)
      | Record -> (* decompose the patterns *)
        let fields =
          match get_type_definition deps sem ty with
          | TDRecord (_, fsl) -> fsl
          | _ -> assert false
        in
        let fields = List.map (fun fsig ->
          let ftyp =
            Types.subst_mult fsig.fs_type_args ta fsig.fs_field_type in
          (fsig.fs_name, ftyp)) fields in
        let unrec p =
          begin match (untype p).pdesc with
          | PWild | PVar _ ->
              List.map (fun (_, ftyp) ->
                {pdesc=PWild;ptyp=ftyp;ploc=ghost_loc}) fields
          | PRecord fpl ->
              List.map (fun (f, ftyp) ->
                begin match List.find_opt (fun (((f', _), _),_) -> f' = f) fpl with
                | Some (_, p) -> p
                | None -> {pdesc=PWild; ptyp=ftyp; ploc=ghost_loc}
                end) fields
          | _ -> assert false
          end
        in
        let pll =
          List.map (function | [] -> assert false | a :: q -> unrec a @ q) pll
        in
        let pl =
          begin match pl with | [] -> assert false | a :: q -> unrec a @ q end
        in useful deps sem pll pl
      | Variant ->
        let constructors =
          match get_type_definition deps sem ty with
          | TDVariant (_, csl) -> csl
          | _ -> assert false
        in
        begin match untype_head pl with
        | {pdesc=PConstr (c, p);_} :: pq -> (* case (a) *)
            let p_typ = p.ptyp in
            let select pl =
              begin match pl with
              | {pdesc=PConstr (c', p');_} :: pq when c = c' -> [p' :: pq]
              | {pdesc=PConstr (_, _);_} :: _ -> []
              | {pdesc=(PWild  | PVar _);_} :: pq ->
                  [{pdesc=PWild; ptyp=p_typ; ploc=ghost_loc} :: pq]
              | _ -> assert false
              end
            in
            let scp = List.concat_map select pll in
            useful deps sem scp (p :: pq)
        | {pdesc=PWild;_} :: pq | {pdesc=PVar _;_} :: pq -> (* case (b) *)
            (* scp : to each constructor, bind the list of patterns *)
            let rec cut pl: string option * pattern list =
              begin match pl with
              | {pdesc=PConstr (((c, _), _), p);_} :: pq ->
                  (Some c, p :: pq)
              | {pdesc=(PWild | PVar _);_} :: pq ->
                  (None, pq)
              | {pdesc=(PType (p, _));_} :: pq -> cut (p :: pq)
              | _ -> assert false
              end
            in
            (* careful, here scp' is not [{S(c, p)| c in constructors}], since
               default cases are missing, so you have to add [D(p)] *)
            let scp', dp = partition cut pll in
            (* is there any unmatched constructor? *)
            let use_default =
              let constructors_matched = SMap.bindings scp' in
              List.compare_lengths constructors constructors_matched <> 0
            in
            SMap.exists (fun c scp' ->
              let cs = List.find (fun cs -> cs.cs_name = c) constructors in
              let ty = Types.subst_mult cs.cs_type_args ta cs.cs_input_type in
              let scp =
                List.map (fun pq -> {pdesc=PWild; ptyp=ty;ploc=ghost_loc} :: pq) dp @ scp'
              in
              let pl = {pdesc=PWild; ptyp=ty; ploc=ghost_loc} :: pq in
              useful deps sem scp pl) scp' ||
            (use_default && useful deps sem dp pq)
        | _ -> assert false
        end
      end
  end


