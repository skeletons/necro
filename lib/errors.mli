(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

(** This file performs all error handling *)
open TypedAST

exception TypeError

type warning =
	| NotExhaustive                          (* 1 *)
	| OneBranch                              (* 2 *)
	| RedundantPattern                       (* 3 *)
	| Unused of {typ:string; name:string}    (* 4 *)

type warn_list = int list
val warning_infos: (int * string) list
val all_warnings: warn_list

type error =
	| CannotInfer
	| CannotInferPatternType
	| ComponentZero
	| Cycle of {typ:string; cycle:string list}
	| DifferentVariableOrPattern of string
	| DifferentVariableTypeOrPattern of {name:string; left:typ; right:typ}
	| EmptyBranching
	| EmptyMatch
	| IncompatibleBranches of string * typ list
	| MissingFields of string list
	| NoSuchComponent of int
	| NoSuchConstructor of string * typ
	| NoSuchField of string * typ
	| NotAnArrow of typ
	| NotAProduct
	| NotARecord
	| NotArrowType of typ
	| NotBindType of {name:string; typ:typ}
	| NotProductType of typ
	| NoTypeMatch of typ
	| ReboundPattern of string
	| Shadowing of string
	| SkeletonInsteadTerm
	| TupleWrongLength of {got:int; expected:typ}
	| TypeVariablePoly of string
	| UnboundBinder of string
	| UnboundFile of string
	| Unbound of {typ:string; name:string}
	| UntypablePattern
	| VarUnification of {name:string; typ1:typ; typ2:typ}
	| WrongNumberArgs of {typ:string; name:string; given:int; expected:int}
	| WrongType of {given:typ; expected:typ}

(** Errors that have 2 locations *)
type error2 =
	| Redefinition of {typ:string; name:string}


val type_error: Util.location -> error -> 'a
val type_error2: Util.location -> Util.location -> error2 -> 'a
val type_warning: warn_list -> Util.location -> warning -> unit
