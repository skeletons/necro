(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

%{
  open ParsedAST
  open Util

  (** type (), needed to unsugar constructors *)
  let ptyp_unit pos:ptyp =
    {contents=Ptyp_product []; loc=(pos, pos)}

  (** term (), needed to unsugar constructors *)
  let pterm_top:pterm =
    {contents=Pterm_tuple []; loc=ghost_loc}

  (** pattern (), needed to unsugar constructors *)
  let ppat_top pos:ppattern =
    {contents=Ppat_tuple []; loc=(pos, pos)}

  let rec mkfunc pl s =
    begin match pl with
    | [] -> s
    | a :: q ->
        {contents =
          Pskel_return {
            contents =Pterm_function (a, mkfunc q s);
            loc = fst a.loc, snd s.loc} ;
        loc = fst a.loc, snd s.loc }
    end

  (* check for duplicate declarations *)
  let check_for_duplicates (l: declaration with_loc list) =
    let get_info decl: string * string * location =
      begin match decl.contents with
      | Decl_nonspec (_, {contents=name;loc}, _) ->
          "type", name, loc
      | Decl_variant (_, {contents=name;loc}, _, _) ->
          "type", name, loc
      | Decl_record (_, {contents=name;loc}, _, _) ->
          "type", name, loc
      | Decl_alias (_, {contents=name;loc}, _, _) ->
          "type", name, loc
      | Decl_pterm (_, {contents=name;loc}, _, _, _, _) ->
          "term", name, loc
      | Decl_binder (name, _) ->
          "symbol", name, decl.loc
      | Decl_comment _ -> assert false
      end
    in
    let valuate decl =
      let (i, n, _) = get_info decl in i, n
    in
    let l', cl, fl =
      List.fold_left (fun (l', cl, fl) d ->
        begin match d.contents with
        | Decl_comment _ -> (l', cl, fl)
        | Decl_variant (_, _, _, cl') ->
            (d :: l', cl @ cl', fl)
        | Decl_record (_, _, _, fl') ->
            (d :: l', cl, fl @ fl')
        | _ -> (d :: l', cl, fl)
        end) ([], [], []) l
    in
    begin match find_duplicate l' valuate with
    | Some (d2, d1) ->
        let ((typ, name, loc1), (_, _, loc2)) = get_info d1, get_info d2 in
        Errors.type_error2 loc2 loc1 (Redefinition {typ; name})
    | None ->
        begin match find_duplicate fl (fun (_, f, _) -> f.contents) with
        | Some ((_, {contents=name;loc=loc1}, _), (_, {loc=loc2;_}, _)) ->
            Errors.type_error2 loc2 loc1
              (Redefinition {typ="field"; name})
        | None ->
            begin match find_duplicate cl (fun (_, c, _) -> c.contents) with
            | Some ((_, {contents=name;loc=loc1}, _), (_, {loc=loc2;_}, _)) ->
                Errors.type_error2 loc2 loc1
                  (Redefinition {typ="constructor"; name})
            | None -> l
            end
        end
    end

  (* binds the special comment to the right declaration.
     *Disclaimer:* [l] is the list of declarations in the file, it can be a very
     long list, it is important to use tail-recursive functions. *)
  let rec bind_comments l =
    let aux = bind_comments in
    begin match l with
    (* over: put the list in the right order and return *)
    | [] -> []
    (* a comment followed by a declaration on the next line:
      bind them together if possible *)
    | {contents=Decl_comment c;loc} as com :: d :: q
        when (snd loc).pos_lnum + 1 = (fst d.loc). pos_lnum ->
          begin match d.contents with
          | Decl_binder _ ->
              com.contents :: d.contents :: aux q
          | Decl_nonspec (o, x, n) ->
              assert (o = None) ;
              Decl_nonspec (Some c.contents, x, n) :: aux q
          | Decl_variant (o, x, ta, cl) ->
              assert (o = None) ;
              aux (mkloc d.loc (Decl_variant (Some c.contents, x, ta, cl)) :: q)
          | Decl_record (o, x, ta, fl) ->
              assert (o = None) ;
              Decl_record (Some c.contents, x, ta, fl) :: aux q
          | Decl_alias (o, x , ta, ty) ->
              assert (o = None) ;
              Decl_alias (Some c.contents, x , ta, ty) :: aux q
          | Decl_pterm (o, x, ta, pl, ty, s) ->
              assert (o = None) ;
              Decl_pterm (Some c.contents, x, ta, pl, ty, s) :: aux q
          | Decl_comment _ ->
              com.contents :: aux (d :: q)
          end
    (* variant types : sometimes unbind last comment  *)
    | {contents=Decl_variant (tycom, x, ta, cl) as d;_} :: q ->
        let cl_rev = List.rev cl in
        begin match cl_rev with
        | [] -> d :: aux q (* no constructor, do nothing *)
        | (ccom, c, ty) :: clq ->
            List.iter (fun (com, _, ty) -> (* check position for all
            constructor special comments *)
              begin match com with
              | None -> ()
              | Some com ->
                  if (fst com.loc).pos_lnum <> (snd ty.loc).pos_lnum then
                    begin
                      report_error_location com.loc ;
                      Format.eprintf "Syntax warning: dangling special comment@."
                    end
              end) clq ;
            begin match ccom with
            | Some ccom
                when (fst ccom.loc).pos_lnum <> (snd ty.loc).pos_lnum ->
                  (* shouldn't be attached to constructor, remove it *)
                let cl = List.rev ((None, c, ty) :: clq) in
                let d = Decl_variant (tycom, x, ta, cl) in
                let comm = {contents=Decl_comment ccom;loc=ccom.loc} in
                d :: aux (comm :: q)
            | _ -> d :: aux q
            end
        end
    (* other cases: just skip *)
    | d :: q -> d.contents :: aux q
    end
%}

(** ** Keywords **)

%token MATCH
%token WITH

%token BRANCH
%token OR
%token END

%token VAL
%token TYPE

%token IN
%token LET

%token BINDER               "binder"
%token REQUIRE              "require"

(** ** Identifiers **)
%token <string> LIDENT
%token <string> UIDENT

(** ** Numbers *)
%token <int> NUM

(** ** Operators **)
%token BAR                  "|"
%token COLON                ":"
%token COMMA                ","
%token DOT                  "."
%token CONS                 "::"
%token EQUAL                "="
%token <string> EQUALSYMBOL "=@"      (* for instance *)
%token <string> SYMBOL      "@"       (* for instance *)
%token <string> SEMISYMBOL  ";@"      (* for instance *)
%token PERCENT              "%"
%token EXISTS               "∃"
%token LAMBDA               "λ"
%token LARROW               "←"
%token RARROW               "→"
%token CDOT                 "·"
%token SEMI                 ";"
%token UNDERSCORE           "_"

(** ** Paired delimiters **)
%token LPAREN               "("
%token RPAREN               ")"
%token LANGLE               "<"
%token RANGLE               ">"

(** ** Comments **)
%token <string * Lexing.position> COMMENT

(** ** Other **)
%token EOF

%start <string with_loc list * ParsedAST.t> main

(** Precedence *)

(** lower priority is first in the list *)
%nonassoc constr (* below comment: | C _\n(** _ *) ← the comment is not bound to C *)
%nonassoc COMMENT (* above constr *)
%nonassoc pterm_lambda (* below SEMI: λ p → s1 ; s2 *)
%nonassoc pskel_letin (* below SEMI: let p = s1 in s2 ; s3 *)
%right RARROW (* below SEMI: λ x → a ; b *)
%right SEMI SEMISYMBOL
%nonassoc pskel_term
%nonassoc COLON (* above pskel_term, but with no high stake: "ret" (t : τ) *)
%nonassoc LARROW (* above pskel_term: λ p → t1 ← (f=t2) *)
%left BAR
%nonassoc pterm_constr
%nonassoc UIDENT (* above pterm_constr: A B  is a term not a function *)
%nonassoc LPAREN (* above pterm_constr: A () is a term not a function *)
%nonassoc LIDENT (* above pterm_constr: A x  is a term not a function *)
(** higher priority is last in the list *)


%% (** Rules **)



(** Auxilary rules *)

(** [included(X)] is X preceded by information on the module to use. For
    instance, [a::x] matches [included(LIDENT)] *)
included(X):
  | x=X {(x, None)}
  | p=LIDENT CONS x=X
    { (x, Some p)}

(** [length(X)] recognizes the same language as [X], and returns its length. *)
length(X):
  | x = X {List.length x}

(** matches [<X>] and returns the same as X *)
chevronned(X):
  | LANGLE x = X RANGLE { x }

(** matches empty and [<X,…,X>], and returns 'a list, if X returns 'a *)
type_args(X):
  | l = loption(chevronned(separated_nonempty_list(COMMA, X))) { l }

(** matches [X sep … sep X] and [sep X sep … sep X], and returns 'a list if
    [X] returns 'a.
    The list is supposed non empty *)
%inline preceded_or_separated_non_empty_list(sep, X):
  | ioption(sep) l = separated_nonempty_list(sep, X) { l }

(** matches [()] and [(X, …, X)] with at least two occurences, and returns 'a
    list if [X] returns 'a. *)
%inline valid_tuple(X):
  | LPAREN RPAREN
    { [] }
  | LPAREN hd = X COMMA tl = separated_nonempty_list(COMMA, X) RPAREN
    { hd :: tl}

(** either(X, Y) matches either X or Y, and returns ('a, 'b) Either.t, if X
    returns 'a and Y returns 'b. We only use it concretely with unit, but you
    never know. *)
either(X,Y):
  | x = X { Either.Left x }
  | y = Y { Either.Right y }

(** located(X) matches X, and returns 'a Util.with_loc when X returns 'a *)
located(X):
  x = X { {contents=x; loc=$loc} }





(** Main definition **)
main:
  reqs = located(require)*
  decls = located(decl)* EOF
  { reqs, bind_comments (check_for_duplicates decls)  }

%inline require:
  | REQUIRE r = LIDENT { r }

%inline comment:
  | c = COMMENT
    { {contents=fst c; loc=(snd c, $endpos)} }

decl:
(* docstring *)
| c = comment
  { Decl_comment c }
(* types *)
| type_decl { $1 }
(* terms *)
| term_decl { $1 }
(* binders *)
| BINDER s = SYMBOL EQUAL t = located(included(LIDENT))
  { Decl_binder (s, t) }

term_decl:
(* [val x<ta> args: τ] *)
| VAL x = located(LIDENT) ta = type_args(LIDENT) args = list(simple_ppattern) COLON ty = typ
  { Decl_pterm (None, x, ta, args, ty, None) }
(* [val x<ta> args: τ = t] *)
| VAL x = located(LIDENT) ta = type_args(LIDENT) args = list(simple_ppattern) COLON ty = typ
  EQUAL s = pskeleton
  { Decl_pterm (None, x, ta, args, ty, Some s) }

type_decl:
(* non-specified types *)
| TYPE l = located(LIDENT) i = length(type_args(UNDERSCORE))
  { Decl_nonspec (None, l, i) }
(* variant types *)
| TYPE l = located(LIDENT) ta = type_args(LIDENT) EQUAL
  cl = preceded_or_separated_non_empty_list(BAR, constructor_def)
  { Decl_variant (None, l, ta, cl) }
(* record types *)
| TYPE l = located(LIDENT) ta = type_args(LIDENT) EQUAL
  LPAREN
    fl = separated_nonempty_list(COMMA, field_def)
  RPAREN
  { Decl_record (None, l, ta, fl) }
(* alias types *)
| TYPE l = located(LIDENT) ta = type_args(LIDENT) EQUAL ty = typ
  { Decl_alias (None, l, ta, ty) }


(** Definition of constructor for a variant type, without the | *)
constructor_def:
  | c = located(UIDENT) ty = option(typ) com=ioption(comment)
  {(com, c, Option.value ~default:(ptyp_unit (snd c.loc)) ty)} %prec constr

(** Definition of field for a record type *)
%inline field_def:
  | f = located(LIDENT) COLON ty = typ com=ioption(comment)
  {(com, f, ty)}

typ: x = located(typ_desc) { x }
typ_desc:
  | ty = simple_typ_desc                      { ty }
  | ty_in = typ RARROW ty_out = typ           { Ptyp_arrow (ty_in, ty_out) }

simple_typ: x = located(simple_typ_desc) { x }
simple_typ_desc:
  | LPAREN ty = typ_desc RPAREN               { ty }
  | ty = located(included(LIDENT))
    ta = type_args(typ)                       { Ptyp_constructor (ty, ta) }
  | tyl = valid_tuple(typ)                    { Ptyp_product tyl }

%inline record(X):
  | LPAREN
    rfl = separated_nonempty_list(COMMA,
      endrule(f = located(LIDENT) EQUAL x = X { (f, x) })
    ) RPAREN
    { rfl }

%inline typed(X):
  | LPAREN x = X COLON ty = typ RPAREN { (x, ty) }

variable:
  v = located(included(LIDENT))  { v }

(** A simple_pterm is a pterm that may appear anywhere without any priority
    restriction, as opposed to pterm which may not. For instance, [C x] is not a
    simple_pterm, but [C] is. *)
simple_pterm: x = located(simple_pterm_desc) { x }
simple_pterm_desc:
  | LPAREN t = pterm_desc RPAREN
    { t }
  | x = variable ta = type_args(typ)
    { Pterm_variable (x, ta) }
  | c = located(included(UIDENT)) ta = type_args(typ)
    { Pterm_constructor (c, ta, pterm_top) } %prec pterm_constr
  | tl = valid_tuple(pterm)
    { Pterm_tuple tl }
  | t = simple_pterm DOT f = located(LIDENT)
    { Pterm_getfield (t, f) }
  | t = simple_pterm DOT n = NUM
    { Pterm_getnth (t, n) }
  | tl = included(record(pterm))
    { Pterm_recmake tl }
  | x = typed(pterm)
    { let (t, ty) = x in Pterm_typed (t, ty) }

%inline semiop:
  | s = SEMISYMBOL             { Symbol s }
  | SEMI PERCENT x = variable  { Percent x }
%inline eqop:
  | s = EQUALSYMBOL            { Symbol s }
  | EQUAL PERCENT x = variable { Percent x }

pterm: x = located(pterm_desc) { x }
pterm_desc:
  | t = simple_pterm_desc { t }
  | c = located(included(UIDENT)) ta = type_args(typ) t = simple_pterm
    { Pterm_constructor (c, ta, t) }
  | LAMBDA p = ppattern either(RARROW, CDOT) s = pskeleton
    { Pterm_function (p, s) } %prec pterm_lambda
  | LAMBDA p = ppattern COLON ty = simple_typ either(RARROW, CDOT) s = pskeleton
    { let pat:ppattern =
        {contents = Ppat_typed (p, ty); loc=(fst p.loc, snd ty.loc)} in
      Pterm_function (pat, s) } %prec pterm_lambda
  | t = pterm LARROW tl = record(pterm)
  { Pterm_recset (t, tl) }

simple_ppattern: x = located(simple_ppattern_desc) { x }
simple_ppattern_desc:
  | LPAREN p = ppattern_desc RPAREN            { p }
  | LPAREN p = ppattern COLON ty = typ RPAREN  { Ppat_typed (p, ty) }
  | UNDERSCORE                                 { Ppat_any }
  | x = LIDENT                                 { Ppat_variable x }
  | pl = valid_tuple(ppattern)                 { Ppat_tuple pl }
  | pl = record(ppattern)                      { Ppat_record pl }
  | c = located(UIDENT) { Ppat_constructor (c.contents, ppat_top (snd c.loc)) }

ppattern: x = located(ppattern_desc) { x }
ppattern_desc:
  | p = simple_ppattern_desc           { p }
  | p1 = ppattern BAR p2 = ppattern    { Ppat_or (p1, p2) }
  | c = UIDENT p = simple_ppattern     { Ppat_constructor (c, p) }

pskeleton: x = located(pskeleton_desc) { x }
pskeleton_desc:
  | t = pterm
    { Pskel_return t } %prec pskel_term
  | BRANCH pl = separated_list(OR, pskeleton) END
    { Pskel_branching pl }
  | MATCH s = pskeleton WITH l = pattern_matching END
    { Pskel_match (s, l) }
  | LET p = ppattern EQUAL s1 = pskeleton IN s2 = pskeleton
    { Pskel_letin (Some p, s1, s2) } %prec pskel_letin
  | LET f = located(LIDENT) pl = simple_ppattern+ EQUAL s1 = pskeleton IN s2 = pskeleton
    { Pskel_letin (
      Some {contents=Ppat_variable f.contents;loc=f.loc},
      mkfunc pl s1, s2) } %prec pskel_letin
  | LET p = ppattern s = located(eqop) s1 = pskeleton IN s2 = pskeleton
    { Pskel_letop (s, Some p, s1, s2) } %prec pskel_letin
  | s1 = pskeleton SEMI s2 = pskeleton
    { Pskel_letin (None, s1, s2) } %prec pskel_letin
  | s1 = pskeleton s = located(semiop) s2 = pskeleton
    { Pskel_letop (s, None, s1, s2) } %prec pskel_letin
  | x = typed(pskeleton)
    { let (s, ty) = x in Pskel_typed (s, ty) }
  | f = simple_pterm x = simple_pterm
    { Pskel_apply (f, x) }
  | EXISTS ty = typ
    { Pskel_exists ty }



%inline pattern_matching:
  pl = preceded_or_separated_non_empty_list(BAR, match_case) { pl }

%inline match_case:
  p = ppattern RARROW s = pskeleton { (p, s) }
