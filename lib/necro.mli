(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

(** Main module for necro. This module provides the library of necro. *)

module Errors = Errors
(** Errors and warning, with functions to raise them properly *)

module Pair = Pair
(** Some useful functions on pairs, should really be part of OCaml's stdlib *)

module Util = Util
(** Some useful functions and modules. *)

module ParsedAST = ParsedAST
(** Types for the parsed AST *)

module Types = Types
(** Some useful functions on types *)

module TypedAST = TypedAST
(** Definition of the typed AST. *)

module Interpretation = Interpretation
(** The ('termout, 'skelout) interpretation type. *)

module Skeleton = Skeleton
(** Some useful functions to handle skeletons and terms. *)

module Printer = Printer
(** Some functions to print parts of a skeletal_semantics. *)

val parse_and_type: ?warn:(Errors.warn_list) ->
	string list -> string -> TypedAST.skeletal_semantics
	(** [parse_and_type ~warn deps f] parses and types the file [f]. [deps] are
			the path to the dependencies of [f]. [warn] states which warning should be
			silent or not. *)

val parse_and_type_list: ?warn:(Errors.warn_list) ->
	string list  -> TypedAST.skeletal_semantics list
	(** [parse_and_type_list ~warn fl] parses and types the files [fl].
			Each file is a dependency to the rest of the list. [warn] states which
			warning should be silent or not. *)

val find_deps : string -> string list
(** Given a file [f], [find_deps f] browses [f] to find all dependencies, and
		looks for them in cwd and in [f]'s directory *)

val keywords: Util.SSet.t
(** Set of the lexer's keywords. *)
