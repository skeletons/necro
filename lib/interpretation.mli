(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

(** This module defines interpretations, a generic way to extract terms out of
    skeletons and terms. *)

open TypedAST

(** The type of interpretations: an interpretation is a collection of functions
    that produce outputs by induction over the structure of skeletons.
    This presentation enables to compose interpretations.
    This type is parameterised by skeletal and term outputs (['s] and ['t]). *)
type ('t, 's) interpretation = {
	var_interp : variable -> 't ;
	constr_interp : constructor -> 't -> 't ;
	tuple_interp : 't list -> 't ;
	function_interp : pattern -> 's -> 't ;
	field_interp : 't -> field -> 't ;
	nth_interp : 't -> typ list -> int -> 't ;
	record_interp : (constructor * 't) list -> 't ;
	recset_interp : 't -> (field * 't) list -> 't ;
	merge_interp : typ -> 's list -> 's ;
	match_interp : 's -> (pattern * 's) list -> 's ;
	letin_interp : bind -> pattern -> 's -> 's -> 's ;
	exists_interp : typ -> 's ;
	return_interp : 't -> 's ;
	apply_interp : 't -> 't -> 's ;
	}



val interpret_term : ('termout, 'skelout) interpretation -> term -> 'termout
(** Given an interpretation, this function computes the value of a term. *)

val interpret_skeleton : ('termout, 'skelout) interpretation -> skeleton -> 'skelout
(** Given an interpretation, this function computes the value of a skeleton. *)


val constant_interpretation: 'term -> 'skel -> ('term, 'skel) interpretation
(** The constant interpretation: every term is evaluated to the same value and
    every skeleton is evaluated to the same value. *)
