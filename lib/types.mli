(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

open TypedAST

val subst_type : string -> typ -> typ -> typ
(** For [s] and [s'] two types, and [x] a variable,
    [subst_type x s s'] = [s'{x<-s}]. *)

val subst_mult : string list -> typ list -> typ -> typ
(** Same as {!subst_type}, but performs several substitutions simultaneously. *)


