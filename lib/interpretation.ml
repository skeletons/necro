(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

open TypedAST

type ('t, 's) interpretation = {
	var_interp : variable -> 't ;
	constr_interp : constructor -> 't -> 't ;
	tuple_interp : 't list -> 't ;
	function_interp : pattern -> 's -> 't ;
	field_interp : 't -> field -> 't ;
	nth_interp : 't -> typ list -> int -> 't ;
	record_interp : (constructor * 't) list -> 't ;
	recset_interp : 't -> (field * 't) list -> 't ;
	merge_interp : typ -> 's list -> 's ;
	match_interp : 's -> (pattern * 's) list -> 's ;
	letin_interp : bind -> pattern -> 's -> 's -> 's ;
	exists_interp : typ -> 's ;
	return_interp : 't -> 's ;
	apply_interp : 't -> 't -> 's ;
	}

let rec interpret_skeleton interp (sk:skeleton) =
	begin match sk.sdesc with
	| Branching (ty, branches) ->
		interp.merge_interp ty (List.map (interpret_skeleton interp) branches)
	| Match (t, psl) ->
		let do_one (p, s) =
			(p, interpret_skeleton interp s)
		in
		interp.match_interp
			(interpret_skeleton interp t) (List.map do_one psl)
	| LetIn (a, p, b, s) ->
		interp.letin_interp a p ( (interpret_skeleton interp) b)
		(interpret_skeleton interp s)
	| Exists ty -> interp.exists_interp ty
	| Return t -> interp.return_interp (interpret_term interp t)
	| Apply (t, arg) ->
			let t_out = (interpret_term interp) t in
			let arg_out = (interpret_term interp) arg in
			interp.apply_interp t_out arg_out
	end

and interpret_term interp (t:term) =
	begin match t.tdesc with
	| TVar tv -> interp.var_interp tv
	| TConstr (c, t') ->
			interp.constr_interp c (interpret_term interp t')
	| TTuple tl ->
			interp.tuple_interp (List.map (interpret_term interp) tl)
	| TFunc (p, s) ->
			interp.function_interp p (interpret_skeleton interp s)
	| TField (t, f) ->
			interp.field_interp (interpret_term interp t) f
	| TNth (t, tyl, n) ->
			interp.nth_interp (interpret_term interp t) tyl n
	| TRecMake l ->
			let one (f, t) =
				(f, interpret_term interp t)
			in
			interp.record_interp (List.map one l)
	| TRecSet (o, l) ->
			let one (f, t) =
				(f, interpret_term interp t)
			in
			let o' = interpret_term interp o in
			interp.recset_interp o' (List.map one l)
	end


let constant_interpretation t s = {
	var_interp = (fun _ -> t) ;
	constr_interp = (fun _ _ -> t) ;
	tuple_interp = (fun _ -> t) ;
	function_interp = (fun _ _ -> t) ;
	field_interp = (fun _ _ -> t) ;
	nth_interp = (fun _ _ _ -> t) ;
	record_interp = (fun _ -> t) ;
	recset_interp = (fun _ _ -> t) ;
	letin_interp = (fun _ _ _ _ -> s) ;
	exists_interp = (fun _ -> s) ;
	apply_interp = (fun _ _ -> s) ;
	merge_interp = (fun _ _ -> s) ;
	match_interp = (fun _ _ -> s) ;
	return_interp = (fun _ -> s)
}
