(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

(** This module provides functions to print the elements defined in the
    {!TypedAST} module. *)

val string_of_type : TypedAST.typ -> string
(** Print a type *)

val string_of_term : ?protect:bool -> indent:int ->
    TypedAST.term -> string
(** Print a term.
    - The output contains line breaks.
    - The argument [indent] indicates the current indentation level.
    - The argument [protect] indicates whether the term needs to be protected
      with parenthesis
    - The output string tries to minimise the amount of parentheses. *)

val string_of_pattern : TypedAST.pattern -> string
(** Print a pattern.
    - The output string has no line breaks.
    - The output string tries to minimise the amount of parentheses. *)

val string_of_skeleton : ?protect:bool -> indent:int ->
    TypedAST.skeleton -> string
(** Print a skeleton.
    - The output contains line breaks.
    - The argument [indent] indicates the current indentation level.
    - The argument [protect] indicates whether the skeleton needs to be protected
      with parenthesis
    - The output string tries to minimise the amount of parentheses. *)

val string_of_ss : TypedAST.t -> string
(** Prints a whole skeletal semantics.
    - The output obviously contains line breaks.
    - The resulting string can be parsed to an equivalent skeletal semantics.
    - The syntactic sugar are used as much as possible. *)

val add_parentheses : string -> string
(** Add parentheses around the string if needed. *)

