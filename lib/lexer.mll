(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

{
	open Parser
	open Lexing

	exception Lexing_error of string

	let kw = [
		"match", MATCH;
		"with", WITH;

		"branch", BRANCH;
		"or", OR;
		"end", END;

		"let", LET;
		"in", IN;

		"val", VAL;
		"type", TYPE;

		"binder", BINDER;
    "require", REQUIRE;
	]

	let keywords = Hashtbl.create (List.length kw)
	let () = List.iter (fun (a, b) -> Hashtbl.add keywords a b) kw

	let newline lexbuf =
		let pos = lexbuf.lex_curr_p in
		lexbuf.lex_curr_p <- { pos with pos_lnum = pos.pos_lnum + 1 ; pos_bol = pos.pos_cnum }
}

let alpha = ['a'-'z'] | ['A'-'Z']
let ident_car = alpha | '_' | '\'' | ['0'-'9']
let lident = ( ['a'-'z'] ident_car* ) | ( '_' ident_car+ )
let uident = ['A'-'Z'] ident_car*
let whitespace = [' ' '\t']
let newline = "\n" | "\r\n" | "\r"
let number =['0'-'9']+
let special_char =
	"!" | "&" | "*" | "⊤" | "⊥" | "@" | "$" | "^" | "∀" | "+" | "-" | "/" | "~"
let symbol = special_char (ident_car | special_char)*
let eqsymb = '=' symbol
let scsymb = ';' symbol

rule token = parse
	| whitespace+ { token lexbuf }
	| "(**"       { comment "" (Lexing.lexeme_start_p lexbuf) lexbuf }
	| "(*"        { inner_comment lexbuf; token lexbuf }
	| newline     { newline lexbuf; token lexbuf }
	(* Operators *)
	| "|"         { BAR }
	| ":"         { COLON }
	| ","         { COMMA }
	| "."         { DOT }
	| "::"        { CONS }
	| "="         { EQUAL }
	| eqsymb as s { EQUALSYMBOL (String.sub s 1 (String.length s - 1)) }
	| symbol as s { SYMBOL s }
	| "%"         { PERCENT }
	| "?" | "∃"   { EXISTS }
	| "->" | "→"  { RARROW }
	| "·"         { CDOT }
	| "<-" | "←"  { LARROW }
	| "\\" | "λ"  { LAMBDA }
	| ";"         { SEMI }
	| scsymb as s { SEMISYMBOL (String.sub s 1 (String.length s - 1)) }
	(* Paired delimiters *)
	| "("         { LPAREN }
	| ")"         { RPAREN }
	| "<"         { LANGLE }
	| ">"         { RANGLE }
	| "_"         { UNDERSCORE }
	| lident as s { try Hashtbl.find keywords s with Not_found -> LIDENT s }
	| uident as s { try Hashtbl.find keywords s with Not_found -> UIDENT s }
	| number as n { try Hashtbl.find keywords n with Not_found ->
			NUM (int_of_string n) }
	| _           { raise (Lexing_error "Invalid character") }
	| eof         { EOF }

and comment s start = parse
	| "\n"   { newline lexbuf; comment (s ^ "\n") start lexbuf }
	| "*)"   { COMMENT (s, start) }
	| "(*"   { inner_comment lexbuf; comment s start lexbuf }
	| _ as s' { comment (s ^ (String.make 1 s')) start lexbuf }
	| eof  { raise (Lexing_error "Unterminated comment") }

and inner_comment = parse
	| "\n" { newline lexbuf; inner_comment lexbuf }
	| "*)" { () }
	| "(*" { inner_comment lexbuf; inner_comment lexbuf }
	| _    { inner_comment lexbuf }
	| eof  { raise (Lexing_error "Unterminated comment") }
