(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

(** The 4 kind of types *)
type type_kind = Unspec | Variant | Record | Alias

(** The 2 kind of terms *)
type term_kind = Nonspecified | Specified

type 'a with_path = 'a ParsedAST.with_path


(** The types in skel. *)
type typ =
  { tydesc: typ_desc
  ; tyloc: Util.location}

and typ_desc =
	| Variable of string
	(** A type variable *)
	| UserType of string with_path * typ list
	(** [UserType (n, ta)] is the type named [n], with type arguments [ta]. *)
	| Arrow of typ * typ
	(** [Arrow (i, o)] is the type [i → o] *)
	| Product of typ list
	(** [Product [t1,…,tn]] is the product type [(t1, …, tn)].

			{b Invariant:} [n≠1] *)

type constructor_signature =
	{ cs_name : string (* the name of the constructor *)
	; cs_type_args : string list (* the type arguments *)
	; cs_input_type : typ (* the input type for the constructor *)
	; cs_variant_type : string (* the variant type produced by the constructor *)
	; cs_comment : string option (* the special comment about the constructor *)
	}

type field_signature =
	{ fs_name : string (* the name of the field *)
	; fs_type_args : string list (* the type arguments *)
	; fs_field_type : typ (* the type of the field *)
	; fs_record_type : string (* the record type to which the field belongs *)
	; fs_comment: string option (* the special comment about the field *)
	}

(** A constructor is described by the constructor name, and the type it
		generates (its name and type arguments) *)
type constructor = string with_path * (string * typ list)

(** A field is described by the field name, and the record type it belongs to
		(its name and type arguments) *)
type field = string with_path * (string * typ list)

type bind =
	| NoBind
	(** No binder, just a regular [let-in] *)
	| TermBind of term_kind * string with_path * typ list * typ
	(** a binder such as [=%bind] *)
	| SymbolBind of string * term_kind * string with_path * typ list * typ
	(** a binder such as [=@] *)


(** The type of patterns, used in [let … in] constructs, functions and pattern
		matching. *)
type pattern = {
	pdesc: pattern_desc ;
	ploc : Util.location ;
	ptyp : typ }

and pattern_desc =
	| PWild
	(** The catch-all pattern [_] *)
	| PVar of string
	(** A variable pattern along with its type *)
	| PConstr of constructor * pattern
	(** [PConstr (C, p)] is the pattern [C p]. *)
	| PTuple of pattern list
	(** [PTuple [p1; …; pn]] is the pattern [(p1, …, pn)].

	    {b Invariant:} [n≠1] *)
	| PRecord of (field * pattern) list
	(** [PRecord [(f1,p1); …; (fn,pn)]] is the record pattern [(f1=p1, …, fn=pn)].

			{b Invariant:} [n≠0]. *)
	| POr of pattern * pattern
	(** [POr (p1, p2)] is the disjunction of pattern [p1] and pattern [p2]*)
	| PType of pattern * typ
	(** [PType (p, ty)] is the pattern [p], along with the information that it has
	    type [ty] *)


(** A variable, together with its type information. *)
type variable =
	| LetBound of string * typ
	(** [LetBound (x, ty)] denotes the variable [x] of type [ty], which was created
	    by a pattern, either in a [let]-binding, a function, or a
	    pattern-matching. *)
	| TopLevel of term_kind * string with_path * typ list * typ
	(** [TopLevel (k, x, ta, ty)] is the term [x], created at top-level, specified
	    or not according to [k], with type arguments [ta], and with type [ty]. *)


and term = {
	tdesc: term_desc ;
	tloc : Util.location ;
	ttyp : typ }

and term_desc =
	| TVar of variable
	(** A variable *)
	| TConstr of constructor * term
	(** [TConstr (C, t)] is the constructor [C] applied to the term [t] *)
	| TTuple of term list
	(** [TTuple [t1, …, tn]] is the term [(t1, …, tn)].

	    {b Invariant:} [n≠1]. *)
	| TFunc of pattern * skeleton
	(** [TFunc (p, s)] is the term [λp.s] *)
	| TField of term * field
	(** [TField (t, f)] is the field [f] of the term [t]. *)
	| TNth of term * typ list * int
	(** [TNth (t, tl, i)] is the i-th component of the tuple term [t], of type
	    [Product tl]. *)
	| TRecMake of (field * term) list
	(** [TRecMake [(f1,t1); …; (fn, tn)]] is the record term [(f1=t1, …, fn=tn)]

	    {b Invariant:} [n>0]*)
	| TRecSet of term * (field * term) list
	(** [TRecSet (t, [(f1,t1); …; (fn, tn)])] is the term [t], with field
	    [f1, …, fn] reassigned respectively to the values of [t1, …, tn].

	    {b Invariant:} [n>0]*)

and skeleton = {
	sdesc: skeleton_desc ;
	sloc : Util.location ;
	styp : typ }

and skeleton_desc =
	| Branching of typ * skeleton list
	(** [Branching (ty, sl)] is a branching with branches [sl], each branch being
			of type [ty]. *)
	| Match of skeleton * (pattern * skeleton) list
	(** [Match (s, psl)] is a pattern-matching on the value of [s], with branches
	    described by [psl].

	    {b Invariant:} [psl≠[]]*)
	| LetIn of bind * pattern * skeleton * skeleton
	(** [LetIn (b, p, s1, s2)] is the skeleton [let p =b s1 in s2], where [=b] is
	 [=] along with the bind annotation [b]. *)
	| Exists of typ
	(** [Exists ty] is the existential [∃ty]. *)
	| Return of term
	(** The skeleton [t] *)
	| Apply of term * term
	(** The application of terms *)


type type_definition =
	| TDUnspec of int (* number of type arguments *)
	| TDVariant of string list * constructor_signature list
	| TDRecord of string list * field_signature list
	| TDAlias of string list * typ


(** A term is described by
		- Its type arguments
		- Its type
		- (optionnally) its definition *)
type term_definition =
	string list * typ * term option

(** The type of skeletal semantics *)
type skeletal_semantics = {
	ss_order : ParsedAST.decl_kind list ;
	(** The order in which the declarations where given *)
	ss_types : (string option * type_definition) Util.SMap.t ;
	(** Each type name is mapped to its definition). *)
	ss_terms : (string option * term_definition) Util.SMap.t ;
	(** Each term name is mapped to its definition. *)
	ss_binders : (term_kind * string with_path) Util.SMap.t ;
	(** The skeletal semantics of all included files. *)
  ss_includes: skeletal_semantics Util.SMap.t
}

type t = skeletal_semantics
