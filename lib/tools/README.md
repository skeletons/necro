# Tools for the necromancer

## Vim: `skel.vim`
You will find in this folder a `skel.vim` file, to get syntactic coloring for
the `skel` files in the vim editor. To use it, put it in your `syntax` folder,
usually `.vim/syntax/` or `.config/vim/syntax/`

Then, add the following line to your `vimrc`~:
``` vim
autocmd bufNewfile,bufRead \(*.skel\|*.sk\) set filetype=skel
```

Another way to use this file is to install [this plugin](https://github.com/lupjo/necro-vim)

## Pygments: `sk.py`
You can also find in this folder a `sk.py` file. It contains a lexer for use
with `pygments`. For example, in the command line, you can type~:
``` bash
pygmentize -l sk.py -x myfile.sk
```
In a TeX document, you can use `minted` to get syntactic coloring this way
```Tex
\usepackage{minted} % In preamble
…
\begin{minted}{sk.py -x}
(* Some sk code *)
\end{minted}
```
if you use `pdflatex`, you will need to include a `-shell-escape` option in
order for minted to work.
NOTE: as of minted v2.7, this doesn't work. Some possible workarounds are described
[here](https://github.com/gpoore/minted/issues/360).

## Emacs: `sk.el`
You can even activate a syntax highlighting in emacs. For doing that you will
have just to include `sk.el` in your `.emacs` configuration file.
For doing that, just to add 
```
(load "~/*your path to the file*/sk.el")
```
This mode is derived from tuareg mode, so you can exploit auto-completion.

**Requirements: tuareg**

If you don't have it, just install it from opam:
```
opam install tuareg
```
Customizations, like prettifying symbols etc.. are automatically inherited in
the necro mode. For more informations see [tuareg](https://github.com/ocaml/tuareg/blob/master/README.md)

