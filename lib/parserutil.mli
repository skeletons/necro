(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

(** Useful functions to help lexing and parsing *)

val getlines: in_channel -> string

(** Given an error start and an error end, prints an error message. *)
val report_error_location : Lexing.lexbuf -> Lexing.position -> Lexing.position -> unit

val parse_from_file :
  ((Lexing.lexbuf -> Parser.token) -> Lexing.lexbuf -> 'a) -> string -> 'a

