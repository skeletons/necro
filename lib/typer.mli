(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

(** This function transforms an untyped AST, into a typed AST, ready for
		consumption. *)
val type_program : ?warn:(Errors.warn_list) ->
	TypedAST.t Util.SMap.t -> (* typed dependencies *)
	string Util.with_loc list * ParsedAST.t -> (* untyped AST *)
	TypedAST.t

