(**************************************************************************)
(*                                                                        *)
(*                             Necro Library                              *)
(*                                                                        *)
(*                 Victoire Noizet, projet Épicure, Inria                 *)
(*                                                                        *)
(*   Copyright 2022 INRIA.                                                *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU General Public License version 3 as described in the file    *)
(*   LICENSE.                                                             *)
(*                                                                        *)
(**************************************************************************)

(** The main type is the type [t], which describes the output of the parser. The
    other types are defined in order to define type [t], and are commented
    individually. *)

open Util


(** used for type arguments in polymorphic types and terms *)
type type_arg = string

(** used for type names. Matches [lexer.mll]'s lident regex. *)
type type_name = string

(** used for constructor names. Matches [lexer.mll]'s lident regex. *)
type constructor_name = string

(** used for field names. Matches [lexer.mll]'s lident regex. *)
type field_name = string

(** used for variable names. Matches [lexer.mll]'s lident regex. *)
type var_name = string

(** used for pterm names. Matches [lexer.mll]'s lident regex. *)
type term_name = string

(** used for binder symbols. Matches [lexer.mll]'s symbol regex *)
type symbol = string

(** Types of declaration:
		There are type and term, special comments, and binders. *)
type decl_kind =
| TypeDecl of type_name
| TermDecl of term_name
| BinderDecl of symbol
| Comment of string with_loc




(** used to store the path of an item.
    Either [(x, None)] meaning item [x] is given with no path, or [(x, Some p)]
    meaning x was given with path [p]. In Skel, this is [p::x]. *)
type 'a with_path = 'a * string option

(** Main type for the parsed AST. A parsed AST is a list of declarations. *)
type t = declaration list

and declaration =
  | Decl_nonspec of nonspec_decl
  | Decl_variant of variant_decl
  | Decl_record of record_decl
  | Decl_alias of alias_decl
  | Decl_pterm of pterm_decl
  | Decl_binder of binder_decl
  | Decl_comment of string with_loc



(** First  component : special comment
    Second component : name of the type
    Third  component : number of type arguments *)
and nonspec_decl = string option * type_name with_loc * int


(** First  component : special comment
    Second component : name of the type
    Third  component : type arguments
    Fourth component : list of constructors *)
and variant_decl = string option * type_name with_loc * type_arg list * constructor_def list

(** First  component : special comment
    Second component : name of the type
    Third  component : type arguments
    Fourth component : list of fields *)
and record_decl = string option * type_name with_loc * type_arg list * field_def list


(** First  component : special comment
    Second component : name of the type
    Third  component : type arguments
    Fourth component : aliased type *)
and alias_decl = string option * type_name with_loc * type_arg list * ptyp


(** First  component : special comment
    Second component : name of the constructor
    Third  component : input type of the constructor *)
and constructor_def = string with_loc option * constructor_name with_loc * ptyp


(** First  component : Special comment
    Second component : name of the term
    Third  component : type arguments
    Fourth component : arguments (will be unsugared later)
    Fifth  component : return type
    Sixth  component : optional skeleton specification *)
and pterm_decl = string option * term_name with_loc * type_arg list * ppattern list * ptyp * pskeleton option


(** First  component : symbol
    Second component : term that it refers to *)
and binder_decl = symbol * term_name with_path with_loc


(** First  component : special comment
    Second component : name of the field
    Third  component : type of the field *)
and field_def = string with_loc option * field_name with_loc * ptyp


(** The type for type annotations *)
and ptyp = ptyp_desc with_loc
and ptyp_desc =
| Ptyp_constructor of type_name with_path with_loc * ptyp list
(** Type constructor. [Ptyp_constructor (t, ta)] is
    - [t           ] if [ta = []]
    - [t<t1, …, tn>] if [ta = [t1, …, tn]] *)
| Ptyp_product of ptyp list
(** [Ptyp_product [t1; …; tn]] represents the product type [(t1, …, tn)].

    {b Invariant:} [n ≠ 1]. *)
| Ptyp_arrow of ptyp * ptyp
(** [Ptyp_arrow (t1, t2)] represents the type [t1 → t2]. *)


(** The type for binder operators. *)
and operator =
  | Symbol of symbol
  | Percent of term_name with_path with_loc

(** The type for terms *)
and pterm = pterm_desc with_loc
and pterm_desc =
| Pterm_variable of term_name with_path with_loc * ptyp list
(** [Pterm_variable (x, l)] is
    - [x           ] if [l = []]
    - [x<τ1, …, τn>] if [l = [τ1; …; τn]] *)
| Pterm_constructor of constructor_name with_path with_loc * ptyp list * pterm
(** [Pterm_constructor (C, l, t)] is
    - [C t           ] if [l = []]
    - [C<τ1, …, τn> t] if [l = [τ1; …; τn]] *)
| Pterm_tuple of pterm list
(** [Pterm_tuple [t1; …; tn]] represents the tuple [(t1, …, tn)].

    {b Invariant:} [n ≠ 1]. *)
| Pterm_function of ppattern * pskeleton
(** [Pterm_function (p, sk)] represents the function [(λ p · sk)]. *)
| Pterm_getfield of pterm * field_name with_loc
(** [Pterm_getfield (t, f)] represents the access to field [f] in pterm [t], that
    is [t.f]. *)
| Pterm_getnth of pterm * int
(** [Pterm_getnth (t, n)] represents the access to [n]-th component in pterm [t], that
    is [t.n]. *)
| Pterm_recmake of field_set list with_path
(** [Pterm_recmake (m :: l)] makes the record with fields [l], whose type is
    supposed to be defined in file [m]. *)
| Pterm_recset of pterm * field_set list
(** [Pterm_recset (t, l)] update the record with fields [l].
    [Pterm_recset (t, [(f1, t1); …; (fn, tn)])] is [t ← (f1=t1, …, fn=tn)].

    {b Invariant:} [l ≠ []]. *)
| Pterm_typed of pterm * ptyp
(** [Pterm_typed (t, τ)] is [(t: τ)]. *)


(** First  component : the field you are binding
    Second component : the pterm you are binding it to *)
and field_set = field_name with_loc * pterm


(** The type for patterns *)
and ppattern = ppattern_desc with_loc
and ppattern_desc =
| Ppat_any
(** the ppattern [_] *)
| Ppat_variable of var_name
(** A variable ppattern such as [x] *)
| Ppat_tuple of ppattern list
(** [Ppat_tuple [p1;…;pn]] represents the ppattern tuple [(p1, …, pn)].

    {b Invariant}: [n ≠ 1] *)
| Ppat_record of field_pat list
(** A record ppattern such as [(f1=p1, f2=p2)] *)
| Ppat_constructor of constructor_name * ppattern
(** [Ppat_constructor (C, p)] is the pattern [C p] *)
| Ppat_or of ppattern * ppattern
(** [Ppat_or (p1, p2)] is [p1 | p2]. *)
| Ppat_typed of ppattern * ptyp
(** [Ppat_typed (p, τ)] is [(p: τ)]. *)


(** First  component : the field you are matching
    Second component : the ppattern you are matching it with *)
and field_pat = field_name with_loc * ppattern


(** The type for skeletons *)
and pskeleton = pskeleton_desc with_loc
and pskeleton_desc =
| Pskel_return of pterm
(** [Pskel_return t] is [t], taken as a skeleton *)
| Pskel_branching of pskeleton list
(** [Pskel_branching [S1;…;Sn]] is [branch S1 … or … Sn end] *)
| Pskel_match of pskeleton * (ppattern * pskeleton) list
(** [Pskel_match (s, [(p1,S1);…;(pn,Sn)])] is
    [match s with | p1 → S1 | … → … | pn → Sn end].

    {b Invariant:} [n ≥ 1]. *)
| Pskel_letin of ppattern option * pskeleton * pskeleton
(** [Pskel_letin (Some p, S1, S2)] is [let p = S1 in S2]

    [Pskel_letin (None, S1, S2)] is [S1 ; S2] *)
| Pskel_letop of operator with_loc * ppattern option * pskeleton * pskeleton
(** [Pskel_letop (op, Some p, S1, S2)] is [let p =op S1 in S2]

    [Pskel_letop (op, None, S1, S2)] is [S1 ;op S2] *)
| Pskel_exists of ptyp
(** [Pskel_exists τ] is [∃τ] *)
| Pskel_apply of pterm * pterm
(** [Pskel_apply (t1, t2)] is [t1 t2] *)
| Pskel_typed of pskeleton * ptyp
(** [Pskel_typed (S, τ)] is [(S : τ)] *)
