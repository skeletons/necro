# Current

# 0.16.2
- Add mandatory `require` keyword
- Add `ss_includes` field to `skeletal_semantics`

# 0.16.1
- Enrich `Skeleton` module

# 0.16.0.1
- Fix

# 0.16
- remove modules. Keep the possibility to split a skeletal semantics into
  several files

# 0.15.1
- Fix error in `necroprint`, which made it generate untypable code. Added
  relevant typing informations.

# 0.15
- Change `pattern` type to add location

# 0.14.9
- Add types dependency graph (see `Skeleton.type_dep_graph`).
- rename `dependency_graph` to `term_dep_graph`. Old name is now deprecated

# 0.14.8
- Force type `()` for skeleton right of a `;`
- Fix syntax error printing. Sometimes printed `Invalid argument ("String.sub / Bytes.sub")`

# 0.14.7.1
- add `protect_ss`
- Fix issue #33

# 0.14.7
- add function `dependency_graph_mult` : computes dependency graph for multiple files
- Check that terms in modules do not depend on clones of said module
- Fix order of arguments for some functions in Skeleton (BREAKING)
- Remove `Graph` module, use OCamlGraph instead

# 0.14.6
- remove `string_of_rel_path`, add `Path.Rel.to_string` and `Path.Abs.to_string`
- Add `Graph` module (should be in Stdlib IMO)
- Add function `dependency_graph` to Necro Dep, and add `necrodep` application
  to analyze dependencies between terms

# 0.14.5
- Warnings can be chosen individually now (e.g `necroparse -w 1,3,4 file.sk`)
- Pretty print type errors
- Fix issue #28
- Expose `parse_and_type_list`
- Fix missing order in sub-modules
- Fix cloning
- Fix printer (erroneous := in type aliases and substitutions)

# 0.14.4
- Remove existentials of type `let x : τ in _`, replace them by `∃τ`
  **BREAKING CHANGE**: `?` and `∃` cannot be used as binders anymore

# 0.14.3
- Fixed module system
- Fix printer (issue #31)
- Fix issue #30
- Fix error when opening when opening current module
- Document module system in [README.md](README.md) (in a few words)

# 0.14.2
- Add `get_type_kind` to `Skeleton`

# 0.14.1
- Remove `This`, use `File (self, _)` instead.
  This way, when we take an information from another file, there is no need to
  fix the path.

# 0.14.0
- Now using reasonable module system

# 0.13.0-alpha.7
- Improve error printing (credits to Martin Andrieux) : print the line
  containing the syntax error

# 0.13.0-alpha.6
- Add exhaustivity information
- Rename `Typing_env` to `TypingEnv`
- Rename `Parsed_ast` to `ParsedAST`
- Remove PlainAST
- Add type at the level between `term_desc` and `term`
- Add type at the level between `skeleton_desc` and `skeleton`
- Add functions to `Skeleton` to navigate skeletal semantics
- Fix function `useful` (error with typed patterns)

# 0.13.0-alpha.5
- Keep track of type of t in t.3
- Edit type of constructor and field

# 0.13.0-alpha.4
- Add unalias in skeleton

# 0.13.0-alpha.3
- Add Path in necro

# 0.13.0-alpha.2
- Update interpretation

# 0.13.0-alpha.1
- Add a module PlainAST for the typed AST without location

# 0.13.0-alpha
- Redesign Necro from the bottom up
- Add mix-in modules
- Change pattern-matching algorithm to use Luc Maranget's decision tree algorithm

# 0.12.2.1
- Fix

# 0.12.2
- remove binds with type arguments. Theoretically a regression, but there is no
context where it was useful

# 0.12.1
- add or-patterns
- add warning for single branch
- fix no-warning option
- warnings are now exposed in the typer module
- binders are checked more thoroughly at the `binder _ = _` point

# 0.12.0.1
- Fix bug with includes

# 0.12
- Allow application to only 1 argument (`f x y` is now syntactically invalid)
- Allow the pattern `((p1:x), (p2:y))` as synonym for `((p1,p2):(x,y))`
- Allow `()` as synonym for `(():())`
- Also allow `λ ((x:a), (y:b)) →`
- write `type a = b` instead of `type a := b` to unify type aliases with ADTs
  and records
- write `binder @ = bind` instead of `binder a = bind`
- Improve location with syntax errors

# 0.11.3
- Remove transformers (Necro Trans has been created as a separate tool)

# 0.11.2.2
- Fix in `necrotrans eta`
- Disable warning printing when using a transformer

# 0.11.2.1
- Fix in version printing

# 0.11.2
- Check binder validity, even when they are never used

# 0.11.1
- Fix in error handling

# 0.11
- Remove `Alias` constructor from `necro_type` handle as a base type
- Clearly separate constructors from fields
- Remove `common_refinement` and simplify `is_exhaustive` and `is_disjoint` (now
  takes as input a skeletal semantics)
- Remove `Util.list_equals`, and updated dependency to OCaml ≥ 4.12
- Separate duplicate checking from actual code. Less efficient, but easier to
  read, check, and maintain 
- Fix wrap (add backtrace)
- add a --version option

# 0.10.1
- Remove all skeletal interface stuff

# 0.10.0
- Use dune to compile files and generate opam package

# 0.9
- Change `ss_types`'s signature to remove some redundancy

# 0.8.8.5
- Fix error localization

# 0.8.8.4
- Fix

# 0.8.8.3
- Fix includes

# 0.8.8.2
- Fix includes

# 0.8.8.1
- Fix printer

# 0.8.8
- Authorize parenthesis around skeleton

# 0.8.7
- Fix `included` for pattern-matching

# 0.8.6
- Update necro-test with matches

# 0.8.5
- Fix includes: All includes were considered as opens

# 0.8.4
- Fix match: takes a term and not a skeleton

# 0.8.3
- Fix `common_refinement`

# 0.8.2
- Fix functions in `skeleton.ml`

# 0.8.1
- Fix `included`

# 0.8
- Adding a match construct

# 0.7
- The constructor `TRecord` has been split into two separate constructors.

# 0.6.4
- Authorizing to have a term and a type with the same name

# 0.6.3
- Add `is_exhaustive` and `is_disjoint` to `Skeleton`

# 0.6.2
- Add handling of `let f (p : ty) = s1 in s2`. Previously, this only worked with
  `p` being a variable

# 0.6.1
- Add handling of `let p : ty = s1 in s2`. This syntax was previously invalid

# 0.6.0
- Add a `no-warning` option
- renaming "value" to "term" in the AST (and everywhere)
- Changed the way to specify arguments (using an OCaml style). Examples:
	+ `val f: t1 → t2 = …`
	+ `val f (x:t1): t2`
	+ `val f (_:t1): t2`
	+ `val f ((x,y):(t1,t2)): t3`
	+ `val f (C x:t1): t2`

# 0.5.2
- Fix: error printing is now in stderr
- Add warning for unused variables
- Add transformer to anonymize unused variables
- Allow underscore as argument for toplevel functions

# 0.5.1
- Fix: common refinement now works with records

# 0.5.0
- TFunc now takes a pattern instead of a variable (*BREAKING*)

# 0.4.3
- removing necessity of `require`, it is now implicited.
- removing chained include path, as they are no longer needed (`a::b::v` not valid anymore)

# 0.4.2
- remove parentheses in argument naming :
  `val f: (a:a) → b` becomes `val f: a:a → b`

# 0.4.1.1:
- alias can now depend upon each other
- improve printing

# 0.4.1:
- add details to some error messages
- `include` becomes `open` and `require`

# 0.4.0:
- There are now two kinds of skeletal semantics. One with locations and one
  without
- Adding an annotation in skeltypes to know what values are specified or
  non-specified
- Changing ski files (adding +/- annotation to reflect if they are specified)

# 0.3.2.1:
- new transformer:
  - `inline [value]`

# 0.3.2.0:
- new transformers:
  - `inlinemonads`
  - `eta`
- new functions in `skeleton.ml`:
  - `specialize_skel` and `specialize_value` to specialize type arguments
  - `pattern_variables` now in the interface
  - `apply` to (as prettily as possible) apply a skeleton to a list of values
  - `alpha_map_skel` and `alpha_map_value` as well as `alpha_rename_skel` and
    `alpha_rename_value` to update variable names with handling of shadowing
- fix: `value_type`

# 0.3.1.1:
- binders can be `(symbol · (symbol | ident_car)* )` instead of `symbol+`

# 0.3.1.0:
- binders can be a string of symbols instead of one unique symbol
- symbols binder can be used after `;`

# 0.3.0.0:
- `term` is now `val`
- special comments
- values can now be put as first hand of an application
- monadic operators moved: `let%bind … = …` ⇒ `let … =%bind …`
- adding symbols as binders, `binder` is therefore a reserved keyword
- adding existentials (`let … : … in`)
- forbidding that a value and a type have the same name
