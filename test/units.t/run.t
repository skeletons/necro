  $ for f in $(ls ./success/*.sk); do necroparse -d $f; done
  $ for f in $(ls ./warnings/*.sk); do necroparse -d $f; done
  File "./warnings/match_non_exhaustive.sk", lines 6-9, characters 2-5:
  This pattern matching is not exhaustive
  File "./warnings/match_useless_case.sk", line 20, characters 4-7:
  This pattern is redundant
    20 | 	| D _
            ^^^
  File "./warnings/one_branch.sk", line 4, characters 2-15:
  This branching has only one branch. It can be removed
     4 | 	branch () end
          ^^^^^^^^^^^^^
  File "./warnings/unused_variable.sk", line 3, characters 8-9:
  Unused variable x
     3 | val f (x
                ^
  $ for f in $(ls ./fail/*.sk); do necroparse -d $f; done
  File "./fail/apply_not_arrow.sk", line 5, characters 2-3:
  This expression has type (). It cannot be applied.
     5 | 	x
          ^
  File "./fail/bind_cannot_infer.sk", line 6, characters 6-8:
  The type of this expression can not be inferred.
     6 | 	let ()
              ^^
  File "./fail/bind_cannot_infer_2.sk", line 6, characters 2-24:
  The type of this expression can not be inferred.
     6 | 	let () =%bind () in ()
          ^^^^^^^^^^^^^^^^^^^^^^
  File "./fail/bind_cannot_unify.sk", line 6, characters 9-15:
  Unification error. Hint: is the type variable a equal to () or ((), ())?
     6 | 	let () =%bind
                 ^^^^^^
  File "./fail/bind_with_args.sk", line 5, characters 13-14:
     5 | binder @ = x<
                     ^
  Syntax error, unexpected token: <
  File "./fail/component_not_tuple.sk", line 6, characters 2-3:
  This expression is not a tuple.
     6 | 	x
          ^
  File "./fail/component_zero.sk", line 5, characters 2-5:
  The components of a tuple are numbered starting with 1.
     5 | 	x.0
          ^^^
  File "./fail/cyclical_alias_1.sk", line 3, characters 6-7:
  Cycle detected in alias definitions: a -> a.
     3 | type a
              ^
  File "./fail/cyclical_alias_2_typeargs.sk", line 5, characters 6-7:
  Cycle detected in alias definitions: a -> b -> a.
     5 | type a
              ^
  File "./fail/cyclical_alias_7.sk", line 3, characters 6-8:
  Cycle detected in alias definitions: a1 -> a2 -> a3 -> a4 -> a5 -> a6 -> a7 -> a1.
     3 | type a1
              ^^
  File "./fail/empty_branch.sk", line 4, characters 10-20:
  Cannot infer a type for this empty branching.
     4 | 	let _ = branch end
                  ^^^^^^^^^^
  File "./fail/func_not_arrow.sk", line 4, characters 2-14:
  This expression is an arrow, it cannot have type ().
     4 | 	λ () → ()
          ^^^^^^^^^^^^
  File "./fail/incompatible_branches.sk", lines 5-9, characters 3-6:
  In this branching, some branches have incompatible types. The types of the branches are [(); ((), ())].
  File "./fail/missing_fields.sk", line 6, characters 2-8:
  The field y is missing in this record.
     6 | 	(x=())
          ^^^^^^
  File "./fail/multapply.sk", line 4, characters 7-8:
     4 | 	f () (
               ^
  Syntax error, unexpected token: (
  File "./fail/no_such_field.sk", line 8, characters 9-10:
  The type a has no field named y.
     8 | 	| (x=_,y
                 ^
  File "./fail/not_a_bind_1.sk", line 4, characters 9-12:
  The term x has type ((), ()) → (). It cannot be used as a binding operator. Binding operators need to have a type matching (_, (_ → _)) → _.
     4 | 	let () =%x
                 ^^^
  File "./fail/not_a_bind_2.sk", line 5, characters 12-13:
  The term @ has type () → (). It cannot be used as a binding operator. Binding operators need to have a type matching (_, (_ → _)) → _.
     5 | binder @ = x
                    ^
  File "./fail/not_a_record_set.sk", line 4, characters 10-12:
  This expression is not a record.
     4 | 	let _ = ()
                  ^^
  File "./fail/or_pattern_variable.sk", line 6, characters 16-17:
  The variable b appears only on one side of this or-pattern.
     6 | 	let (A a|B (a,b
                        ^
  File "./fail/or_pattern_variable_different_type.sk", line 6, characters 6-15:
  The variable a has type () in the left-hand side of this or-pattern, and type ((), ()) in the right-hand side.
     6 | 	let (A a|B a)
              ^^^^^^^^^
  File "./fail/pattern_cannot_infer.sk", line 6, characters 13-14:
  The type of this pattern could not be inferred.
     6 | 	let _ = λ A
                     ^
  File "./fail/pattern_constr_not_variant.sk", line 6, characters 4-7:
  A pattern was expected of type a.
     6 | 	| C _
            ^^^
  File "./fail/pattern_constr_wrong_type.sk", line 8, characters 4-5:
  The type a has no constructor named B.
     8 | 	| B
            ^
  File "./fail/pattern_rebound_variable_record.sk", line 6, characters 6-16:
  The variable x is multiply bound in this pattern.
     6 | 	let (a=x, b=x)
              ^^^^^^^^^^
  File "./fail/pattern_rebound_variable_tuple.sk", line 4, characters 6-12:
  The variable x is multiply bound in this pattern.
     4 | 	let (x, x)
              ^^^^^^
  File "./fail/pattern_rec_not_rec.sk", line 7, characters 4-9:
  A pattern was expected of type a.
     7 | 	| (x=a)
            ^^^^^
  File "./fail/pattern_rec_wrong_type.sk", line 8, characters 5-6:
  The type a has no field named y.
     8 | 	| (y
             ^
  File "./fail/pattern_tuple_not_tuple.sk", line 7, characters 4-13:
  A pattern was expected of type a.
     7 | 	| (_, _, _)
            ^^^^^^^^^
  File "./fail/pattern_tuple_wrong_length.sk", line 7, characters 4-13:
  A pattern was expected of type (a, a).
     7 | 	| (_, _, _)
            ^^^^^^^^^
  File "./fail/redef_constr.sk", line 3, characters 16-17:
  The constructor A has already been defined.
     3 | type t = | A | A
                        ^
  File "./fail/redef_constr.sk", line 3, characters 12-13:
  First defined here.
     3 | type t = | A
                    ^
  File "./fail/redef_field_record_make.sk", line 6, characters 9-10:
  The field x has already been defined.
     6 | 	(x=(), x
                 ^
  File "./fail/redef_field_record_make.sk", line 6, characters 3-4:
  First defined here.
     6 | 	(x
           ^
  File "./fail/redef_field_record_set.sk", line 6, characters 15-16:
  The field x has already been defined.
     6 | 	a ← (x=(), x
                       ^
  File "./fail/redef_field_record_set.sk", line 6, characters 9-10:
  First defined here.
     6 | 	a ← (x
                 ^
  File "./fail/redef_record.sk", line 4, characters 12-13:
  The field x has already been defined.
     4 | type t2 = (x
                    ^
  File "./fail/redef_record.sk", line 3, characters 12-13:
  First defined here.
     3 | type t1 = (x
                    ^
  File "./fail/redef_term.sk", line 4, characters 5-6:
  The term x has already been defined.
     4 | val x
             ^
  File "./fail/redef_term.sk", line 3, characters 5-6:
  First defined here.
     3 | val x
             ^
  File "./fail/redef_type.sk", line 4, characters 6-7:
  The type a has already been defined.
     4 | type a
              ^
  File "./fail/redef_type.sk", line 3, characters 6-7:
  First defined here.
     3 | type a
              ^
  File "./fail/semicolon_not_unit.sk", line 4, characters 2-10:
  This tuple has length 2, but a term was expected of type ().
     4 | 	((), ())
          ^^^^^^^^
  File "./fail/skeleton_instead_term.sk", lines 2-3, characters 2-4:
  Syntax Error. Got a skeleton while a term was expected.
  File "./fail/tuple_not_prod.sk", line 5, characters 2-10:
  This expression is a product, it cannot have type a.
     5 | 	((), ())
          ^^^^^^^^
  File "./fail/type_variable_poly.sk", line 3, characters 14-18:
  The type variable a is not polymorphic.
     3 | val x<a> (_: a<a>
                      ^^^^
  File "./fail/unbound_binder.sk", line 4, characters 9-11:
  The binding symbol @ was not defined.
     4 | 	let () =@
                 ^^
  File "./fail/unbound_field.sk", line 6, characters 4-5:
  Unbound field y.
     6 | 	a.y
            ^
  File "./fail/unbound_type.sk", line 3, characters 11-12:
  Unbound type t.
     3 | val x (_: t
                   ^
  File "./fail/unification_error.sk", line 6, characters 2-19:
  Unification error. Hint: is the type variable a equal to ((), ()) or ()?
     6 | 	(x=(), y=((),()))
          ^^^^^^^^^^^^^^^^^
  File "./fail/wrong_component.sk", line 5, characters 2-3:
  This type has less than 3 components.
     5 | 	x
          ^
  File "./fail/wrong_number_args.sk", line 3, characters 21-29:
  The variable f was given 3 type arguments instead of 2 expected.
     3 | val f<a,b> (): () = f<a,b,b>
                             ^^^^^^^^
  File "./fail/wrong_type.sk", line 4, characters 4-12:
  This expression has type ((), ()), but an expression was expected of type ().
     4 | 	f ((), ())
            ^^^^^^^^
  [1]
