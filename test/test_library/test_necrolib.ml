open Necro


let edges_are g l =
  let n = List.length l in
  assert (n = List.length (List.sort_uniq Stdlib.compare l)) ;
  assert (Skeleton.PGraph.nb_edges g = n) ;
  List.iter (fun (x, y) ->
    let e name = (name, None) in
    assert (Skeleton.PGraph.mem_edge g (e x) (e y))) l

let _ =
  let sem = parse_and_type [] "testfile.sk" in
  let graph = Skeleton.type_dep_graph [] sem in
  let a,b,c,d,e,f,g = "a","b","c","d","e","f","g" in
  edges_are graph [(a,b); (a,c); (b,d); (c,d); (e,e); (f,g); (g,f)]
