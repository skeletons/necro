# Release Process
- make sure https://gitlab.inria.fr/skeletons/opam-repository is cloned as a sibling of this repo
- edit `CHANGES.md`
- do `make release` (this asks for the version number to use)