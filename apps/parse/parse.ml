let give_version () =
	Printf.printf "Necro Library, version: %s\n"
	(match Build_info.V1.version () with
	 | None -> "n/a"
	 | Some v -> Build_info.V1.Version.to_string v);
	 exit 0

type minus =
| Depend
| Warnings of string

let warnings =
	"\t\tall\tAll warnings" ::
	"\t\tnone\tNo warning" ::
	List.map (fun (i, str) ->
		Format.sprintf "\t\t%d\t%s" i str) Necro.Errors.warning_infos
	|> String.concat "\n"

(* Print usage in stderr and exit *)
let usage () =
	Printf.eprintf
	("Usage: %s [OPTION]... [FILE]...\n\n\
	Parse and type the given file. For every file, the dependencies must precede
	in the list except if the -d option is given.\n\n\
	Options:\n\
	\t-v / --version\tPrint the version and exit.\n\
	\t-d / --find-deps\tOnly one file must be given. The dependencies are then \
	looked for in the cwd, and in the path of the given file.\n\
	\t-w / --warnings\tChoose warnings to print, using a comma-separated list.\n\
	\tOptions are:\n\
	%s\n")
	Sys.argv.(0) warnings ;
	exit 1


let parse l =
	let rec parse accu_files accu_options l =
		begin match l with
		| [] ->
				(accu_files, accu_options)
		| "-v" :: _ | "--version" :: _ ->
				give_version ()
		| "-d" :: q | "--find-deps" :: q ->
				parse accu_files (Depend :: accu_options) q
		| "-w" :: x :: q | "--warnings" :: x :: q ->
				parse accu_files (Warnings x :: accu_options) q
		| a :: q ->
				parse (a :: accu_files) accu_options q
		end
	in parse [] [] (List.tl l)

(** MAIN FUNCTION **)
let () =
	let files, options = parse (Array.to_list Sys.argv) in
	let f, d =
		begin match files with
		| [] -> usage ()
		| a :: [] when List.mem Depend options -> a, None
		| _ :: _ when List.mem Depend options -> usage ()
		| a :: q -> a, Some (List.rev q)
		end
	in
	let warn =
		let spec = List.filter_map (fun o ->
			begin match o with
			| Warnings x -> Some x
			| _ -> None
			end) options
		in
		begin match spec with
		| [] | ["all"]-> Necro.Errors.all_warnings
		| ["none"] -> []
		| _ :: _ :: _ -> usage ()
		| [x] ->
				begin try
					x |> String.split_on_char ',' |> List.map int_of_string |> List.sort_uniq Int.compare
				with _ ->
					usage ()
				end
		end
	in
	let d =
		begin match d with
		| Some q -> q
		| None -> Necro.find_deps f
		end
	in
	ignore (Necro.parse_and_type ~warn d f)
