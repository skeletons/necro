# Contents

This folder defines several executables for Necro. They are automatically
included when installing Necro Library with opam. Below is a description of each
of them

## Necro Parse

The folder `parse` contains the app Necro Parse. You may use it by typing
`necroparse file.sk` where `file.sk` is your Skel file. It will try to parse and
type your file. If there is an error or a warning, they will be raised in
stderr. Otherwise, the program will print nothing, and return.

For more information, type `necroparse`.

## Necro Print

The folder `print` contains the app Necro Print. You may use it by typing
`necroprint file.sk` where `file.sk` is your Skel file. It will pretty-print
your file. Be warned that comments will be removed, except for special comments.

For more information, type `necroprint`

## Necro Dep

The folder `dep` contains the app Necro Dep. You may use it by typing
`necrodep file.sk` where `file.sk` is your Skel file. It will generate a
dependency graph for all terms defined in your file.

For more information, type `necroprint`
