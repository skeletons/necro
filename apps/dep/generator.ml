(** WARNING: only works with one file for now *)

open Necro


let log name g ff =
	let roots = (Skeleton.PGraph.fold_vertex (fun v roots ->
		if Skeleton.PGraph.in_degree g v = 0 then v :: roots else roots) g []) in
	(* Print all roots *)
	Format.fprintf ff "@[<v 2>The roots for file %s are:@,%a@]" name
	(Format.pp_print_list (fun ff (x, _) ->
		Format.fprintf ff "- %s" x)) roots

let generate_log deps sem name =
	let g = Skeleton.term_dep_graph deps sem in
	let ff = Format.str_formatter in
	let () = log name g ff in
	Format.flush_str_formatter ()

let pprint path x =
	begin match path with
	| None -> x
	| Some p -> p ^ "::" ^ x
	end

let print_vertex g ff v =
	let pprint (x, path) = pprint path x in
	let s = Skeleton.PGraph.succ g v in
	if s = [] then
			Format.fprintf ff "\"%s\"" (pprint v)
	else
			Format.fprintf ff "\"%s\" -> {%a}" (pprint v) (Format.pp_print_list
			~pp_sep:(fun ff () -> Format.fprintf ff " ") (fun ff x ->
				Format.fprintf ff "\"%s\"" (pprint x))) s

let print_graph ff name g =
	Format.fprintf ff "@[<v 2>digraph %s {@,%a@]@,}"
	name (Format.pp_print_list (print_vertex g)) (Skeleton.PGraph.fold_vertex
	List.cons g [])

let generate_dot deps sem name =
	let g = Skeleton.term_dep_graph deps sem in
	let ff = Format.str_formatter in
	let () = print_graph ff name g in
	Format.flush_str_formatter ()
